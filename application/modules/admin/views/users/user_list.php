<link rel="stylesheet" href="<?= base_url() ?>/public/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-7">
				<h1>Pengguna <a href="<?= base_url('admin/users/add') ?>" class="btn btn-sm btn-default">Tambah baru</a></h1>
			</div>

			<div class="col-sm-5">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Penelitian</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<!-- fash message yang muncul ketika proses penghapusan data berhasil dilakukan -->
			<?php if ($this->session->flashdata('msg') != '') : ?>
				<div class="alert alert-success flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Success!</h4>
					<?= $this->session->flashdata('msg'); ?>
				</div>
			<?php endif; ?>



			<div class="card">

				<div class="card-body">

					<table id="tbpengguna" class="table table-bordered table-striped">
						<thead>
							<tr>
								<!--<th style="width:20%">Username</th>-->
								<th style="width:20%">Username</th>
								<th style="width:40%">Nama Lengkap</th>
								<th style="width:15%" class="text-center">Role</th>
								<th style="width:15%" class="text-center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($users->result_array() as $row) {  ?>
								<tr>
									<td><?= $row['username']; ?></td>
									<?php if ($row['role'] == 3) { ?>
										<td>
											<div class="media">
												<?php if ($row['photo'] == '') { ?>

													<div style="width:40px;height:40px; background:url('<?= base_url(); ?>public/dist/img/nophoto.png') center top no-repeat; background-size:100%;" class="img-circle"></div>
												<?php } else { ?>

													<div style="width:40px;height:40px; background:url('<?= base_url($row['photo']); ?>') center center no-repeat; background-size:cover;" class="profile-user-img img-responsive img-circle"></div>

												<?php } ?> &nbsp;


												<div class="media-body mt-1 ml-2"><?= ($row['nama_lengkap'] == '') ? $row['username'] : $row['nama_lengkap']; ?></div>
											</div>
										</td>
									<?php } else { ?>
										<td>
											<div class="media">
												<?php if ($row['d_photo'] == '') { ?>

													<div style="width:40px;height:40px; background:url('<?= base_url(); ?>public/dist/img/nophoto.png') center top no-repeat; background-size:100%;" class="img-circle"></div>
												<?php } else { ?>

													<div style="width:40px;height:40px; background:url('<?= base_url($row['d_photo']); ?>') center center no-repeat; background-size:cover;" class="profile-user-img img-responsive img-circle"></div>

												<?php } ?> &nbsp;


												<div class="media-body mt-1 ml-2"><?= ($row['d_nama'] == '') ? $row['username'] : $row['d_nama']; ?></div>
											</div>
										</td>

									<?php } ?>

									<td><?= $row['rolenya']; ?></td>

									<td class="text-center">
										<!--<a class="btn btn-default btn-sm" href="<?= base_url('admin/users/detail/' . $row['id']); ?>">
											<i class="fa fa-search" style="color:;"></i>
										</a>-->
										<?php if ($row['role'] == 3) { ?>
											<a class="btn btn-default btn-sm" href="<?= base_url('admin/users/edit_profile/' . $row['id']); ?>">
												<i class="fas fa-pencil-alt" style="color:;"></i>
											</a>
										<?php } else if ($row['role'] == 2) { ?>
											<a class="btn btn-default btn-sm" href="<?= base_url('admin/users/edit_staf/' . $row['id']); ?>">
												<i class="fas fa-pencil-alt" style="color:;"></i>
											</a>
										<?php } ?>
										<a href="" style="color:#fff;" title="Hapus" class="delete btn btn-sm btn-danger" data-href="<?= base_url('admin/users/del/' . $row['id']); ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
									</td>
								</tr>
							<?php } ?>

						</tbody>
						</tfoot>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<div class="modal fade" id="confirm-delete">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Perhatian</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Tutuo">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Yakin ingin menghapus data ini?&hellip;</p>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- DataTables -->
<script src="<?= base_url() ?>/public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
</script>

<!-- page script -->
<script>
	$(document).ready(function() {
		$('#tbpengguna').DataTable({
			initComplete: function() {
				this.api().columns([2]).every(function() {
					var column = this;
					var select = $('<select style="float:right"><option value="">Pilih Role</option></select> ')
						.appendTo($(column.header()))
						.on('change', function() {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
							);

							column
								.search(val ? '^' + val + '$' : '', true, false)
								.draw();
						});


					column.data().unique().sort().each(function(d, j) {
						select.append('<option value="' + d + '">' + d + '</option>')
					});
					$(select).click(function(e) {
						e.stopPropagation();
					});
				});
			}
		});
	});


	$("#pengguna").addClass('menu-open');
	$("#pengguna .semua_pengguna a.nav-link").addClass('active');
</script>