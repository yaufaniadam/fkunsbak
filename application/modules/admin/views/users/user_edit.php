<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Ubah Pengguna</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Ubah Pengguna</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">

        <?php if (isset($msg) || validation_errors() !== '') : ?>
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="fa fa-exclamation"></i> Terjadi Kesalahan</h4>
            <?= validation_errors(); ?>
            <?= isset($msg) ? $msg : ''; ?>
          </div>
        <?php endif; ?>
        <!-- fash message yang muncul ketika proses penghapusan data berhasil dilakukan -->
        <?php if ($this->session->flashdata('msg') != '') : ?>
          <div class="alert alert-success flash-msg alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>Success!</h4>
            <?= $this->session->flashdata('msg'); ?>
          </div>
        <?php endif; ?>

        <div class="card card-warning">
          <div class="card-body box-profile">
            <div class="row">
              <div class="col-md-2">
                <?php if ($user['photo'] == '') { ?>
                  <div style="width:100px;height:100px; background:url('<?= base_url(); ?>public/dist/img/nophoto.png') center top no-repeat; background-size:100%;" class="img-circle"></div>
                <?php } else { ?>
                  <div style="width:110px;height:110px; background:url('<?= base_url($user['photo']); ?>') center center no-repeat; background-size:cover;" class="profile-user-img img-responsive img-circle"></div>
                <?php } ?>
              </div>
              <div class="col-md-9">
                <h4 class="mt-4 mb-0"><?= $user['nama_lengkap']; ?></h4>
                <p><i class="fas fa-envelope mr-1"></i> <?= $user['email']; ?></p>
              </div>
            </div>
          </div>
        </div>

      </div>


      <div class="col-md-5">

        <div class="card card-success">
          <div class="card-body box-profile">

            <?php echo form_open_multipart(base_url('admin/users/edit/' . $user['id']), 'class="form-horizontal"');  ?>
            <input type="hidden" name="role" value=" <?= $user['role']; ?>">
            <div class="form-group">
              <label for="username" class="control-label">Username</label>
              <div class="">
                <input type="text" value="<?= $user['username']; ?>" name="username" class="form-control" id="username" placeholder="">

              </div>
            </div>

            <div class="form-group">
              <label for="email" class="control-label">Email</label>
              <div>
                <input type="email" value="<?= $user['email']; ?>" name="email" class="form-control" id="email" placeholder="">
              </div>
            </div>

            <div class="form-group">
              <label for="password" class="control-label">Password</label>
              <div>
                <input type="password" name="password" class="form-control" id="password" placeholder="">
                <input type="hidden" name="password_hidden" value="<?= $user['password']; ?>">
              </div>
            </div>

            <!-- <div class="form-group">
              <label for="role" class="control-label">Role</label>
              <div>
                <select name="role" class="form-control">
                  <option value="">Pilih Role</option>
                  <option value="1" <?= ($user['role'] == 1) ? 'selected' : ''; ?>>Admin Divisi</option>
                  <option value="2" <?= ($user['role'] == 2) ? 'selected' : ''; ?>>Dosen Pembimbing</option>
                  <option value="3" <?= ($user['role'] == 3) ? 'selected' : ''; ?>>Residen</option>
                </select>
              </div>
            </div>-->

            <div class="form-group">
              <div>
                <input type="submit" name="submit" value="Ubah Akun" class="btn btn-info">
              </div>
            </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
      <?php if ($user['role'] == 3) { ?>
        <div class="col-md-7">

          <div class="card card-danger">
            <div class="card-body box-profile">
              <?php echo form_open_multipart(base_url('admin/users/edit_profile/' . $user['id']), 'class="form-horizontal"');  ?>
              <div class="form-group">
                <label for="nama_lengkap" class="control-label">Nama Lengkap</label>
                <input type="text" value="<?= $residen['nama_lengkap']; ?>" name="nama_lengkap" class="form-control" id="nama_lengkap" placeholder="" required="true">
              </div>
              <div class="form-group">
                <label for="nim" class="control-label">NIM</label>
                <input type="text" value="<?= $residen['nim']; ?>" name="nim" class="form-control" id="nim" placeholder="" required="true">
              </div>
              <div class="form-group">
                <label for="angkatan" class="control-label">Angkatan</label>
                <input type="number" value="<?= $residen['angkatan']; ?>" name="angkatan" class="form-control" id="angkatan" placeholder="" required="true">
              </div>
              <div class="form-group">
                <label for="tempat_lahir" class="control-label">Tempat Lahir</label>
                <input type="text" value="<?= $residen['tempat_lahir']; ?>" name="tempat_lahir" class="form-control" id="tempat_lahir" placeholder="" required="true">
              </div>
              <div class="form-group">
                <label for="tanggal_lahir" class="control-label">Tanggal Lahir</label>
                <input type="text" value="<?= $residen['tanggal_lahir']; ?>" name="tanggal_lahir" class="form-control" id="tanggal_lahir" placeholder="Cth: 21 Januari 1988" required="true">
              </div>
              <div class="form-group">
                <label for="tanggal_lahir" class="control-label">Jenis Kelamin</label>
                <br>
                <input type="radio" value="l" name="gender" required="true" <?= ($residen['gender'] == 'l') ? 'checked="true"' : ''; ?>> Laki-laki
                <input type="radio" value="p" name="gender" required="true" <?= ($residen['gender'] == 'p') ? 'checked="true"' : ''; ?>> Perempuan
              </div>
              <div class="form-group">
                <label for="telp" class="control-label">Telepon</label>
                <input type="number" value="<?= $residen['telp']; ?>" name="telp" class="form-control" id="telp" placeholder="" required="true">
              </div>
              <div class="form-group">
                <label for="alamat" class="control-label">Alamat</label>
                <textarea name="alamat" class="form-control" id="alamat"><?= $residen['alamat']; ?></textarea>
              </div>

              <div class="form-group">
                <label for="photo" class="control-label">Foto Profil (jpg/png) 200x200px</label>
                <input type="file" name="photo" class="form-control" id="photo">
                <input type="hidden" name="photo_hidden" value="<?= $residen['photo']; ?>">
              </div>



              <div class="form-group">
                <label for="photo" class="control-label">Staf Pembimbing Akademik</label><br>
                <div class="">
                  <select id="pembimbing" class="form-control" name="pembimbing">
                    <option value="">Pilih Staf Pembimbing</option>
                    <?php foreach ($staf as $staf) { ?>
                      <option <?php if ($residen['id_pembimbing'] == $staf['id']) {
                                echo "selected";
                              } ?> value="<?= $staf['id'] ?>"><?= $staf['nama_lengkap']; ?><?php $staf['id'] ?></option>
                    <?php } ?>
                  </select>
                </div>
                <!-- <input type="hidden" id="hidden_pembimbing" name="pembimbing"> -->
              </div>

              <div class="form-group">
                <input type="submit" name="submit" value="Ubah Profil" class="btn btn-info">
              </div>
              <?php echo form_close(); ?>

            </div>
          </div>
        </div>
      <?php } else if ($user['role'] == 2) {  ?>

        <div class="col-md-7">

          <div class="card card-danger ">
            <div class="card-body box-profile">
              <?php echo form_open_multipart(base_url('admin/users/edit_staf/' . $user['id']), 'class="form-horizontal"');  ?>
              <div class="form-group">
                <label for="nama_lengkap" class="control-label">Nama Lengkap</label>
                <input type="text" value="<?= $staf['nama_lengkap']; ?>" name="nama_lengkap" class="form-control" id="nama_lengkap" placeholder="" required="true">
              </div>
              <div class="form-group">
                <label for="nip" class="control-label">NIP</label>
                <input type="text" value="<?= $staf['nip']; ?>" name="nip" class="form-control" id="nip" placeholder="" required="true">
              </div>

              <div class="form-group">
                <label for="telp" class="control-label">Telepon</label>
                <input type="text" value="<?= $staf['telp']; ?>" name="telp" class="form-control" id="telp" placeholder="" required="true">
              </div>
              <div class="form-group">
                <label for="alamat" class="control-label">Alamat</label>
                <textarea name="alamat" class="form-control" id="alamat"><?= $staf['alamat']; ?></textarea>
              </div>

              <div class="form-group">
                <label for="photo" class="control-label">Foto Profil (jpg/png) 200x200px</label>
                <input type="file" name="photo" class="form-control" id="photo">
                <input type="hidden" name="photo_hidden" value="<?= $staf['photo']; ?>">
              </div>



              <div class="form-group">
                <input type="submit" name="submit" value="Ubah Profil" class="btn btn-info">
              </div>
              <?php echo form_close(); ?>

            </div>
          </div>
        </div>

      <?php  } ?>
    </div>
  </div>
</section>

<script>
  $("#pengguna").addClass('menu-open');
</script>