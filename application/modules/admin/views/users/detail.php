<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Detail Pengguna</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Detail Pengguna</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">

      </div>
      <div class="col-md-5">

        <div class="card card-info card-outline">
          <div class="card-body box-profile">
            <div class="form-group">
              <label for="username" class="control-label">Username</label>
              <input type="text" value="<?= $user['username']; ?>" class="form-control" disabled>
            </div>
          </div>
        </div>
        <?php if ($user['role'] == 3) { ?>
          <div class="card card-success card-outline">
            <div class="card-body box-profile">
              <div class="form-group">
                <label for="username" class="control-label">Staf Pembimbing Akademik</label>
                <input type="text" value="<?= $user['nama_dosen']; ?>" class="form-control" disabled>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      <?php if ($user['role'] == 3) { ?>
        <div class="col-md-7">

          <div class="card card-danger card-outline">
            <div class="card-body box-profile">
              <div class="form-group">
                <label for="tempat_lahir" class="control-label">Tempat Lahir</label>
                <input type="text" value="<?= $user['tempat_lahir']; ?>" name="tempat_lahir" class="form-control" disabled>
              </div>
              <div class="form-group">
                <label for="tanggal_lahir" class="control-label">Tanggal Lahir</label>
                <input type="text" value="<?= $user['tanggal_lahir']; ?>" name="tanggal_lahir" class="form-control" disabled>
              </div>
              <div class="form-group">
                <label for="tanggal_lahir" class="control-label">Jenis Kelamin</label>
                <br>
                <input type="radio" value="l" name="gender" required="true" <?= ($user['gender'] == 'l') ? 'checked="true"' : ''; ?>> Laki-laki
                <input type="radio" value="p" name="gender" required="true" <?= ($user['gender'] == 'p') ? 'checked="true"' : ''; ?>> Perempuan
              </div>
              <div class="form-group">
                <label for="telp" class="control-label">Telepon</label>
                <input type="text" value="<?= $user['telp']; ?>" class="form-control" disabled>
              </div>
              <div class="form-group">
                <label for="alamat" class="control-label">Alamat</label>
                <textarea name="alamat" class="form-control" disabled><?= $user['alamat']; ?></textarea>
              </div>


            </div>
          </div>
        </div>
      <?php } else if ($user['role'] == 2) {  ?>
        <div class="col-md-7">

          <div class="card card-danger card-outline">
            <div class="card-body box-profile">

              <div class="form-group">
                <label for="telp" class="control-label">Telepon</label>
                <input type="text" value="<?= $user['telp']; ?>" class="form-control" disabled>
              </div>
              <div class="form-group">
                <label for="alamat" class="control-label">Alamat</label>
                <textarea name="alamat" class="form-control" disabled><?= $user['alamat']; ?></textarea>
              </div>


            </div>
          </div>
        </div>

      <?php } ?>
    </div>
  </div>
</section>



<script>
  $("#profil a.nav-link").addClass('active');
</script>