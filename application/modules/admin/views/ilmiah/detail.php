<!-- Content Header (Page header) -->
<?php

$last = $this->uri->total_segments();
$id = $this->uri->segment($last);

$this->load->library('user_agent');
$refer =  $this->agent->referrer();
$exploded = explode("/", $refer);
?>

<!-- <link rel="stylesheet" href="<?= base_url('public/plugins/ekko-lightbox/ekko-lightbox.css'); ?>"> -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Ilmiah</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="<?= base_url('dosen/dashboard') ?>">Home</a></li>
					<li class="breadcrumb-item active"><a href="<?= base_url('dosen/ilmiah/tahap/' . $for_breadcrumb) ?>"><?= $title ?></a></li>
					<li class="breadcrumb-item active"><?= $ilmiah['judul_ilmiah'] ?></li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->


<section class="content">

	<div class="col-12">
		<?php if (isset($msg) || validation_errors() !== '') : ?>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="fa fa-exclamation"></i> Terjadi Kesalahan</h4>
				<?= validation_errors(); ?>
				<?= isset($msg) ? $msg : ''; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php if ($this->session->flashdata('msg') != '') : ?>
		<div class="alert alert-success flash-msg alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4>Success!</h4>
			<?= $this->session->flashdata('msg'); ?>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('wrn') != '') : ?>
		<div class="alert alert-warning flash-msg alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4>Warning!</h4>
			<?= $this->session->flashdata('wrn'); ?>
		</div>
	<?php endif; ?>

	<div class="container-fluid">
		<div class="row">

			<div class="col-md-3">

				<!-- Profile Image -->
				<div class="card card-olive card-outline">
					<div class="card-body box-profile">
						<div class="text-center">

							<?php if ($res_pic == '') { ?>
								<div style="width:110px;height:110px; background:url('<?= base_url(); ?>public/dist/img/nophoto.png') center top no-repeat; background-size:100%;" class="profile-user-img img-responsive img-circle"></div>
							<?php } else { ?>
								<div style="width:110px;height:110px; background:url('<?= base_url($res_pic); ?>') center center no-repeat; background-size:cover;" class="profile-user-img img-responsive img-circle"></div>
							<?php } ?>

						</div>
						<div class="text-center">
						</div>
						<h3 class="profile-username text-center"><?= $author ?></h3>

						<ul class="list-group list-group-unbordered mb-3">
							<li class="list-group-item">
								<b>Diunggah</b> <a class="float-right"><?= $ilmiah['date'] ?></a>
							</li>
							<li class="list-group-item">
								<b>Tanggal Maju</b> <a class="float-right"><?= $ilmiah['tgl_maju'] ?></a>
							</li>
							<li class="list-group-item">
								<b>Nilai</b> <a class="float-right"><?= $ilmiah['nilai'] == '' ? 'belum dinilai' : $ilmiah['nilai'] ?></a>
							</li>
							<?php if ($ilmiah['file_nilai']) { ?>
								<a href="<?= base_url($ilmiah['file_nilai']) ?>" style="width: 100%;" class="btn btn-warning btn-sm float-right"><i class="fa fa-download"></i> Unduh Nilai Asli</a>
							<?php } else { ?>
								<h5>Nilai asli belum diupload</h5>
							<?php } ?>

							<br>
							<a href="<?= base_url($ilmiah['file_presensi']) ?>" style="width: 100%;" class="btn btn-warning btn-sm float-right"><i class="fa fa-download"></i> Unduh Presensi</a>


						</ul>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->


			</div>
			<!-- /.col -->
			<div class="col-md-9">
				<div class="card">
					<div class="card-header p-3">
						<h4><?= $ilmiah['judul_ilmiah'] ?></h4>
					</div><!-- /.card-header -->
				</div>
				<div class="card">
					<div class="card-header pl-3 pt-2 pb-2">
						Deskripsi
					</div><!-- /.card-header -->
					<div class="card-body">
						<?= $ilmiah['deskripsi'] ?>
					</div>
					<div class="card-footer">
						<a href="<?= base_url($ilmiah['file']) ?>" class="btn btn-warning btn-sm"><i class="fa fa-download"></i>Unduh</a>
					</div>
				</div>

				<div class="card card-olive card-outline">
					<div class="card-header pl-3 pt-2 pb-2">
						Verifikasi Ilmiah
					</div><!-- /.card-header -->
					<div class="card-body box-profile">
						<ul class="list-group list-group-unbordered mb-3">

							<p>Saya telah memeriksa dokumen ini dengan seksama.</p>
							<?php if ($ilmiah['nilai'] != '') { ?>
								<a href="" style="color:#fff;" title="Konfirm" class="btn btn-info float-right <?= $ilmiah['status'] == 1 ? 'disabled' : '' ?>" data-href="<?= base_url('admin/ilmiah/konfirm/' . $ilmiah['id'] . '/1') ?>" data-toggle="modal" data-target="#confirm-konfirm"><?= $ilmiah['status'] == 1 ? 'Sudah dikonfirmasi' : 'Konfirmasi' ?></a>
							<?php } else { ?>
								<h5>Dokumen belum lengkap</h5>
							<?php } ?>

						</ul>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</section>

<!-- MODAL KONFIRMASI -->
<div class="modal fade" id="confirm-konfirm">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Konfirmasi dokumen ini?</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Tutuo">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p><i class="fa fa-exclamation-triangle"></i>
					Dokumen tidak dapat dihapus ataupun diubah apabila sudah dikonfirmasi&hellip;
				</p>

			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a class="btn btn-primary btn-ok">Konfirmasi</a>
			</div>
		</div>
	</div>
</div>
<!-- MODAL KONFIRMASI -->

<!-- MODAL REVISI -->
<div class="modal fade" id="confirm-revisi">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Kirim permintaan revisi?</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Tutuo">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Setelah permintaan revisi dikirim, dokumen tidak dapat dihapus&hellip;</p>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a class="btn btn-primary btn-ok">Konfirmasi</a>
			</div>
		</div>
	</div>
</div>
<!-- MODAL REVISI -->

<script>
	$('#confirm-konfirm').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});

	$('#confirm-revisi').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});

	$("#<?= $id_menu; ?>").addClass('menu-open');
	$("#<?= $id_menu; ?> .<?= $class_menu; ?> a.nav-link").addClass('active');
</script>