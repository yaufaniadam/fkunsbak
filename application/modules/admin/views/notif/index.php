<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<table id="tb_penelitian" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="width:100%" class="text-center">Semua Pesan</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($mails as $mail) { ?>

								<tr>
									<td>
										<div class="row <?= $mail['status'] == 0 ? 'font-weight-bold' : '' ?> ">
											<div class="col-2"><?= $mail['subject'] ?></div>
											<div class="col-8">
												<a href="<?= base_url('admin/notif/mail/' . $mail['id']) ?>"><?= substr($mail['message'], 0, 35) ?>...</a>
											</div>
											<div class="col-2"><?= $mail['tanggal_notif'] ?></div>
										</div>
									</td>
								</tr>

							<?php } ?>
						</tbody>
					</table>
				</div>
				<!-- /.card-body -->
			</div>

		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<!-- fash message yang muncul ketika proses penghapusan data berhasil dilakukan -->
			<?php if ($this->session->flashdata('msg') != '') : ?>
				<div class="alert alert-success flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Success!</h4>
					<?= $this->session->flashdata('msg'); ?>
				</div>
			<?php endif; ?>

			<div class="row">
				<div class="col-sm-8">
					<div class="btn-group" role="group" aria-label="Basic example">
						<a href="<?= base_url('admin/residen/tahap/1/0') ?>" id="tahap-1" class="btn btn-default">Tahap 1</a>
						<a href="<?= base_url('admin/residen/tahap/2/1') ?>" id="tahap-2" class="btn btn-default">Tahap 2a</a>
						<a href="<?= base_url('admin/residen/tahap/3/1') ?>" id="tahap-3" class="btn btn-default">Tahap 2b</a>
						<a href="<?= base_url('admin/residen/tahap/4/0') ?>" id="tahap-4" class="btn btn-default">Tahap 3</a>
					</div>
				</div>
			</div>

			<br>

			<div class="card">

				<div class="card-body">

					r4eskjjjjjjjiu

				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->

<script src="<?= base_url() ?>/public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
</script>

<!-- page script -->
<script>
	$(function() {
		$("#tb_penelitian").DataTable();
	});

	$("#pesan  a.nav-link").addClass('active');
</script>