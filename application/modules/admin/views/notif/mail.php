<?php foreach ($query as $notif) {
	# code...
} ?>
<div class="container">

	<div class="col-lg-12">
		<div class="card col-lg-9 col-md-8 ml-auto mr-auto">
			<div class="card-body">
				<div class="">
					<div class="card shadow-none mt-3 border border-light">
						<div class="card-body " style="min-height: 500px;">
							<div class="media mb-3">
								<div class="media-body">
									<span class="media-meta float-right"><?= $notif['tanggal_notif'] ?></span>
									<h4 class="text-primary m-0"><?= $notif['subject'] ?></h4>
									<!-- <small class="text-muted">From : info@example.com</small> -->
								</div>
							</div> <!-- media -->

							<!-- <p><b>Hi Sir...</b></p> -->
							<p>
								<?= $notif['message'] ?>
							</p>
						</div>
					</div> <!-- card -->
				</div> <!-- end Col-9 -->
			</div>
		</div>
	</div>

</div>
