<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Tambah TOD</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Tambah TOD</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">

				<?php if (isset($msg) || validation_errors() !== '') : ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="fa fa-exclamation"></i> Terjadi Kesalahan</h4>
						<?= validation_errors(); ?>
						<?= isset($msg) ? $msg : ''; ?>
					</div>
				<?php endif; ?>

			</div>

			<div class="col-md-6">
				<div class="card card-success card-outline">
					<div class="card-body box-profile">

						<?php echo form_open_multipart(base_url('admin/tod/tambah_tod'), '') ?>

						<div class="form-group">
							<label for="username" class="control-label">Tanggal Mulai</label>
							<div class="">
								<input type="date" name="" class="form-control" id="start_date" placeholder="" onchange="getStartDate()">
								<input type="hidden" name="start_date" id="hidden_start_date">
							</div>
						</div>

						<div class="form-group">
							<label for="username" class="control-label">Tanggal Selesai</label>
							<div class="">
								<input type="date" name="" class="form-control" id="end_date" placeholder="" onchange="getEndDate()">
								<input type="hidden" name="end_date" id="hidden_end_date">
							</div>
						</div>

						<div class="form-group">
							<div>
								<input type="submit" name="submit" value="Tambah TOD" class="btn btn-info">
							</div>
						</div>
						<?php echo form_close(); ?>

					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<script>
	function getStartDate() {
		var startDate = document.getElementById('start_date').value;
		document.getElementById("hidden_start_date").value = startDate;
	}

	function getEndDate() {
		var endDate = document.getElementById('end_date').value;
		document.getElementById("hidden_end_date").value = endDate;
	}

	$("#tod").addClass('menu-open');
	$("#tod .tambah_tod a.nav-link").addClass('active');
</script>
