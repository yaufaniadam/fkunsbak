<link rel="stylesheet" href="<?= base_url() ?>/public/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Tour of Duty <a href="<?= base_url('admin/tod/tambah_tod'); ?>" class="btn btn-sm btn-default">Tambah baru</a></h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Penelitian</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<!-- fash message yang muncul ketika proses penghapusan data berhasil dilakukan -->
			<?php if ($this->session->flashdata('msg') != '') : ?>
				<div class="alert alert-success flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Success!</h4>
					<?= $this->session->flashdata('msg'); ?>
				</div>
			<?php endif; ?>

			<div id="msg"></div>


			<br>

			<div class="card">

				<div class="card-body">

					<div id="accordion">
						<div class="row">
							<div class="col-5">
								<?php
								foreach ($query as $tod) { ?>
									<div class="card">
										<div class="card-header" id="headingOne">
											<h5 class="mb-0">
												<span><?= ++$start ?>.</span>
												<button class="btn btn-link buka-tod" data-toggle="collapse" data-target="#collapse<? = $tod['id'] ?>" aria-expanded="true" aria-controls="collapse<?= $tod['id'] ?>">
													periode <?= $tod['start_date'] ?> s.d <?= $tod['end_date'] ?>
												</button>
												<a href="<?= base_url("admin/tod/update_tod/" . $tod['id']) ?>" class="btn btn-default float-right">
													<i class="fas fa-pencil-alt" style="color:;"></i>
												</a>
											</h5>
										</div>

										<div id="collapse<?= $tod['id'] ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
											<div class="list-group" id="<?= $tod['id'] ?>">
												<?php foreach (getAllDivisi() as $divisi) { ?>
													<a href="#" id="<?= $divisi['id'] ?>" class="list-group-item list-group-item-action buka-divisi"><?= $divisi['divisi'] ?></a>
												<?php } ?>
											</div>
										</div>
									</div>
								<?php } ?>
								<?php echo $this->pagination->create_links(); ?>
							</div>
							<div class="col-1"></div>
							<div class="col-6" id="residen_container">
								<table id="tb_residen" class="table table-bordered table-striped">
									<thead>
										<th class="text-center">Residen</th>
									</thead>
									<tbody>
										<tr>
											<td id="residen_data">
												Pilih divisi untuk menampilkan daftar residen
											</td>
										</tr>
									</tbody>
								</table>
								<!-- <ul class="list-group">
									<li class="list-group-item">Pilih divisi untuk melihat daftar residen</li>
								</ul> -->
							</div>
						</div>
					</div>

				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<div class="modal fade" id="confirm-delete">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Perhatian</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Tutuo">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Yakin ingin menghapus data ini?&hellip;</p>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- DataTables -->
<script src="<?= base_url() ?>/public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
</script>

<!-- page script -->
<script>
	$(function() {
		$("#tb_residen").DataTable();
	});






	$(".buka-divisi").click(function(e) {
		var divisi_id = $(this).attr("id");
		var tod_id = e.target.parentNode.id
		var url = ' <?= base_url("admin/tod/residen_per_tod/") ?>' + tod_id + '/' + divisi_id

		$.ajax({
			type: 'POST',
			url: url,
			dataType: 'json',
			success: function(data) {
				var residen = ''
				if (data.length > 0) {
					for (let i = 0; i < data.length; i++) {
						// residen +=
						// 	'<ul class="list-group">' +
						// 	'<li class="list-group-item">' + data[i].nama_lengkap + '</li>' +
						// 	'</ul>';
						residen +=
							data[i].nama_lengkap;
					}
					// $('#residen_container').html(residen)
					$('#residen_data').html(residen)
				} else {
					// residen =
					// 	'<ul class="list-group">' +
					// 	'<li class="list-group-item">' + "Tidak ada residen di divisi ini" + '</li>' +
					// 	'</ul>';

					residen = "data residen tidak ditemukan";

					// $('#residen_container').html(residen);
					$('#residen_data').html(residen)
				}
				console.log(data)
			}
		});

		// console.log("tod id : " + tod_id);
		// console.log("divisi id : " + divisi_id)
		// console.log("url : " + url)
	})

	function getTod() {

	}

	$("#tod").addClass('menu-open');
	$("#tod .daftar_tod a.nav-link").addClass('active');
</script>
