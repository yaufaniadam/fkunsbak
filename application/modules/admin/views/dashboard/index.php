<section class="content-header">
	<h1>Beranda</h1>
</section>

<section class="content">
	<div class="col-md-12">
		<div class="box box-success box-solid text-center mb-5">
			<div class="box-header with-border">
				<h3 class="box-title">Selamat Datang, <?= $_SESSION['username'] ?></h3>
			</div>
			<div class="box-body">
				Anda login sebagai Administrator
			</div>
		</div>
	</div>

	<div class="row">
		<!-- ./col -->
		<?php if ($_SESSION['role'] == 1) { ?>
			<?php foreach ($tahap as $tahap) { ?>
				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-<?php if ($tahap['id'] == 1) {
													echo "red";
												} elseif ($tahap['id'] == 2) {
													echo "yellow";
												} elseif ($tahap['id'] == 3) {
													echo "green";
												} else {
													echo "blue";
												} ?>">
						<div class="inner">
							<p>Jumlah ilmiah</p>
							<h3><?= ilmiah_dashboard($tahap['id']) ?></h3>

							<p>Tahap <?= $tahap['tahap'] ?></p>
						</div>
						<div class="icon">
							<i class="far fa-file-alt"></i> </div>
					</div>
				</div>
			<?php } ?>
		<?php } else { ?>
			<div class="col-lg-4 col-xs-6">
				<div class="small-box bg-red">
					<div class="inner">
						<p>Jumlah ilmiah</p>
						<h3><?= $jumlah_ilmiah_tahap_1; ?></h3>

						<p>Tahap 1</p>
					</div>
					<div class="icon">
						<i class="far fa-file-alt"></i> </div>
				</div>
			</div>
			<div class="col-lg-4 col-xs-6">
				<div class="small-box bg-yellow">
					<div class="inner">
						<p>Jumlah ilmiah</p>
						<h3><?= $jumlah_ilmiah_tahap_2; ?></h3>

						<p>Tahap 2</p>
					</div>
					<div class="icon">
						<i class="far fa-file-alt"></i> </div>
				</div>
			</div>
			<div class="col-lg-4 col-xs-6">
				<div class="small-box bg-green">
					<div class="inner">
						<p>Jumlah Residen</p>
						<h3><?= $jumlah_residen; ?></h3>

						<p>Semua Tahap</p>
					</div>
					<div class="icon">
						<i class="far fa-file-alt"></i> </div>
				</div>
			</div>
		<?php } ?>
	</div>

</section>

<script>
	$("#beranda a.nav-link").addClass('active');
</script>
