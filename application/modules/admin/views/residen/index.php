<link rel="stylesheet" href="<?= base_url() ?>/public/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

<?php
foreach ($query as $residen) {
}
$url_tahap = $this->uri->segment(4)
?>

<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1><?= $title; ?></h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active"><?= $title; ?></li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<!-- fash message yang muncul ketika proses penghapusan data berhasil dilakukan -->
			<?php if ($this->session->flashdata('msg') != '') : ?>
				<div class="alert alert-success flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Success!</h4>
					<?= $this->session->flashdata('msg'); ?>
				</div>
			<?php endif; ?>

			<div class="row">
				<div class="col-sm-8">
					<div class="btn-group" role="group" aria-label="Basic example">
						<a href="<?= base_url('admin/residen/tahap/1/0') ?>" id="tahap-1" class="btn btn-default">Tahap 1</a>
						<a href="<?= base_url('admin/residen/tahap/2/1') ?>" id="tahap-2" class="btn btn-default">Tahap 2a</a>
						<a href="<?= base_url('admin/residen/tahap/3/1') ?>" id="tahap-3" class="btn btn-default">Tahap 2b</a>
						<a href="<?= base_url('admin/residen/tahap/4/0') ?>" id="tahap-4" class="btn btn-default">Tahap 3</a>
					</div>
				</div>
			</div>

			<br>

			<div class="card">

				<div class="card-body">

					<?php if ($type == 'all_residen') {
						include 'allResiden.php';
					} elseif ($type == 'residen_by_tahap') {
						if ($url_tahap == 2 or $url_tahap == 3) {
							include 'residenByTahap.php';
						} else {
							include 'residenByTahap_1.php';
						}
					} elseif ($type == 'tahap_spesifik_residen') {
						include 'tahapSpesifikResiden.php';
					} elseif ($type == 'residen_by_divisi') {
						include 'residenByDivisi.php';
					} elseif ($type == 'divisi_spesifik_residen') {
						include 'divisiSpesifikResiden.php';
					}
					?>

				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<div class="modal fade" id="confirm-delete">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Perhatian</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Tutuo">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Yakin ingin menghapus data ini?&hellip;</p>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- DataTables -->
<script src="<?= base_url() ?>/public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
</script>

<!-- page script -->
<script>
	$(function() {
		$("#tb_penelitian").DataTable({
			"order": [
				[3, "desc"]
			]
		});
	});

	$("#tahap-<?= $tahap_id ?>").removeClass('btn-default').addClass('btn-warning');

	$("#<?= $id_menu; ?>").addClass('menu-open');
	$("#<?= $id_menu; ?> .<?= $class_menu; ?> a.nav-link").addClass('active');
</script>