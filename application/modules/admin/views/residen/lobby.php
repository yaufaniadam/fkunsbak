<link rel="stylesheet" href="<?= base_url() ?>/public/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<?php
$this->session->set_userdata('referred_from', current_url());
?>
<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Residen</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Penelitian</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<!-- fash message yang muncul ketika proses penghapusan data berhasil dilakukan -->
			<?php if ($this->session->flashdata('msg') != '') : ?>
				<div class="alert alert-success flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Success!</h4>
					<?= $this->session->flashdata('msg'); ?>
				</div>
			<?php endif; ?>

			<div class="btn-group" role="group" aria-label="Basic example">
				<a href="<?= base_url('admin/residen/lobby/2/12'); ?>" id="2" class="btn btn-default ">Tahap 2A</a>
				<a href="<?= base_url('admin/residen/lobby/3/12'); ?>" id="3" class="btn btn-default">Tahap 2B</a>
			</div>
			<br>
			<br>

			<div class="card">

				<div class="card-body">

					<table id="tb_penelitian" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="width:40%">Nama</th>
								<th class="text-center">NIM</th>
								<th class="text-center">Tahap</th>
								<th class="text-center">Pilih Divisi</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($query as $residen) {  ?>
								<tr>
									<td><?= $residen['nama_lengkap']; ?></td>
									<td><?= $residen['nim']; ?></td>
									<td><?= $residen['tahap']; ?></td>
									<td>
										<button type="button" id="<?= $residen['id_residen'] ?>" class="btn btn-primary buka-modal" data-toggle="modal" data-target="#exampleModal">
											Pilih Divisi
										</button>
									</td>
								</tr>
							<?php } ?>

						</tbody>
						</tfoot>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<div class="modal fade" id="confirm-delete">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Perhatian</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Tutuo">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Yakin ingin menghapus data ini?&hellip;</p>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- MODAL DIVISI -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Pilih Divisi Residen</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- <?php echo form_open_multipart(base_url('admin/residen/submit_divisi/' . $residen['id_residen']), '') ?> -->
			<form id="pilih-divisi" action="" enctype="multipart/form-data" method="post" accept-charset="utf-8">
				<div class="modal-body">
					<div class="form-group">
						<div>
							<div class="row">
								<div class="col-sm-2">
									<label for="residen-name" class="control-label col-sm-2">Nama:</label>
								</div>
								<div class="col-sm-10">
									<p class="nama-residen" name="nama">Loading...</p>
									<input class="hide-id" type="hidden" name="id" value="">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2">
									<label for="residen-nim" class="control-label col-sm-2">NIM:</label>
								</div>
								<div class="col-sm-10">
									<p class="nim-residen" name="nim">Loading...</p>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2">
									<label for="residen-divisi" class="control-label col-sm-2">Divisi:</label>
								</div>
								<div class="col-sm-10">
									<select class="selectpicker" name="divisi" id="divisi">
										<option disabled selected>Pilih Divisi</option>
										<!-- <?php foreach ($daftar_divisi as $divisi) { ?>
											<option value="<?= $divisi['id'] ?>"><?= $divisi['divisi'] ?></option>
										<?php } ?> -->
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary" value="Submit">
				</div>
			</form>
			<!-- <?php echo form_close(); ?> -->
		</div>
	</div>
</div>
<!-- MODAL DIVISI -->

<!-- DataTables -->
<script src="<?= base_url() ?>/public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
</script>

<!-- page script -->
<script>
	$(".buka-modal").click(function() {

		var id = $(this).attr("id");
		var url = '<?= base_url('admin/residen/pilih_divisi/') ?>' + id;
		var url2 = '<?= base_url('admin/residen/not_residen_divisi/'); ?>' + id;

		$('#divisi')
			.find('option')
			.remove()
			.end();
		// alert(url);
		$.ajax({
			type: 'POST',
			url: url,
			dataType: 'json',
			success: function(data) {
				var nama = data[0].nama_lengkap;
				var nim = data[0].nim;

				$('.nama-residen').html(nama);
				$('.nim-residen').html(nim);
				$('.hide-id').attr('value', id)
				$('#pilih-divisi').attr('action', '<?= base_url('admin/residen/submit_divisi/') ?>' + id)

				$.ajax({
					type: 'POST',
					url: url2,
					dataType: 'json',
					success: function(data) {
						// console.log(data)
						$('#divisi').append($('<option>', {
							value: '',
							text: 'pilih divisi'
						}));
						$.each(data, function(i, divisi) {
							$('#divisi').append($('<option>', {
								value: divisi.id,
								text: divisi.divisi
							}));
						});
					}
				});
			}
		})
		// document.getElementById("hidename").value = "asdf";
		// document.getElementById("pilih-divisi").action = '<?= base_url('admin/residen/submit_divisi/') ?>' + id;
	});



	$(function() {
		$("#tb_penelitian").DataTable();
	});

	$("#<?= $btn_class ?>").removeClass("btn-default").addClass("btn-warning");
	$("#<?= $id_menu ?>").addClass('menu-open');
	$("#<?= $id_menu ?> .<?= $class_menu ?> a.nav-link").addClass('active');
</script>
