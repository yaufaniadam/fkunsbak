<!-- Content Header (Page header) -->
<?php
$segments = $this->uri->segment(5, 1);
$tahap_indc = $segments;
$last = $this->uri->total_segments();
$divisi_rekap = $this->uri->segment($last);
$this->session->set_userdata('referred_from', current_url());
if ($segments == 2) {
	$tahap_indc = '2a';
}
if ($segments == 3) {
	$tahap_indc = '2b';
}
if ($segments == 4) {
	$tahap_indc = 3;
}
foreach ($residen as $residen) {
}

$id_residen = $this->uri->segment(4);
$tahap_residen = $this->uri->segment(5);
$divisi_residen = $this->uri->segment(6);
if ($tahap_residen == 2 or $tahap_residen == 3) {
	$progress_per_tahap = residenProgress($id_residen, $tahap_residen, $divisi_residen);
	$approved_progress_per_tahap = approvedProgress($id_residen, $tahap_residen, $divisi_residen);
} else {
	$progress_per_tahap = residenProgress($id_residen, $tahap_residen);
	$approved_progress_per_tahap = approvedProgress($id_residen, $tahap_residen);
}
?>

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Residen</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->


<section class="content">

	<div class="col-12">
		<?php if (isset($msg) || validation_errors() !== '') : ?>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="fa fa-exclamation"></i> Terjadi Kesalahan</h4>
				<?= validation_errors(); ?>
				<?= isset($msg) ? $msg : ''; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php if ($this->session->flashdata('msg') != '') : ?>
		<div class="alert alert-success flash-msg alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4>Success!</h4>
			<?= $this->session->flashdata('msg'); ?>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('wrn') != '') : ?>
		<div class="alert alert-warning flash-msg alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4>Warning!</h4>
			<?= $this->session->flashdata('wrn'); ?>
		</div>
	<?php endif; ?>

	<div class="container-fluid">
		<div class="row">

			<div class="col-md-3">

				<!-- Profile Image -->
				<div class="card card-olive card-outline">
					<div class="card-body box-profile">
						<div class="text-center">

							<?php if ($residen['photo'] == '') { ?>
								<div style="width:110px;height:110px; background:url('<?= base_url(); ?>public/dist/img/nophoto.png') center top no-repeat; background-size:100%;" class="profile-user-img img-responsive img-circle"></div>
							<?php } else { ?>
								<div style="width:110px;height:110px; background:url('<?= base_url($residen['photo']); ?>') center center no-repeat; background-size:cover;" class="profile-user-img img-responsive img-circle"></div>
							<?php } ?>

						</div>

						<h3 class="profile-username text-center"><?= $residen['nama_lengkap'] ?></h3>

						<ul class="list-group list-group-unbordered mb-3">
							<li class="list-group-item">
								<b>Tahap</b> <a class="float-right"><?= $residen_tahap ?></a>
							</li>
							<?php if ($residen_tahap == '2a' or $residen_tahap == '2b') { ?>
								<li class="list-group-item">
									<b>Divisi</b> <a class="float-right"><?= $divisi ?></a>
								</li>
							<?php } ?>
							<li class="list-group-item">
								<b>Progress : </b>
								<?php if ($residen_tahap == '1' or $residen_tahap == '3') { ?>
									<a>
										<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: <?= $progress; ?>%;height :20px; ">
											<?= $progress; ?>%
										</div>
									</a>
								<?php } ?>
								<a>
									<div class="mt-2 progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: <?= $approved_progress; ?>%;height :20px; ">
										<?= $approved_progress; ?>%
									</div>
								</a>
							</li>

							<?php if ($residen_tahap_id == 2 || $residen_tahap_id == 3) { ?>
								<?php if ($approved_progress == 100.0) { ?>
									<li class="list-group-item text-center">
										<a href="<?= base_url('admin/residen/divisi_selesai/' . $residen['id'] . '/' . $tahap_residen . '/' . $divisi_residen) ?>" class="btn btn-primary" href="">Divisi Selesai</a>
									</li>
								<?php } elseif ($approved_progress == 100.0 && $jumlah_residen_divisi == 6) { ?>
									<li class="list-group-item text-center">
										<a href="<?= base_url('admin/residen/naiktahap/' . $residen['id'] . '/' . $tahap_residen . '/' . $divisi_residen) ?>" class="btn btn-primary" href="">Naik Tahap</a>
									</li>
								<?php } ?>
							<?php } else { ?>
								<?php if ($approved_progress == 100.0) { ?>
									<li class="list-group-item text-center">
										<a href="<?= base_url('admin/residen/naiktahap/' . $residen['id'] . '/' . $tahap_residen . '/' . $divisi_residen) ?>" class="btn btn-primary" href="">Naik Tahap</a>
									</li>
								<?php } ?>
							<?php } ?>
							<a href="<?= base_url('admin/rekap/download/' . $id_residen . '/' . $tahap_residen . '/' . $divisi_rekap) ?>" class="btn btn-primary ">Download Rekap Data</a>
						</ul>

					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->

			</div>
			<!-- /.col -->
			<div class="col-md-9">
				<div class="card">
					<div class="card-header pl-3 pt-2 pb-1">
						<h5>Daftar Ilmiah</h5>
					</div><!-- /.card-header -->

					<div class="card-body">
						<div class="row">
							<div class="col-6">
								<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
									<div class="btn-group mr-2" role="group" aria-label="First group">
										<div class="dropdown">
											<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Tahap <?= $tahap_indc; ?>
											</button>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<a class="dropdown-item" href="<?= base_url('admin/residen/detail/' . $residen['id'] . '/1/0') ?>">
													1
												</a>
												<a class="dropdown-item" href="<?= base_url('admin/residen/detail/' . $residen['id'] . '/2/1') ?>">
													2a
												</a>
												<a class="dropdown-item" href="<?= base_url('admin/residen/detail/' . $residen['id'] . '/3/1') ?>">
													2b
												</a>
												<a class="dropdown-item" href="<?= base_url('admin/residen/detail/' . $residen['id'] . '/4/0') ?>">
													3
												</a>
											</div>
										</div>
									</div>
									<?php if ($segments == 2 or $segments == 3) { ?>
										<div class="btn-group mr-2" role="group" aria-label="Second group">
											<div class="dropdown">
												<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Divisi
												</button>
												<div class="dropdown-menu scrollable-menu" aria-labelledby="dropdownMenuButton">
													<?php foreach ($all_divisi as $all_divisi) { ?>
														<a class="dropdown-item" href="<?= base_url('admin/residen/detail/' . $residen['id'] . '/' . $segments) . '/' . $all_divisi['id'] ?>"><?= $all_divisi['divisi'] ?></a>
													<?php } ?>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="col-6">
								Progress :
								<a>
									<div class="mt-2 progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: <?= $progress_per_tahap; ?>%;height :20px; ">
										<?= $progress_per_tahap; ?>%
									</div>
								</a>
								<a>
									<div class="mt-2 progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: <?= $approved_progress_per_tahap; ?>%;height :20px; ">
										<?= $approved_progress_per_tahap; ?>%
									</div>
								</a>
							</div>
						</div>

						<br>
						<table id="tb_penelitian" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th style="width: 50%;" class="text-center">Judul Ilmiah</th>
									<th class="text-center">Kategori</th>
									<th class="text-center">Approved</th>
									<th class="text-center">Detail</th>
								</tr>
							</thead>
							<tbody>
								<?php if ($ilmiah) {
									foreach ($ilmiah as $ilmiah) { ?>
										<tr>
											<td><?= $ilmiah['judul_ilmiah'] ?></td>
											<td><?= $ilmiah['kategori'] ?></td>
											<td class="text-center">
												<?= $ilmiah['status'] == 1 ? '<i class="fas fa-check"></i>' : (($ilmiah['status'] == 2) ? '<i class="fas fa-times"></i>' : "") ?>
											</td>
											<td class="text-center">
												<a href="<?= base_url('admin/ilmiah/detail/' . $ilmiah['id']) ?>" class="btn btn-default btn-sm">
													<i class="fa fa-search" style="color:;"></i>
												</a>
											</td>
										</tr>
								<?php }
								} else {
									echo "<tr><td class='text-center' colspan = 4 >No Data</td></tr>";
								}
								?>

							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</section>

<!-- MODAL DIVISI -->
<!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<?php echo form_open_multipart(base_url('admin/users/add'), '') ?>
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Pilih Divisi Residen</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-2">
						<h6 class="text-center mt-2">Divisi</h6>
					</div>
					<div class="col-sm-10">
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<input type="submit" value="Submit">
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div> -->
<!-- MODAL DIVISI -->

<!-- tb js -->
<script src="<?= base_url() ?>/public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script>
	$('#confirm-konfirm').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});

	$(function() {
		$("#tb_penelitian").DataTable();
	});

	$("#<?= $id_menu; ?>").addClass('menu-open');
	$("#<?= $id_menu; ?> .<?= $class_menu; ?> a.nav-link").addClass('active');
</script>

<style>
	.scrollable-menu {
		height: auto;
		max-height: 200px;
		overflow-x: hidden;
	}
</style>
