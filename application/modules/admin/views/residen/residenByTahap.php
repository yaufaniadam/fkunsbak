<?php

?>
<table id="tb_penelitian" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th style="width: 50%;" class="text-center">Nama</th>
			<th class="text-center">NIM</th>
			<th class="text-center">Angkatan</th>
			<th class="text-center">Progress</th>
			<th class="text-center">Tahap</th>
		</tr>

	</thead>
	<tbody>

		<?php
		if ($query) {
			// $progress = 

			foreach ($query as $residen) {  ?>
				<tr>

					<td><a href="<?= base_url('admin/residen/detail/' . $residen['id_residen'] . '/' . $residen['tahap'] . '/' . $residen['id_divisi']) ?>"><u><?= $residen['nama_lengkap']; ?></u></a></td>
					<td><?= $residen['nim']; ?></td>
					<td><?= $residen['angkatan']; ?></td>
					<td>
						<div class="progress" style="width: 100%;">
							<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: <?= residenProgress($residen['id_residen'], $residen['tahap'], $residen['id_divisi']); ?>%"><?= residenProgress($residen['id_residen'], $residen['tahap'], $residen['id_divisi']); ?>%</div>
						</div>
					</td>
					<td class="text-center"><?= $residen['tahap']; ?></td>
				</tr>
		<?php }
		} ?>
	</tbody>
</table>
