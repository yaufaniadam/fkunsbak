<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Tambah Staf Tahap</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Staf Tahap</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">

				<?php if (isset($msg) || validation_errors() !== '') : ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="fa fa-exclamation"></i> Terjadi Kesalahan</h4>
						<?= validation_errors(); ?>
						<?= isset($msg) ? $msg : ''; ?>
					</div>
				<?php endif; ?>

			</div>

			<div class="col-md-6">
				<div class="card card-success card-outline">
					<div class="card-body box-profile">

						<?php echo form_open_multipart(base_url('admin/dosenpj/tambah'), '') ?>

						<div class="form-group">
							<label for="username" class="control-label">Dosen</label>
							<div class="">
								<div class="form-group">
									<select name="dosen" class="form-control" id="dosen" oncClick="getDosen()">
										<option disabled>Pilih Dosen</option>
										<?php foreach ($dosen as $dosen) { ?>
											<option value="<?= $dosen['id'] ?>"><?= $dosen['nama_lengkap'] ?></option>
										<?php } ?>
									</select>
								</div>
								<!-- <input type="hidden" name="dosen" id="hidden_dosen"> -->
							</div>
						</div>

						<div class="form-group">
							<label for="username" class="control-label">Tahap</label>
							<div class="">
								<select class="form-control" id="tahap" name="tahap">
									<option disabled>Pilih Tahap</option>
									<?php foreach ($tahap as $tahap) { ?>
										<option value="<?= $tahap['id'] ?>"><?= $tahap['tahap'] ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div>
								<input type="submit" name="submit" value="Tambah" class="btn btn-info">
							</div>
						</div>
						<?php echo form_close(); ?>

					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<script>
	function getDosen() {
		var startDate = document.getElementById('dosen').value;
		document.getElementById("hidden_dosen").value = startDate;
	}

	function getTahap() {
		var endDate = document.getElementById('tahap').value;
		document.getElementById("hidden_tahap").value = endDate;
	}

	$("#dpj").addClass('menu-open');
	$("#dpj .tambah_dpj a.nav-link").addClass('active');
</script>