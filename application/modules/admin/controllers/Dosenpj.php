<?php defined('BASEPATH') or exit('No direct script access allowed');
class Dosenpj extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct();
		// $this->load->model('admin/user_model', 'user_model');
		// $this->load->model('admin/profile_model', 'profile_model');
		$this->load->model('Dosenpj_model', 'dosenpj_model');
		$this->load->library('datatable'); // loaded my custom serverside datatable library
	}

	function index()
	{
		$data['query'] = $this->db->query("SELECT pt.*,d.nama_lengkap,t.tahap FROM `pj_tahap` pt LEFT JOIN dosen d ON pt.id_dosen = d.id LEFT JOIN tahap t ON pt.id_tahap = t.id")->result_array();
		$data['view'] = 'staftahap/index.php';
		$this->load->view('layout/layout', $data);
	}

	public function tambah()
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules(
				'dosen',
				'Dosen',
				'trim|required',
				array(
					'required' => '%s wajib diisi'
				)
			);
			$this->form_validation->set_rules(
				'tahap',
				'Tahap',
				'trim|required',
				array(
					'required' => '%s wajib diisi'
				)
			);


			if ($this->form_validation->run() == false) {
				$data['tahap'] = $this->db->query("SELECT * FROM tahap")->result_array();
				$data['dosen'] = $this->db->query("SELECT * FROM dosen WHERE dosen.id NOT IN (SELECT id_dosen FROM pj_tahap) AND dosen.id NOT IN (SELECT id_dosen FROM staf_divisi)")->result_array();
				$data['view'] = 'staftahap/tambah.php';
				$this->load->view('layout/layout', $data);
			} else {
				$data = array(
					'id_dosen' => $this->input->post('dosen'),
					'id_tahap' => $this->input->post('tahap'),
				);

				$data = $this->security->xss_clean($data);
				$result = $this->dosenpj_model->tambah_staftahap($data);

				if ($result) {
					$this->session->set_flashdata('msg', 'Penanggungjawab berhasil ditambahkan!');
					redirect(base_url('admin/dosenpj'));
				}
			}
		} else {
			$data['tahap'] = $this->db->query("SELECT * FROM tahap")->result_array();
			$data['dosen'] = $this->db->query("SELECT * FROM dosen WHERE dosen.id NOT IN (SELECT id_dosen FROM pj_tahap)")->result_array();
			// AND dosen.id NOT IN (SELECT id_dosen FROM staf_divisi)
			$data['view'] = 'staftahap/tambah.php';
			$this->load->view('layout/layout', $data);
		}
	}

	public function hapus($pj_tahap_id)
	{
		$this->db->delete('pj_tahap', array('id' => $pj_tahap_id));
		$this->session->set_flashdata('msg', 'Penanggungjawab berhasil hapus!');
		redirect(base_url('admin/dosenpj'));
	}
}
