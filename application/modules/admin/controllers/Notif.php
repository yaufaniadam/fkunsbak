<?php defined('BASEPATH') or exit('No direct script access allowed');
class Notif extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		// $this->load->model('Tod_model', 'tod_model');
		// $this->load->model('ilmiah_model', 'ilmiah_model');
		// $this->load->model('residen_model', 'residen_model');
		// $this->load->model('divisi_model', 'divisi_model');
		// $this->load->model('tahap_model', 'tahap_model');
		// $this->load->model('kategori_model', 'kategori_model');
		// $this->load->model('divisi_model', 'divisi_model');
	}

	public function index()
	{
		$data['mails'] = $this->db->get('admin_notif')->result_array();
		$data['view'] = 'notif/index';
		$this->load->view('layout/layout', $data);
	}

	public function mail($id_notif)
	{
		$this->db->set('status', 1);
		$this->db->where('id', $id_notif);
		$this->db->update('admin_notif');
		$data['query'] = $this->db->query("SELECT * FROM admin_notif WHERE id = $id_notif")->result_array();
		$data['view'] = 'admin/notif/mail';
		$this->load->view('layout/layout', $data);
	}
}
