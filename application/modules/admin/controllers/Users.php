<?php defined('BASEPATH') or exit('No direct script access allowed');

class Users extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model', 'user_model');
		$this->load->model('profile/profile_model', 'profile_model');
		$this->load->library('datatable'); // loaded my custom serverside datatable library
		//$this->load->library('excel');
	}

	public function index($role = 0)
	{
		// periksa kategori
		$role = ($role != '') ? $role : '';

		$data['role'] = $role;
		$data['users'] = $this->user_model->get_users($role);
		$data['count_all'] = $this->user_model->count_all_users_by_role('');
		$data['count_admin'] = $this->user_model->count_all_users_by_role(4);
		$data['count_dp'] = $this->user_model->count_all_users_by_role(2);
		$data['count_residen'] = $this->user_model->count_all_users_by_role(3);
		$data['view'] = 'admin/users/user_list';

		$this->load->view('layout/layout', $data);
	}

	public function add()
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data['view'] = 'admin/users/user_add';
				$this->load->view('layout/layout', $data);
			} else {

				$data = array(
					'username' => $this->input->post('username'),
					'email' => $this->input->post('email'),
					'role' => $this->input->post('role'),
					'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
					'created_at' => date('Y-m-d : h:m:s'),
					'updated_at' => date('Y-m-d : h:m:s'),
				);

				$data = $this->security->xss_clean($data);
				$result = $this->user_model->add_user($data, $this->input->post('role'));

				//masukkan data ke residen_tahap
				if ($result) {
					$this->session->set_flashdata('msg', 'Pengguna berhasil ditambahkan!');
					redirect(base_url('admin/users'));
				}
			}
		} else {
			$data['roles'] = $this->user_model->get_roles();
			$data['view'] = 'admin/users/user_add';
			$this->load->view('layout/layout', $data);
		}
	}

	public function edit($id = 0)
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');

			if ($this->form_validation->run() == FALSE) {
				$data['user'] = $this->user_model->get_user_by_id($id);
				$data['view'] = 'admin/users/user_edit';
				$this->load->view('layout/layout', $data);
			} else {

				$data = array(
					'username' => $this->input->post('username'),
					'email' => $this->input->post('email'),
					'password' => ($this->input->post('password') !== "" ? password_hash($this->input->post('password'), PASSWORD_BCRYPT) : $this->input->post('password_hidden')),
					'updated_at' => date('Y-m-d : h:m:s'),
				);

				$data = $this->security->xss_clean($data);
				$result = $this->user_model->edit_user($data, $id);
				if ($this->input->post('role') == 2) {
					$link = "edit_staf";
				} else if ($this->input->post('role') == 3) {
					$link = "edit_profile";
				}
				if ($result) {
					$this->session->set_flashdata('msg', 'Pengguna berhasil diubah!');
					redirect(base_url("admin/users/$link/$id"));
				}
			}
		}
	}

	public function edit_profile($id)
	{
		if ($this->input->post('submit')) {

			$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('telp', 'Telepon', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('gender', 'Jenis Kelamin', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('pembimbing', 'Staf Pembimbing', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('nim', 'NIM', 'trim|required', array('required' => '%s wajib diisi.'));

			if ($this->form_validation->run() == FALSE) {
				$data['user'] = $this->user_model->get_user_by_id($id);
				$data['residen'] = $this->profile_model->get_residen_by_id($id);
				$data['title'] = 'Profil Pengguna';
				$data['view'] = "admin/users/user_edit";
				$data['staf'] = $this->db->get('dosen')->result_array();
				$this->load->view('layout/layout', $data);
			} else {

				$upload_path = './uploads/fotoProfil';

				if (!is_dir($upload_path)) {
					mkdir($upload_path, 0777, TRUE);
				}
				//$newName = "hrd-".date('Ymd-His');
				$config = array(
					'upload_path' => $upload_path,
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => FALSE,
				);

				$this->load->library('upload', $config);
				$this->upload->do_upload('photo');
				$photo = $this->upload->data();

				$data = array(
					'nama_lengkap' => $this->input->post('nama_lengkap'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tanggal_lahir' => $this->input->post('tanggal_lahir'),
					'nim' => $this->input->post('nim'),
					'angkatan' => $this->input->post('angkatan'),
					'alamat' => $this->input->post('alamat'),
					'telp' => $this->input->post('telp'),
					'gender' => $this->input->post('gender'),
					'photo' => ($photo['file_name']) !== "" ? $upload_path . '/' . $photo['file_name'] : $this->input->post('photo_hidden'),
					'id_pembimbing' => $this->input->post('pembimbing'),
				);

				$data = $this->security->xss_clean($data);
				$result = $this->profile_model->update_residen_by_id($data, $id);
				if ($result) {
					$this->session->set_flashdata('msg', 'Profil Residen berhasil diubah!');
					redirect(base_url("admin/users/edit_profile/$id"), 'refresh');
				}
			}
		} else {
			$data['user'] = $this->user_model->get_user_by_id($id);
			$data['residen'] = $this->profile_model->get_residen_by_id($id);
			$data['title'] = 'Profil Pengguna';
			$data['view'] = "admin/users/user_edit";
			$data['staf'] = $this->db->get('dosen')->result_array();
			$this->load->view('layout/layout', $data);
		}
	}
	public function edit_staf($id)
	{
		if ($this->input->post('submit')) {

			$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('nip', 'NIP', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('telp', 'Telepon', 'trim|required', array('required' => '%s wajib diisi.'));

			if ($this->form_validation->run() == FALSE) {
				$data['user'] = $this->user_model->get_user_by_id($id);
				$data['residen'] = $this->profile_model->get_residen_by_id($id);
				$data['staf'] = $this->profile_model->get_staf_by_id($id);
				$data['view'] = "admin/users/user_edit";
				$this->load->view('layout/layout', $data);
			} else {

				$upload_path = './uploads/fotoProfil';

				if (!is_dir($upload_path)) {
					mkdir($upload_path, 0777, TRUE);
				}
				//$newName = "hrd-".date('Ymd-His');
				$config = array(
					'upload_path' => $upload_path,
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => FALSE,
				);

				$this->load->library('upload', $config);
				$this->upload->do_upload('photo');
				$photo = $this->upload->data();

				$data = array(
					'nama_lengkap' => $this->input->post('nama_lengkap'),
					'nip' => $this->input->post('nip'),
					'alamat' => $this->input->post('alamat'),
					'telp' => $this->input->post('telp'),
					'photo' => ($photo['file_name']) !== "" ? $upload_path . '/' . $photo['file_name'] : $this->input->post('photo_hidden'),
				);

				$data = $this->security->xss_clean($data);
				$result = $this->profile_model->update_staf_by_id($data, $id);
				if ($result) {
					$this->session->set_flashdata('msg', 'Profil Staf berhasil diubah!');
					redirect(base_url("admin/users/edit_staf/$id"), 'refresh');
				}
			}
		} else {
			$data['user'] = $this->user_model->get_user_by_id($id);
			$data['residen'] = $this->profile_model->get_residen_by_id($id);
			$data['staf'] = $this->profile_model->get_staf_by_id($id);
			$data['view'] = "admin/users/user_edit";
			$this->load->view('layout/layout', $data);
		}
	}

	public function detail($id = 0)
	{
		$data['user'] = $this->user_model->get_user_by_id($id);

		$data['view'] = 'admin/users/detail';
		$this->load->view('layout/layout', $data);
	}

	public function del($id)
	{
		$this->db->delete('ci_users', array('id' => $id));
		$this->session->set_flashdata('msg', 'Pengguna berhasil dihapus!');
		redirect(base_url('admin/users'));
	}

	public function upload()
	{

		if (isset($_POST['submit'])) {

			$upload_path = './uploads/users';

			if (!is_dir($upload_path)) {
				mkdir($upload_path, 0777, TRUE);
			}

			$config = array(
				'upload_path' => $upload_path,
				'allowed_types' => "xlsx",
				'overwrite' => FALSE,
			);

			$this->load->library('upload', $config);
			$this->upload->do_upload('file');
			$upload = $this->upload->data();


			if ($upload) { // Jika proses upload sukses			    	

				$excelreader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				$loadexcel = $excelreader->load('./uploads/users/' . $upload['file_name']); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				$data['sheet'] = $sheet;
				$data['file_excel'] = $upload['file_name'];

				$data['title'] = 'Upload Pengguna';
				$data['view'] = 'admin/users/upload';

				$this->load->view('layout/layout', $data);
			} else {

				$data['title'] = 'Upload Pengguna';
				$data['view'] = 'admin/users/upload';
				$this->load->view('layout/layout', $data);
			}
		} else {

			$data['title'] = 'Upload Pengguna';
			$data['view'] = 'admin/users/upload';
			$this->load->view('layout/layout', $data);
		}
	}

	public function import($file_excel)
	{

		$excelreader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		$loadexcel = $excelreader->load('./uploads/users/' . $file_excel); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		$data_user = array();
		$user_detail = array();

		$numrow = 1;
		foreach ($sheet as $row) {

			if ($numrow > 1) {

				if ($row['E'] == "residen") {
					$role = 3;
				} else if ($row['E'] == "staf") {
					$role = 2;
				} else {
					$role = '';
				}

				$duplikat = cek_duplikat_user($row['A'], $row['C']);

				if ($duplikat == 0) {
					// Kita push (add) array data ke variabel data
					array_push($data_user, array(
						'password' => password_hash($row['A'], PASSWORD_BCRYPT),
						'username' => $row['A'],
						'email' => $row['C'],
						'created_at' => date('Y-m-d : h:m:s'),
						'nama_lengkap' => $row['B'],
						'telp' => $row['D'],
						'role' => $role,
						'password' => password_hash($row['F'], PASSWORD_BCRYPT),
					));
				}
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		$this->user_model->import_users($data_user);

		redirect("admin/users"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}
}
