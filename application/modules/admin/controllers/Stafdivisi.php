<?php defined('BASEPATH') or exit('No direct script access allowed');
class Stafdivisi extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		// $this->load->model('admin/user_model', 'user_model');
		// $this->load->model('admin/profile_model', 'profile_model');
		$this->load->model('Dosenpj_model', 'dosenpj_model');
		$this->load->library('datatable'); // loaded my custom serverside datatable library
	}

	public function index()
	{
		$data['query'] = $this->db->query("SELECT sd.*,dsn.nama_lengkap,dvs.divisi FROM staf_divisi sd LEFT JOIN dosen dsn ON sd.id_dosen = dsn.id LEFT JOIN divisi dvs on sd.id_divisi = dvs.id WHERE sd.admin_divisi = 0")->result_array();
		$data['view'] = 'stafdivisi/index.php';
		$this->load->view('layout/layout', $data);
	}

	public function tambah()
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules(
				'dosen',
				'Dosen',
				'trim|required',
				array(
					'required' => '%s wajib diisi'
				)
			);
			$this->form_validation->set_rules(
				'divisi',
				'Divisi',
				'trim|required',
				array(
					'required' => '%s wajib diisi'
				)
			);
			if ($this->form_validation->run() == false) {
				$data['divisi'] = $this->db->query("SELECT * FROM divisi WHERE divisi.id NOT IN (SELECT id_divisi FROM staf_divisi)")->result_array();
				$data['dosen'] = $this->db->query("SELECT * FROM dosen WHERE dosen.id NOT IN (SELECT id_dosen FROM pj_tahap)")->result_array();
				$data['view'] = 'stafdivisi/tambah.php';
				//  AND dosen.id NOT IN (SELECT id_dosen FROM staf_divisi)
				$this->load->view('layout/layout', $data);
			} else {
				$data = array(
					'id_dosen' => $this->input->post('dosen'),
					'id_divisi' => $this->input->post('divisi'),
					'admin_divisi' => $this->input->post('is_admin')
				);

				$data = $this->security->xss_clean($data);
				$result = $this->dosenpj_model->tambah_stafdivisi($data);

				if ($result) {
					$this->session->set_flashdata('msg', 'Staf Divisi berhasil ditambahkan!');
					redirect(base_url('admin/stafdivisi'));
				}
			}
		} else {
			//	$data['divisi'] = $this->db->query("SELECT * FROM divisi WHERE divisi.id NOT IN (SELECT id_divisi FROM staf_divisi)")->result_array();
			$data['divisi'] = $this->db->query("SELECT * FROM divisi")->result_array();
			$data['dosen'] = $this->db->query("SELECT * FROM dosen WHERE dosen.id NOT IN (SELECT id_dosen FROM pj_tahap) AND dosen.id NOT IN (SELECT id_dosen FROM staf_divisi)")->result_array();
			$data['view'] = 'stafdivisi/tambah.php';
			$this->load->view('layout/layout', $data);
		}
	}

	public function hapus($id)
	{
		$this->db->delete('staf_divisi', array('id' => $id));
		$this->session->set_flashdata('msg', 'Data berhasil dihapus!');
		redirect(base_url('admin/stafdivisi'));
	}

	public function akademik()
	{
		$data['query'] = $this->db->query("SELECT sd.*,dsn.nama_lengkap,dvs.divisi FROM staf_divisi sd LEFT JOIN dosen dsn ON sd.id_dosen = dsn.id LEFT JOIN divisi dvs on sd.id_divisi = dvs.id WHERE sd.admin_divisi = 0")->result_array();
		$data['view'] = 'stafdivisi/akademik';
		$this->load->view('layout/layout', $data);
	}
}
