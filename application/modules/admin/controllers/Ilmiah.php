<?php defined('BASEPATH') or exit('No direct script access allowed');
class Ilmiah extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ilmiah_model', 'ilmiah_model');
		$this->load->model('residen_model', 'residen_model');
		$this->load->model('kategori_model', 'kategori_model');
		$this->load->model('divisi_model', 'divisi_model');
		$this->load->model('Notif_model', 'notif_model');
	}

	public function index()
	{
		if ($_SESSION['role'] == 2) {
			$id_user = $_SESSION['user_id'];
			$id_staf = $this->db->get_where('dosen', array('user_id' => $id_user))->row()->id;
			$divisi_staf = $this->db->get_where('staf_divisi', array('id_dosen' => $id_staf))->row()->id_divisi;
			$query = $this->db->query("SELECT i.id, i.judul_ilmiah, i.date, k.kategori, rt.tahap, r.nama_lengkap, d.nama_lengkap AS nama_dosen FROM ilmiah i
			LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
			LEFT JOIN residen r ON r.id=i.id_residen		
			LEFT JOIN dosen d ON d.id = i.id_staf
			LEFT JOIN divisi dvs ON dvs.id = i.id_divisi
			LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap 
			");
			// WHERE i.id_divisi = $divisi_staf
			$result = $query->result_array();
			$data['nama_dosen'] = $this->db->get_where('dosen', array('user_id' => $id_user))->row()->nama_lengkap;
		} else {
			$query = $this->db->query("SELECT  i.judul_ilmiah, i.date, k.kategori, rt.tahap, r.nama_lengkap FROM ilmiah i
			LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
			LEFT JOIN residen r ON r.id=i.id_residen		
			LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap		
			");
			$result = $query->result_array();
		}

		$data['query'] = $result;
		$data['id_menu'] = "ilmiah";
		$data['class_menu'] = "index";
		$data['title'] = 'Ilmiah';
		//$data['deskripsi'] = 'Tampilkan semua ilmiah dari semua residen. dan semua tahap';
		$data['view'] = 'ilmiah/index.php';
		$this->load->view('layout/layout', $data);
	}

	public function tahap($tahap)
	{
		$query = $this->db->query("SELECT i.id, i.judul_ilmiah, i.date, k.kategori, rt.tahap, r.nama_lengkap FROM ilmiah i
		LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
		LEFT JOIN residen r ON r.id=i.id_residen		
		LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap	
		WHERE i.id_tahap=$tahap AND i.file_nilai <> '' AND i.status = 1 
		");
		$result = $query->result_array();

		$data['query'] = $result;
		$data['id_menu'] = "ilmiah";
		$data['class_menu'] = "itahap" . $tahap;
		$data['title'] = 'Ilmiah Tahap ' . $tahap;
		$data['tahap_id'] = $tahap;
		$data['view'] = 'ilmiah/ilmiahByTahap.php';
		$this->load->view('layout/layout', $data);
	}

	public function divisi()
	{
		$query = $this->db->query("SELECT i.judul_ilmiah, i.date, k.kategori, rt.tahap, r.nama_lengkap FROM ilmiah i
		LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
		LEFT JOIN residen r ON r.id=i.id_residen		
		LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap	
		WHERE (rt.tahap='2a' OR rt.tahap='2b')
		");
		$result = $query->result_array();

		$data['query'] = $result;
		$data['id_menu'] = "ilmiah";
		$data['class_menu'] = "divisi";
		$data['title'] = 'Ilmiah Semua Divisi ';
		$data['deskripsi'] = 'Tampilkan semua ilmiah dari semua residen berdasarkan Divisi. Yg tampil hanya tahap 2a dan 2b saja';
		$data['view'] = 'ilmiah/index.php';
		$this->load->view('layout/layout', $data);
	}

	public function detail($id)
	{
		$author = $this->residen_model->get_author_of_ilmiah($id);
		$query = $this->db->query("SELECT *, DATE_FORMAT(date, '%d %M %Y') as date, DATE_FORMAT(tgl_maju, '%d %M %Y') as tgl_maju FROM ilmiah WHERE id = " . $id);
		$result = $query->row_array();
		$result2 = $query->row();
		$id_tahap = $result2->id_tahap;
		$id_residen = $query->row()->id_residen;

		$query2 = $this->db->get_where('tahap', array('id' => $id_tahap));
		$tahap_result = $query2->row();
		$tahap = $tahap_result->tahap;

		$query3 = $this->db->get_where('residen', array('id' => $id_residen));
		$res_pic = $query3->row()->photo;

		$data['ilmiah'] = $result;
		$data['res_pic'] = $res_pic;
		$data['title'] = 'Ilmiah Tahap ' . $tahap;
		$data['author'] = $author;
		$data['id_menu'] = 'ilmiah';
		$data['for_breadcrumb'] = $id_tahap;
		$data['class_menu'] = 'tahap' . $id_tahap;
		$data['view'] = 'ilmiah/detail.php';
		$this->load->view('layout/layout', $data);
	}

	public function konfirm($id, $stats)
	{
		$query = $this->db->get_where('ilmiah', array('id' => $id));
		$result = $query->row();
		$status = $result->status;
		$nilai = $result->nilai;
		$refered_from =  $this->session->userdata('referred_from');

		if ($status != 1 && $nilai != null && $stats == 1) {
			$this->db->where('id', $id);
			$this->db->update('ilmiah', array('status' => $stats));

			$residen = $this->db->query("SELECT i.id_residen,r.nama_lengkap FROM ilmiah i LEFT JOIN residen r ON i.id_residen = r.id WHERE i.id = $id")->result_array();
			foreach ($residen as $residen) {
			}
			$nama_residen = $residen['nama_lengkap'];
			$judul_ilmiah = $this->db->query("SELECT * FROM ilmiah WHERE id = $id")->row()->judul_ilmiah;
			$sender = $_SESSION['user_id'];
			$data = array(
				'id_residen' => $residen['id_residen'],
				'subject' => 'Konfirmasi Ilmiah',
				'content' => "Pemberitahuan kepada residen $nama_residen, Ilmiah anda yang berjudul $judul_ilmiah telah dikonfirmasi oleh admin.",
				'status' => 0,
				'sender' => $sender
			);

			// dump($data);
			$this->notif_model->send_notif($data);
			$this->session->set_flashdata('msg', 'Ilmiah berhasil dikonfirmasi!');
			redirect(base_url('admin/ilmiah/detail/' . $id));
		} elseif ($status != 1 && $status != 2 && $stats == 2) {
			$this->db->where('id', $id);
			$this->db->update('ilmiah', array('status' => $stats));
			$this->session->set_flashdata('msg', 'Permintaan revisi berhasil dikirim!');
			redirect(base_url('dosen/ilmiah/detail/' . $id));
		} elseif ($status == 1) {
		} elseif ($nilai == null) {
			$this->session->set_flashdata('wrn', 'Dokumen ilmiah belum lengkap!');
			redirect(base_url('dosen/ilmiah/detail/' . $id));
			$this->session->set_flashdata('wrn', 'Ilmiah sudah dikonfirmasi!');
			redirect(base_url('dosen/ilmiah/detail/' . $id));
		}
	}

	public function destroy($id)
	{
		$query = $this->db->get_where('ilmiah', array('id' => $id));
		$tahap_ilmiah = $query->row()->id_tahap;
		$path_file = $query->row_array();
		$file_nilai = $query->row_array();
		$file_presensi = $query->row_array();

		if (realpath($path_file['file'])) {
			unlink(realpath($path_file['file']));
		}
		if (realpath($file_nilai['file_nilai'])) {
			unlink(realpath($file_nilai['file_nilai']));
		}
		if (realpath($file_presensi['file_presensi'])) {
			unlink(realpath($file_presensi['file_presensi']));
		}
		$this->db->delete('ilmiah', array('id' => $id));
		$this->session->set_flashdata('msg', 'Ilmiah berhasil dihapus!');
		redirect(base_url('admin/ilmiah/tahap/' . $tahap_ilmiah));
	}
}
