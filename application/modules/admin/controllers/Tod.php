<?php defined('BASEPATH') or exit('No direct script access allowed');
class Tod extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tod_model', 'tod_model');
		// $this->load->model('ilmiah_model', 'ilmiah_model');
		// $this->load->model('residen_model', 'residen_model');
		// $this->load->model('divisi_model', 'divisi_model');
		// $this->load->model('tahap_model', 'tahap_model');
		// $this->load->model('kategori_model', 'kategori_model');
		$this->load->model('divisi_model', 'divisi_model');
	}

	public function index()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url('admin/tod/index');
		$config['total_rows'] = $this->db->get('tod')->num_rows();
		$config['per_page'] = 5;
		$config['full_tag_open'] = '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		$config['attributes'] = array('class' => 'page-link');

		$this->pagination->initialize($config);
		// var_dump($config['base_url']);
		// die;
		$data['start'] = $this->uri->segment(4) == "" ? 0 : $this->uri->segment(4);
		$data['query'] = $this->tod_model->get_all_tod($data['start'], $config['per_page']);


		$query = $this->db->query('select MONTHNAME(start_date) as start_date, MONTHNAME(end_date) as end_date,id from tod');
		$results = $query->result_array();

		$data['tods'] = $results;
		$data['divisi'] = $this->divisi_model->get_all_divisi();
		$data['view'] = 'admin/tod/index';
		$this->load->view('layout/layout', $data);
	}

	public function tambah_tod()
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('start_date', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('end_date', 'Tanggal Selesai', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$data['view'] = 'admin/tod/tambah_tod';
				$this->load->view('layout/layout', $data);
			} else {
				$data = array(
					'start_date' => $this->input->post('start_date'),
					'end_date' => $this->input->post('end_date'),
				);

				$data = $this->security->xss_clean($data);
				$result = $this->tod_model->tambah_tod($data);

				if ($result) {
					$this->session->set_flashdata('msg', 'TOD berhasil ditambahkan!');
					redirect(base_url('admin/tod'));
				}
			}
		} else {
			$data['view'] = 'admin/tod/tambah_tod';
			$this->load->view('layout/layout', $data);
		}
	}

	public function residen_per_tod($id_tod, $id_divisi)
	{
		$residen = $this->tod_model->get_residen_per_tod($id_tod, $id_divisi);
		echo json_encode($residen);
	}

	public function get_tod_by_id()
	{
		$id = $this->input->post('id');
		$tod = $this->db->get('tod', array('id' => $id))->result_array();
		echo json_encode($tod);
	}

	public function update_tod($id)
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('start_date', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('end_date', 'Tanggal selesai', 'trim|required');


			if ($this->form_validation->run() == FALSE) {
				$query = $this->db->get_where('tod', array('id' => $id));
				$result = $query->result_array();
				$result1 = $query->row();

				$id_tod = $result1->id;

				$data['query'] = $result;
				$data['id_tod'] = $id_tod;
				$data['view'] = 'admin/tod/ubah_tod.php';
				$this->load->view('layout/layout', $data);
			} else {
				$data = array(
					'start_date' => $this->input->post('start_date'),
					'end_date' => $this->input->post('end_date'),
				);

				$data = $this->security->xss_clean($data);

				$result = $this->tod_model->update_tod($id, $data);

				if ($result) {
					$this->session->set_flashdata('msg', 'Data TOD berhasil diubah');
					redirect(base_url('admin/tod'));
				}
			}
		} else {
			$query = $this->db->get_where('tod', array('id' => $id));
			$result = $query->result_array();
			$result1 = $query->row();

			$id_tod = $result1->id;

			$data['query'] = $result;
			$data['id_tod'] = $id_tod;
			$data['view'] = 'admin/tod/ubah_tod.php';
			$this->load->view('layout/layout', $data);
		}
	}
}
