<?php defined('BASEPATH') or exit('No direct script access allowed');
class Residen extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ilmiah_model', 'ilmiah_model');
		$this->load->model('residen_model', 'residen_model');
		$this->load->model('divisi_model', 'divisi_model');
		$this->load->model('tahap_model', 'tahap_model');
		$this->load->model('kategori_model', 'kategori_model');
		$this->load->model('Notif_model', 'notif_model');

		// $this->load->model('divisi_model', 'divisi_model');
	}

	public function index()
	{
		$query = $this->db->query('select nama_lengkap, nim from residen');
		$result = $query->result_array();

		$data['query'] = $result;
		$data['id_menu'] = "residen";
		$data['class_menu'] = "index";
		$data['title'] = 'Residen';
		$data['type'] = 'all_residen';
		$data['deskripsi'] = 'Tampilkan semua residen baik yang masih aktif maupun yng sudah lulus';
		$data['view'] = 'residen/index';
		$this->load->view('layout/layout', $data);
	}

	public function tahap($id_tahap, $id_divisi = 0)
	{
		// $query = $this->db->query("SELECT r.nama_lengkap, r.nim, r.id,
		// (select rt.tahap from residen_tahap rt where rt.id_residen = r.id order by rt.id desc limit 1) as tahap 
		// from residen r		
		// ");
		// $result = $this->residen_model->get_residen_by_tahap();
		// $id_residen = $result['id'];
		// echo $id_residen; 

		$data['query'] = $this->residen_model->get_residen_by_tahap_and_divisi($id_tahap, $id_divisi);
		$data['tahap_id'] = $id_tahap;
		$data['id_menu'] = "residen";
		$data['class_menu'] = "tahap" . $id_tahap;
		$data['title'] = 'Residen';
		$data['type'] = 'residen_by_tahap';
		// $data['deskripsi'] = 'Tampilkan semua residen aktif dan Tahap yang ditempuh saat ini';
		$data['view'] = 'residen/index';
		$this->load->view('layout/layout', $data);
	}

	public function detail($id_residen, $id_tahap = 1, $id_divisi = 0)
	{

		$residen_tahap = $this->tahap_model->get_residen_tahap($id_residen);
		$progress = $this->ilmiah_model->count_ilmiah_progress($id_residen);
		$approved_ilmiah = $this->ilmiah_model->count_approved_ilmiah($id_residen);
		$jumlah_kategori_per_tahap = $this->kategori_model->get_jumlah_kategori_by_tahap($id_residen);
		$approved_progress = ($residen_tahap == 1 ? $approved_ilmiah / 8  * 100 : $approved_ilmiah / $jumlah_kategori_per_tahap  * 100);
		// $approved_progress = $approved_ilmiah / $jumlah_kategori_per_tahap  * 100;

		if ($residen_tahap == 2 or $residen_tahap == 3) {
			$data['divisi'] = $this->divisi_model->get_divisi_by_id_residen($id_residen);
			$data['approved_progress'] = $this->divisi_model->get_tahap2_progress($id_residen, $residen_tahap);
			$data['progress'] = $progress;
		} else {
			$data['approved_progress'] = number_format($approved_progress, 0);
			$data['progress'] = $progress;
		}
		// $data['progress'] = $progress;
		$data['residen'] = $this->residen_model->get_residen_by_id($id_residen);
		$data['residen_tahap'] = $this->tahap_model->get_tahap_by_id($residen_tahap);
		$data['residen_tahap_id'] = $residen_tahap;
		$data['tahap'] = $this->tahap_model->get_all_tahap();
		$data['ilmiah'] = $this->ilmiah_model->get_residen_ilmiah($id_residen, $id_tahap, $id_divisi);
		// $data['progress'] = $this->ilmiah_model->count_ilmiah_progress($id_residen);
		$data['residen_divisi'] = $this->divisi_model->get_residen_divisi($id_residen);
		$data['jumlah_residen_divisi'] = $this->divisi_model->count_residen_divisi($id_residen);
		$data['all_divisi'] = $this->divisi_model->get_all_divisi();
		$data['view'] = 'residen/detail';
		$this->load->view('layout/layout', $data);
	}

	public function tahap_by_residen($id_residen)
	{
		$query = $this->db->query("SELECT rt.*, r.nama_lengkap FROM residen_tahap rt
		LEFT JOIN residen r ON r.id=rt.id_residen 
		WHERE rt.id_residen=$id_residen
		");
		$result = $query->result_array();

		$data['query'] = $result;
		$data['id_menu'] = "residen";
		$data['class_menu'] = "tahap-residen";
		$data['title'] = 'Tahap spesifik Residen';
		$data['type'] = 'tahap_spesifik_residen';
		$data['deskripsi'] = 'Tampilkan semua tahap spesifik residen';
		$data['view'] = 'residen/index';
		$this->load->view('layout/layout', $data);
	}

	public function divisi()
	{
		$query = $this->db->query("select r.nama_lengkap, r.nim, rt.tahap, rto.id_tod, rto.id_divisi, d.divisi, CONCAT(DATE_FORMAT(tod.start_date, '%M %Y') , ' - ', DATE_FORMAT(tod.end_date, '%M %Y')) as periode
		from residen r
		LEFT JOIN residen_tahap rt ON rt.id_residen = r.id
		LEFT JOIN residen_tod rto ON rto.id_residen = r.id
		JOIN divisi d ON rto.id_divisi = d.id
		JOIN tod tod ON rto.id_tod = tod.id
		WHERE (rt.tahap='2a' OR rt.tahap='2b')	
		");
		$result = $query->result_array();

		$data['query'] = $result;
		$data['id_menu'] = "residen";
		$data['class_menu'] = "residen-divisi";
		$data['title'] = 'Residen by Divisi';
		$data['type'] = 'residen_by_divisi';
		$data['deskripsi'] = 'Tampilkan semua residen yang sudah menempuh tahap 2a dan 2b serta divisinya yg ditempuh saat ini';
		$data['view'] = 'residen/index';
		$this->load->view('layout/layout', $data);
	}

	public function divisi_by_residen($id_residen)
	{
		$query = $this->db->query("SELECT rd.*,r.nama_lengkap FROM residen_divisi rd
		LEFT JOIN residen r ON r.id=rd.id_residen
		WHERE rd.id_residen=$id_residen
		");
		$result = $query->result_array();

		$data['query'] = $result;
		$data['id_menu'] = "residen";
		$data['class_menu'] = "divisi-residen";
		$data['title'] = 'Divisi spesifik Residen';
		$data['type'] = 'divisi_spesifik_residen';
		$data['deskripsi'] = 'Tampilkan semua divisi spesifik residen';
		$data['view'] = 'residen/index';
		$this->load->view('layout/layout', $data);
	}

	public function lobby($id_tahap = 2, $id_divisi = 12)
	{
		$data['query'] = $this->residen_model->residen_lobby($id_tahap, $id_divisi);
		$data['daftar_divisi'] = $this->divisi_model->get_all_divisi();
		$data['tahap_id'] = $id_tahap;
		$data['id_menu'] = "residen";
		$data['btn_class'] = $id_tahap;
		$data['class_menu'] = "lobby";
		$data['title'] = 'Residen by Tahap';
		$data['type'] = 'residen_by_tahap';
		// $data['deskripsi'] = 'Tampilkan semua residen aktif dan Tahap yang ditempuh saat ini';
		$data['view'] = 'residen/lobby';
		$this->load->view('layout/layout', $data);
	}

	public function pilih_divisi($id_residen)
	{
		$residen = $this->residen_model->get_residen_by_id($id_residen);
		echo json_encode($residen);
	}

	public function not_residen_divisi($id_residen)
	{
		$divisi = $this->residen_model->get_bukan_residen_divisi($id_residen);
		echo json_encode($divisi);
	}

	public function submit_divisi($residen_id)
	{
		$referred_from = $this->session->userdata('referred_from');
		$data = array(
			'id_divisi' => $this->input->post('divisi'),
		);
		$data = $this->security->xss_clean($data);
		$result = $this->residen_model->update_residen_divisi($residen_id, $data);
		if ($result) {
			$this->session->set_flashdata('msg', 'Divisi residen berhasil diubah!');
			redirect($referred_from);
		}
		// echo $residen_id;
	}

	function if_sukses_naik_tahap($id_residen, $tahap)
	{
		$residen = $this->db->get_where('residen', array('id' => $id_residen))->row()->nama_lengkap;
		$data = array(
			'id_residen' => $id_residen,
			'sender' => $_SESSION['user_id'],
			'subject' => 'Kenaikan Tahap',
			'content' => "Pemberitahuan kepada residen $residen, bahwa anda dinyatakan naik ke tahap $tahap."
		);
		$this->notif_model->send_notif($data);
		return true;
	}

	public function naiktahap($id_residen, $id_tahap = 1, $id_divisi = 0)
	{
		$residen_tahap = $this->tahap_model->get_residen_tahap($id_residen);
		date_default_timezone_set("Asia/Bangkok");
		$jumlah_divisi_residen = $this->divisi_model->count_residen_divisi($id_residen);
		$jumlah_kategori_per_tahap = $this->kategori_model->get_jumlah_kategori_by_tahap($id_residen);
		$approved_ilmiah = $this->ilmiah_model->count_approved_ilmiah($id_residen);

		if ($residen_tahap < 4) {
			if ($residen_tahap == 1) {
				if ($approved_ilmiah == 8) {
					$this->residen_model->tahap_2($id_residen);
					$this->if_sukses_naik_tahap($id_residen, '2A');
					$this->session->set_flashdata('msg', 'Residen naik ke tahap selanjutnya!');
					redirect(base_url('admin/residen/lobby'));
				} else {
					$this->session->set_flashdata('wrn', 'Residen belum memenuhi syarat untuk naik ke tahap selanjutnya');
					redirect(base_url('admin/residen/detail/' . $id_residen . '/' . $id_tahap . '/' . $id_divisi));
				}
			} else {
				if ($approved_ilmiah == $jumlah_kategori_per_tahap) {
					if ($residen_tahap == 2) {
						if ($jumlah_divisi_residen == 6) {
							$this->residen_model->tahap_2($id_residen);
							$this->if_sukses_naik_tahap($id_residen, '2B');
							$this->session->set_flashdata('msg', 'Residen naik ke tahap selanjutnya!');
							redirect(base_url('admin/residen/lobby'));
						} else {
							$this->session->set_flashdata('wrn', 'Residen belum menyelesaikan semua divisi');
							redirect(base_url('admin/residen/detail/' . $id_residen . '/' . $id_tahap . '/' . $id_divisi));
						}
					} elseif ($residen_tahap == 1) {
						$this->residen_model->tahap_2($id_residen);
						$this->if_sukses_naik_tahap($id_residen, '2A');

						$this->session->set_flashdata('msg', 'Residen naik ke tahap selanjutnya!');
						redirect(base_url('admin/residen/lobby'));
					} else {
						$this->resden_model->tahap_3($id_residen);
						$this->if_sukses_naik_tahap($id_residen, '3');
						$this->session->set_flashdata('msg', 'Residen naik ke tahap selanjutnya!');
						redirect(base_url('admin/residen/detail/' . $id_residen . '/' . $id_tahap . '/' . $id_divisi));
					}
				} else {
					$this->session->set_flashdata('wrn', 'Residen belum memenuhi syarat untuk naik ke tahap selanjutnya');
					redirect(base_url('admin/residen/detail/' . $id_residen . '/' . $id_tahap . '/' . $id_divisi));
				}
			}
		} else {
			$this->session->set_flashdata('wrn', 'Residen sudah mencapai tahap maksimal');
			redirect(base_url('admin/residen/detail/' . $id_residen . '/' . $id_tahap . '/' . $id_divisi));
		}
	}

	public function naiktahapdivisi($id_residen, $id_tahap = 1, $id_divisi = 0)
	{
		date_default_timezone_set("Asia/Bangkok");
		$jumlah_kategori_per_tahap = $this->kategori_model->get_jumlah_kategori_by_tahap($id_residen);
		$approved_ilmiah = $this->ilmiah_model->count_approved_ilmiah($id_residen);
		$residen_tahap = $this->tahap_model->get_residen_tahap($id_residen);
		if ($residen_tahap > 4) {
			if ($approved_ilmiah == $jumlah_kategori_per_tahap) {
				$this->residen_model->residen_naik_tahap($id_residen);
			} else {
				echo "belum memenuhi syarat";
			}
		} else {
			echo "tahap max";
		}
	}

	public function divisi_selesai($id_residen, $id_tahap, $id_divisi)
	{
		// date_default_timezone_set("Asia/Bangkok");
		$approved_ilmiah = $this->ilmiah_model->count_approved_ilmiah($id_residen);
		$residen_tahap = $this->tahap_model->get_residen_tahap($id_residen);
		$referred_from = $this->session->userdata('referred_from');

		if ($residen_tahap == 2 || $residen_tahap == 3) {
			if ($approved_ilmiah == 12) {
				$this->residen_model->divisi_selesai($id_residen);
				$this->session->set_flashdata('msg', 'Residen telah menyelesaikan divisi');
				redirect(base_url('admin/residen/detail/' . $id_residen . '/' . $id_tahap . '/' . $id_divisi));
			} else {
				$this->session->set_flashdata('wrn', 'Residen belum memenuhi persyaratan');
				redirect(base_url('admin/residen/detail/' . $id_residen . '/' . $id_tahap . '/' . $id_divisi));
			}
		} elseif ($residen_tahap == 4) {
			if ($approved_ilmiah == 9) {
				echo "naik tahap";
			} else {
				$this->session->set_flashdata('wrn', 'Residen sudah menyelesaikan semua divisi');
				redirect(base_url('admin/residen/detail/' . $id_residen . '/' . $id_tahap . '/' . $id_divisi));
			}
		}
	}
}
