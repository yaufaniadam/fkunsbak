<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Dashboard extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('residen_model', 'residen_model');
	}

	public function index()
	{

		// dd($_SESSION);
		if ($_SESSION['role'] == 2) {
			$id_user = $_SESSION['user_id'];
			$id_staf = $this->db->get_where('dosen', array('user_id' => $id_user))->row()->id;
			$divisi_staf = $this->db->get_where('staf_divisi', array('id_dosen' => $id_staf))->row()->id_divisi;
			$data['jumlah_ilmiah_tahap_1'] = $this->db->get_where('ilmiah', array('id_divisi' => $divisi_staf, 'id_tahap' => 1))->num_rows();
			$data['jumlah_ilmiah_tahap_2'] = $this->db->get_where('ilmiah', array('id_divisi' => $divisi_staf, 'id_tahap' => !1))->num_rows();
			$data['jumlah_residen'] = $this->db->query("SELECT DISTINCT * FROM residen_divisi WHERE id_divisi = $divisi_staf")->num_rows();
		}
		$data['tahap'] = $this->db->get('tahap')->result_array();
		$data['view'] = 'dashboard/index';
		$this->load->view('layout/layout', $data);
	}

	public function printpdf()
	{

		$query = $this->db->query('select nama_lengkap, nim from residen');
		$results = $query->result_array();

		$html = '<h1>Hello world!</h1>';
		$html .= '<table style="border: 1px solid red"><tr><td>Nama</td></tr>';

		foreach ($results as $result) {
			$html .= '<tr><td>' . $result['nama_lengkap'] . '</td></tr>';
		}
		$html .= '</table>';



		$mpdf = new \Mpdf\Mpdf();
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
}
