<?php defined('BASEPATH') or exit('No direct script access allowed');
class Rekap extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/user_model', 'user_model');
		// $this->load->model('admin/profile_model', 'profile_model');
		// $this->load->library('datatable'); // loaded my custom serverside datatable library
	}


	public function download($id_residen, $tahap_residen, $divisi_rekap)
	{
		// if ($tahap_residen == 2 || $tahap_residen == 3) {
		// 	$divisi_residen = $this->db->query("SELECT divisi FROM divisi d LEFT JOIN residen_divisi rd ON d.id = rd.id_divisi WHERE rd.id = (SELECT MAX(id) FROM residen_divisi WHERE id_residen = $id_residen)")->row()->divisi;
		// }
		$data_residen = $this->db->query("SELECT * FROM residen WHERE id = $id_residen")->row();

		$penanggungjawab = $this->db->query("SELECT pj.*,dsn.nama_lengkap FROM `pj_tahap` pj LEFT JOIN dosen dsn ON pj.id_dosen = dsn.id")->result_array();

		if ($tahap_residen == 1 || $tahap_residen == 4) {
			$jumlah_kategori = $this->db->query("SELECT * FROM `kategori_tahap` WHERE id_tahap = $tahap_residen")->num_rows();
			$total_nilai = $this->db->query("SELECT SUM(nilai) AS nilai FROM ilmiah WHERE id_tahap = $tahap_residen AND id_residen = $id_residen")->row()->nilai;
			$nilai_rata_rata = $total_nilai / $jumlah_kategori;
			$daftar_ilmiah = $this->db->query("SELECT * FROM ilmiah WHERE id_residen = $id_residen AND id_tahap = $tahap_residen")->result_array();
		} else {
			$jumlah_kategori = $this->db->query("SELECT * FROM `kategori_tahap` WHERE id_tahap = $tahap_residen")->num_rows();
			$total_nilai = $this->db->query("SELECT SUM(nilai) AS nilai FROM ilmiah WHERE id_residen = $id_residen AND id_tahap = $tahap_residen AND id_divisi = $divisi_rekap")->row()->nilai;
			$nilai_rata_rata = $total_nilai / $jumlah_kategori;
			$daftar_ilmiah = $this->db->query("SELECT * FROM ilmiah WHERE id_residen = $id_residen AND id_tahap = $tahap_residen AND id_divisi = $divisi_rekap")->result_array();
		}

		$css = "<style>
		.text-center{ text-align:center;
		}
		
		</style>";
		$html = '<h1 class="text-center">REKAP ILMIAH<br>PPDS I ILMU PENYAKIT DALAM</h1>';

		$html .= '<figure class="text-center"><img src="' . base_url($data_residen->photo) . '" /></figure>';
		$html .= '<table style="width:60%; border:1px solid #ddd; margin : auto;">
		<tbody>
		<tr>
		<td width="170"><strong>Nama </strong></td>
		<td width="431"><strong>: ' . $data_residen->nama_lengkap . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>NIM </strong></td>
		<td width="431"><strong>: ' . $data_residen->nim . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>TTL </strong></td>
		<td width="431"><strong>: ' . $data_residen->tempat_lahir . '/' . $data_residen->tanggal_lahir . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>Alamat </strong></td>
		<td width="431"><strong>: ' . $data_residen->alamat . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>Awal Pendidikan </strong></td>
		<td width="431"><strong>: ' . $data_residen->angkatan . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>No. HP </strong></td>
		<td width="431"><strong>: ' . $data_residen->telp  . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>Tahap</strong></td>
		<td width="431"><strong>: ' . $tahap_residen  . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>Nilai rata-rata</strong></td>
		<td width="431"><strong>: ' . $nilai_rata_rata  . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>Nilai Total</strong></td>
		<td width="431"><strong>: ' . $total_nilai  . '</strong></td>
		</tr>
		</tbody>
		</table>
		';

		$html .= '<table style="width:60%; border:1px solid #ddd; margin : auto;"><thead><tr><th>Judul Ilmiah</th>';
		$html .= '<th>Kategori Ilmiah</th>';
		if ($tahap_residen == 1) {
			$html .= '<th>Pembimbing</th>';
		}
		$html .= '<th>Nilai</th>';
		$html .= '</tr></thead>';
		foreach ($daftar_ilmiah as $ilmiah) {
			$html .= '<tr><td>' . $ilmiah['judul_ilmiah'] . '</td>';
			$html .= '<td>' . $this->db->get_where('kategori_ilmiah', array('id' => $ilmiah['id_kategori']))->row()->kategori . '</td>';
			if ($tahap_residen == 1) {
				$html .= '<td>' . $this->db->get_where('dosen', array('id' => $ilmiah['id_staf']))->row()->nama_lengkap . '</td>';
			}
			$html .= '<td>' . $ilmiah['nilai'] . '</td>';
			$html .= '</tr>';
		}

		$html .= '</table>';


		$html .= '<table style="width:60%; border:1px solid #ddd; margin : auto;">
		<tbody>
		<tr>
		<td width="200">&nbsp;</td>
		<td width="200">Mengetahui, Penanggung Jawab Tahap</td>
		<td width="200">&nbsp;</td>
		</tr>';

		$i = 0;

		foreach ($penanggungjawab as $pj) {
			$i++;
			if ($i % 2 == 1) {
				$html .= '<tr><td>' . $pj['nama_lengkap'] . '</td>';
			} else {
				$html .= '<td>' . $pj['nama_lengkap'] . '</td></tr>';
			}
		}

		// $i = 1;
		// foreach ($penanggungjawab as $penanggungjawab) {
		// 	$i++;
		// 	$html .= '
		// 	<tr>
		// 	<td width="200">(' . $penanggungjawab['nama_lengkap'] . ')</td>
		// 	<td width="200">&nbsp;</td>
		//  <td width="200">(' . $penanggungjawab['nama_lengkap'] . ')</td>
		// 	</tr>
		// 	<tr>
		// 	<td width="200">&nbsp;</td>
		// 	<td width="200">&nbsp;</td>
		// 	<td width="200">&nbsp;</td>
		// 	</tr>';
		// 	// <td width="200">(' . $penanggungjawab['nama_lengkap'] . ')</td>
		// }

		$html .= '
		</tbody>
		</table>
		';

		echo $css;
		echo $html;

		// $mpdf = new \Mpdf\Mpdf();

		// $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
		// $mpdf->Output();
	}

	public function index($role = 0)
	{
		// periksa kategori
		$role = ($role != '') ? $role : '';

		$data['role'] = $role;
		$data['users'] = $this->user_model->get_users($role);
		$data['count_all'] = $this->user_model->count_all_users_by_role('');
		$data['count_admin'] = $this->user_model->count_all_users_by_role(1);
		$data['count_tu'] = $this->user_model->count_all_users_by_role(2);
		$data['count_reviewer'] = $this->user_model->count_all_users_by_role(3);
		$data['count_dosen'] = $this->user_model->count_all_users_by_role(4);
		$data['view'] = 'admin/rekap';

		$this->load->view('layout/layout', $data);
	}

	public function add()
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
			$this->form_validation->set_rules('mobile_no', 'Number', 'trim|required');
			//$this->form_validation->set_rules('role', 'Role', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data['view'] = 'admin/users/user_add';
				$this->load->view('layout', $data);
			} else {

				$upload_path = './uploads/fotoProfil';

				if (!is_dir($upload_path)) {
					mkdir($upload_path, 0777, TRUE);
				}
				//$newName = "hrd-".date('Ymd-His');
				$config = array(
					'upload_path' => $upload_path,
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => FALSE,
				);

				$this->load->library('upload', $config);
				$this->upload->do_upload('foto_profil');
				$foto_profil = $this->upload->data();


				$data = array(
					'username' => $this->input->post('username'),
					'firstname' => $this->input->post('firstname'),
					'email' => $this->input->post('email'),
					'mobile_no' => $this->input->post('mobile_no'),
					'id_prodi' => $this->input->post('id_prodi'),
					'is_admin' => $this->input->post('is_admin'),
					'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
					'created_at' => date('Y-m-d : h:m:s'),
					'updated_at' => date('Y-m-d : h:m:s'),
					'is_verify' => 1,
					'is_active' => 1,
					'photo' => $upload_path . '/' . $foto_profil['file_name'],
				);

				$data = $this->security->xss_clean($data);
				$result = $this->user_model->add_user($data);
				if ($result) {
					$this->session->set_flashdata('msg', 'Pengguna berhasil ditambahkan!');
					redirect(base_url('admin/users'));
				}
			}
		} else {
			// $data['prodi'] = $this->prodi_model->get_all_prodi();
			$data['view'] = 'admin/users/user_add';
			$this->load->view('layout', $data);
		}
	}

	public function edit($id = 0)
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('firstname', 'Nama Lengkap', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
			$this->form_validation->set_rules('mobile_no', 'Number', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data['user'] = $this->user_model->get_user_by_id($id);
				$data['view'] = 'admin/users/user_edit';
				$this->load->view('layout', $data);
			} else {

				$upload_path = './uploads/fotoProfil';

				if (!is_dir($upload_path)) {
					mkdir($upload_path, 0777, TRUE);
				}

				$config = array(
					'upload_path' => $upload_path,
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => FALSE,
				);

				$this->load->library('upload', $config);
				$this->upload->do_upload('foto_profil');
				$foto_profil = $this->upload->data();

				$data = array(
					'username' => $this->input->post('username'),
					'firstname' => $this->input->post('firstname'),
					'email' => $this->input->post('email'),
					'mobile_no' => $this->input->post('mobile_no'),
					'password' => ($this->input->post('password') !== "" ? password_hash($this->input->post('password'), PASSWORD_BCRYPT) : $this->input->post('password_hidden')),
					'updated_at' => date('Y-m-d : h:m:s'),
					'photo' => ($foto_profil['file_name']) !== "" ? $upload_path . '/' . $foto_profil['file_name'] : $this->input->post('foto_profil_hidden'),
				);

				$data = $this->security->xss_clean($data);
				$result = $this->user_model->edit_user($data, $id);

				if ($result) {
					$this->session->set_flashdata('msg', 'Pengguna berhasil diubah!');
					redirect(base_url('admin/users'));
				}
			}
		} else {
			$data['user'] = $this->user_model->get_user_by_id($id);
			$data['view'] = 'admin/users/user_edit';
			$this->load->view('layout', $data);
		}
	}

	public function detail($id = 0)
	{
		$data['user'] = $this->user_model->get_user_by_id($id);

		$data['view'] = 'admin/users/detail';
		$this->load->view('layout', $data);
	}

	public function del($id)
	{
		$this->db->delete('ci_users', array('id' => $id));
		$this->session->set_flashdata('msg', 'Pengguna berhasil dihapus!');
		redirect(base_url('admin/users'));
	}
}
