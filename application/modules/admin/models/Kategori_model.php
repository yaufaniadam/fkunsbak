<?php
class Kategori_model extends CI_Model
{
	public function get_kategori_by_tahap($tahap)
	{
		$query = $this->db->get_where('kategori_ilmiah', array('tahap' => $tahap));
		return $query->result_array();
	}
	public function get_jumlah_kategori_by_tahap($id_residen)
	{
		$query1 = $this->db->get_where('residen_tahap', array('id_residen' => $id_residen, 'aktif' => 1));
		$id_tahap = $query1->row()->tahap;

		$query = $this->db->query("SELECT ki.kategori, kt.id_kategori,kt.id_tahap FROM kategori_ilmiah ki LEFT JOIN kategori_tahap kt ON ki.id = kt.id_kategori WHERE kt.id_tahap = $id_tahap");
		return $query->num_rows();
	}
}
