<?php
class User_model extends CI_Model
{

	public function add_user($data, $role)
	{
		$this->db->insert('ci_users', $data);
		$last_id = $this->db->insert_id();
		if ($role == 2) {
			$this->db->insert('dosen', array('user_id' => $last_id));
		} elseif ($role == 3) {
			$this->db->insert('residen', array('user_id' => $last_id));
			$last_id = $this->db->insert_id();
			$this->db->insert('residen_tahap', array(
				'id_residen' => $last_id,
				'start_date' => date('Y-m-d : h:m:s'),
				'aktif' => 1,
				'tahap' => 1
			));
		}
		return true;
	}

	//---------------------------------------------------
	// get all users for server-side datatable processing (ajax based)
	public function get_users($role)
	{

		if ($role != '') {
			$where = "a.role!= 1 AND a.role='$role'";
		} else {
			$where = "a.role != 1";
		}

		return $this->db->query("SELECT a.*, o.role as rolenya, r.nama_lengkap, r.photo, d.nama_lengkap as d_nama, d.photo as d_photo FROM ci_users a
			LEFT JOIN residen r ON a.id = r.user_id
			LEFT JOIN dosen d ON a.id = d.user_id
			LEFT JOIN role o ON o.id = a.role
			WHERE $where
			");
	}

	// Count total users by role
	public function count_all_users_by_role($role)
	{

		if ($role != '') {
			$this->db->where('role', $role);
		} else {
			$this->db->where('role !=', 1);
		}
		return $this->db->count_all_results('ci_users');
	}

	//---------------------------------------------------
	// Get user detial by ID
	public function get_user_by_id($id)
	{
		$query = $this->db->query("SELECT role from ci_users WHERE ci_users.id='$id'");

		$role = $query->row_array();

		if ($role['role'] == 3) {
			$query = $this->db->query("SELECT a.*, r.nama_lengkap, r.nim, r.telp, r.alamat, r.photo, r.tempat_lahir, r.tanggal_lahir, r.gender, r.id_pembimbing, d.nama_lengkap as nama_dosen
		 	FROM ci_users a
		 	LEFT JOIN residen r ON r.user_id=a.id
		 	LEFT JOIN dosen d ON d.id=r.id_pembimbing
		 	WHERE a.id='$id'
		 	");
			return $query->row_array();
		} else if ($role['role'] == 2) {
			$query = $this->db->query("SELECT a.*, d.nama_lengkap,d.telp, d.alamat, d.photo, d.id_divisi
		 	FROM ci_users a
		 	LEFT JOIN dosen d ON a.id=d.user_id
		 	WHERE a.id='$id'
		 	");
			return $query->row_array();
		}
	}

	//---------------------------------------------------
	// Edit user Record
	public function edit_user($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('ci_users', $data);
		return true;
	}

	//---------------------------------------------------
	// Edit user Record
	public function get_roles()
	{
		return $this->db->select('*')->from('role')->get()->result_array();
	}

	public function import_users($data_user)
	{

		foreach ($data_user as $user) {

			$data = [
				'username' => $user['username'],
				'email' => $user['email'],
				'role' => $user['role'],
				'password' => $user['password'],
			];

			$this->db->insert('ci_users', $data);
			if ($user['role'] == 3) {
				$detail_user = [
					'nama_lengkap' => $user['nama_lengkap'],
					'nim' => $user['username'],
					'telp' => $user['telp'],
					'user_id' =>  $this->db->insert_id(),
				];
				$this->db->insert('residen', $detail_user);
				$this->db->insert('residen_tahap', array(
					'id_residen' => $this->db->insert_id(),
					'start_date' => date('Y-m-d : h:m:s'),
					'aktif' => 1,
					'tahap' => 1
				));
			} else if ($user['role'] == 2) {
				$detail_user = [
					'nama_lengkap' => $user['nama_lengkap'],
					'telp' => $user['telp'],
					'user_id' =>  $this->db->insert_id(),
				];
				$this->db->insert('dosen', $detail_user);
			} else {
			}
		}

		//$this->db->insert_batch('ci_users', $data);
	}
}
