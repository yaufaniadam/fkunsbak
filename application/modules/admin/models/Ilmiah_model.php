<?php
class Ilmiah_model extends CI_Model
{
	public function count_residen_ilmiah($id)
	{
		# code...
	}

	public function get_residen_ilmiah($id_residen, $id_tahap, $id_divisi)
	{
		if ($id_tahap == 1) {
			$query = $this->db->query("SELECT i.id,i.status, i.judul_ilmiah,d.divisi, i.date, k.kategori, rt.tahap, r.nama_lengkap FROM ilmiah i
			LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
			LEFT JOIN residen r ON r.id=i.id_residen		 
			LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap	
			LEFT JOIN divisi d ON d.id = i.id_divisi	
			WHERE i.id_tahap=$id_tahap AND i.id_residen = $id_residen");
			// $query = $this->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'file_nilai !=' => ''));
		} else {
			$query = $this->db->query("SELECT i.id,i.status, i.judul_ilmiah,d.divisi, i.date, k.kategori, rt.tahap, r.nama_lengkap FROM ilmiah i
			LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
			LEFT JOIN residen r ON r.id=i.id_residen		 
			LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap	
			LEFT JOIN divisi d ON d.id = i.id_divisi	
			WHERE i.id_tahap=$id_tahap AND i.id_residen = $id_residen AND i.id_divisi = $id_divisi");
			// $query = $this->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'id_divisi' => $id_divisi, 'file_nilai !=' => ''));
		}
		return $query->result_array();
	}

	public function count_approved_ilmiah($id_residen)
	{
		$query1 = $this->db->get_where('residen_tahap', array('id_residen' => $id_residen, 'aktif' => 1));
		$id_tahap = $query1->row()->tahap;

		if ($id_tahap == 2 || $id_tahap == 3) {
			$query2 = $this->db->query("SELECT id_divisi FROM residen_divisi WHERE id = (SELECT MAX(id) FROM residen_divisi WHERE id_residen = $id_residen)");
			$id_divisi = $query2->row()->id_divisi;
			$query = $this->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'id_divisi' => $id_divisi, 'status' => 1));
			return $query->num_rows();
		} elseif ($id_tahap == 4) {
			$query = $this->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'id_divisi' => 0, 'status' => 1));
			return $query->num_rows();
		} else {
			$query = $this->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'status' => 1));
			return $query->num_rows();
		}
	}

	public function count_ilmiah_progress($id_residen)
	{
		$query1 = $this->db->get_where('residen_tahap', array('id_residen' => $id_residen, 'aktif' => 1));
		$id_tahap = $query1->row()->tahap;

		if ($id_tahap == 2 || $id_tahap == 3) {
			$query2 = $this->db->query("SELECT id_divisi FROM residen_divisi WHERE id = (SELECT MAX(id) FROM residen_divisi WHERE id_residen = $id_residen)");
			$id_divisi = $query2->row()->id_divisi;
			$query3 = $this->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'id_divisi' => $id_divisi, 'file_nilai !=' => ''));
			$jumlahdoc = $query3->num_rows();
		} elseif ($id_tahap == 4) {
			$query3 = $this->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'id_divisi' => 0, 'file_nilai !=' => ''));
			$jumlahdoc = $query3->num_rows();
		} else {
			$query3 = $this->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'file_nilai !=' => ''));
			$jumlahdoc = $query3->num_rows();
		}

		$query4 = $this->db->query("SELECT ki.kategori, kt.id_kategori,kt.id_tahap FROM kategori_ilmiah ki LEFT JOIN kategori_tahap kt ON ki.id = kt.id_kategori WHERE kt.id_tahap = $id_tahap");
		$jumlahkategori = $query4->num_rows();

		$ilmiahprogress = ($id_tahap == 1 ? $jumlahdoc / 8 * 100 : $jumlahdoc / $jumlahkategori * 100);

		return number_format($ilmiahprogress, 0);
	}
}
