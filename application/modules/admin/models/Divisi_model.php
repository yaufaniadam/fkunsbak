<?php
class Divisi_model extends CI_Model
{
	public function get_residen_divisi($id_residen)
	{
	}

	public function get_divisi_by_id_residen($id_residen)
	{
		$query1 = $this->db->query("SELECT id_divisi FROM residen_divisi WHERE id = (SELECT MAX(id) FROM residen_divisi WHERE id_residen = $id_residen)");
		$id_divisi = $query1->row()->id_divisi;

		$query2 = $this->db->get_where('divisi', array('id' => $id_divisi));
		return $query2->row()->divisi;
	}

	public function get_all_divisi()
	{
		$query = $this->db->query("SELECT * FROM divisi WHERE id !=12");
		return $query->result_array();
	}

	public function count_residen_divisi($id_residen)
	{
		$query = $this->db->get_where('residen_divisi', array('id_residen' => $id_residen));
		return $query->num_rows();
	}

	public function get_tahap2_progress($id_residen, $residen_tahap)
	{
		$jumlah_residen_divisi_selesai = $this->db->query("SELECT * FROM residen_divisi WHERE id_residen = $id_residen AND id_tahap = $residen_tahap AND end_date != 0")->num_rows();
		$progress = $jumlah_residen_divisi_selesai / 6 * 100;
		return number_format($progress, 0);
	}
}
