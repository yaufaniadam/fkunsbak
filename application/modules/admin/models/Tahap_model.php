<?php
class Tahap_model extends CI_Model
{
	public function get_residen_tahap($id_residen)
	{
		$query = $this->db->get_where('residen_tahap', array('id_residen' => $id_residen, 'aktif' => 1));
		return $query->row()->tahap;
	}

	public function get_all_tahap()
	{
		$query = $this->db->get('tahap');
		return $query->result_array();
	}

	public function get_tahap_by_id($tahap_id)
	{
		$query = $this->db->get_where('tahap', array('id' => $tahap_id));
		return $query->row()->tahap;
	}
}
