<?php
class Residen_model extends CI_Model
{

	public function get_author_of_ilmiah($id_ilmiah)
	{
		$query = $this->db->query("SELECT nama_lengkap FROM residen WHERE id = (SELECT id_residen FROM ilmiah WHERE id =" . $id_ilmiah . ")");
		$result = $query->row();
		return $result->nama_lengkap;
	}

	public function get_residen_by_tahap($id_tahap)
	{
		// $query = $this->db->query("SELECT r.nama_lengkap, r.nim, r.id,
		// (select rt.tahap from residen_tahap rt where rt.id_residen = r.id order by rt.id desc limit 1) as tahap 
		// from residen r		
		// ");

		$query = $this->db->query("SELECT r.nama_lengkap,r.nim,r.id,rt.tahap FROM residen r,residen_tahap rt WHERE r.id = rt.id_residen AND rt.tahap = $id_tahap AND rt.aktif = 1	
		");
		$id_residen = $query->row()->id;
		return  $query->result_array();
	}

	public function get_residen_by_tahap_and_divisi($id_tahap, $id_divisi)
	{
		$query_tahap_1 = $this->db->query("SELECT r.angkatan,r.nama_lengkap,r.nim,r.id,rt.tahap FROM residen r,residen_tahap rt WHERE r.id = rt.id_residen AND rt.tahap = $id_tahap AND rt.aktif = 1");

		$query_tahap_2 = $this->db->query("SELECT *,r.angkatan FROM residen r LEFT JOIN residen_tahap rt ON r.id = rt.id_residen LEFT JOIN residen_divisi rd ON r.id = rd.id_residen WHERE rt.aktif = 1 AND rt.tahap = $id_tahap AND rd.id_divisi != 12 AND rd.id = (SELECT MAX(id) FROM residen_divisi WHERE id_residen = r.id)");
		// AND rd.id = (SELECT MAX(id) FROM residen_divisi)

		// $query_tahap_2 = $this->db->query("SELECT * FROM residen r LEFT JOIN residen_tahap rt ON r.id = rt.id_residen LEFT JOIN residen_divisi rd ON r.id = rd.id_residen WHERE rt.aktif = 1 AND rt.tahap = $id_tahap AND rd.id_divisi = $id_divisi AND rd.id = (SELECT MAX(id) FROM residen_divisi)
		// ");

		if ($id_tahap == 2 or $id_tahap == 3) {
			return  $query_tahap_2->result_array();
		} else {
			return  $query_tahap_1->result_array();
		}
	}

	public function residen_lobby($id_tahap, $id_divisi)
	{
		$query = $this->db->query("SELECT * FROM residen r LEFT JOIN residen_tahap rt ON r.id = rt.id_residen LEFT JOIN residen_divisi rd ON r.id = rd.id_residen WHERE rt.aktif = 1 AND rt.tahap = $id_tahap AND rd.id_divisi = $id_divisi AND rd.id = (SELECT MAX(id) FROM residen_divisi WHERE id_residen = r.id )");
		$result = $query->result_array();

		return $result;
	}

	public function get_residen_by_id($id_residen)
	{
		$query = $this->db->get_where('residen', array('id' => $id_residen));
		return $query->result_array();
	}

	public function get_bukan_residen_divisi($id_residen)
	{
		// $query = $this->db->query("SELECT r.*,rd.* FROM residen r LEFT JOIN residen_divisi rd ON r.id = rd.id_residen WHERE r.id = $id_residen AND rd.id = (SELECT MAX(id) FROM residen_divisi WHERE id_residen = $id_residen)");
		$query = $this->db->query("SELECT * FROM `divisi` WHERE id NOT IN (SELECT id_divisi FROM residen_divisi WHERE id_residen = $id_residen)");
		return $query->result_array();
	}

	public function tahap_3($id_residen, $residen_tahap)
	{
		$this->db->where('id_residen', $id_residen);
		$this->db->update('residen_tahap', array('end_date' => date("Y-m-d h:i:s"), 'aktif' => 0, 'status' => 1));
		$this->db->insert('residen_tahap', array('id_residen' => $id_residen, 'tahap' => $residen_tahap + 1, 'start_date' => date("Y-m-d h:i:s"), 'aktif' => 1));
		return true;
	}

	public function tahap_2($id_residen)
	{
		date_default_timezone_set("Asia/Bangkok");
		$query = $this->db->get_where('residen_tahap', array('id_residen' => $id_residen, 'aktif' => 1));
		$residen_tahap = $query->row()->tahap;

		$query2 = $this->db->query("SELECT MAX(id) AS id FROM tod");
		$id_tod = $query2->row()->id;

		$this->db->where('id_residen', $id_residen);
		$this->db->update('residen_tahap', array('end_date' => date("Y-m-d h:i:s"), 'aktif' => 0, 'status' => 1));

		$this->db->insert('residen_tahap', array('id_residen' => $id_residen, 'tahap' => $residen_tahap + 1, 'start_date' => date("Y-m-d h:i:s"), 'aktif' => 1));
		$this->db->insert('residen_divisi', array('id_residen' => $id_residen, 'id_tahap' => $residen_tahap + 1, 'id_divisi' => 12));
		$this->db->insert('residen_tod', array('id_residen' => $id_residen, 'id_divisi' => 12, 'id_tod' => $id_tod));

		return true;
	}

	public function update_residen_divisi($residen_id, $data)
	{
		// $query = $this->db->get_where('residen_tahap', array('id_residen' => $residen_id, 'aktif' => 1));
		// $residen_tahap = $query->row()->tahap;

		foreach ($data as $id_divisi) {
		}

		$id_residen_div = $this->db->query("SELECT MAX(id) AS id FROM residen_divisi WHERE id_residen = $residen_id")->row()->id;
		$this->db->query("UPDATE residen_divisi SET id_divisi = $id_divisi WHERE id_residen = $residen_id AND id = $id_residen_div");
		// $this->db->query("UPDATE residen_divisi SET id_divisi = $id_divisi WHERE id_residen = $residen_id AND id = (SELECT MAX(id) FROM residen_divisi WHERE id_residen = $residen_id)");
		// $this->db->where('id_residen', $residen_id)->where("id", "(SELECT MAX(id) FROM residen_divisi WHERE id_residen = $residen_id)");
		// $this->db->update('residen_divisi', $data);
		$id_tod = $this->db->query("SELECT MAX(id) AS id FROM tod")->row()->id;
		$this->db->insert('residen_tod', array('id_residen' => $residen_id, 'id_divisi' => $id_divisi, 'id_tod' => $id_tod));

		return true;
	}

	public function divisi_selesai($residen_id)
	{
		date_default_timezone_set("Asia/Bangkok");
		$date =  date('Y-m-d h:i:s');

		$query = $this->db->get_where('residen_tahap', array('id_residen' => $residen_id, 'aktif' => 1));
		$residen_tahap = $query->row()->tahap;

		// echo $date;

		// $this->db->where('id_residen', $residen_id)
		// 	->where('id', "SELECT MAX(id) FROM residen_divisi WHERE id_residen = $residen_id");
		// $this->db->update('residen_divisi', array('end_date' => $date));

		$this->db->query("UPDATE residen_divisi SET end_date = '$date' WHERE id_residen = $residen_id AND id = (SELECT MAX(id) FROM residen_divisi WHERE id_residen = $residen_id)");

		$this->db->insert('residen_divisi', array('id_residen' => $residen_id, 'id_tahap' => $residen_tahap, 'id_divisi' => 12));
		return true;
	}
}
