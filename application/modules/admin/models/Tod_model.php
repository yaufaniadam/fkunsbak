<?php
class Tod_model extends CI_Model
{
	public function get_all_tod($starting_index = 0, $amount_of_data_to_be_shown)
	{
		$query = $this->db->query("SELECT * FROM tod order by id DESC LIMIT $starting_index,$amount_of_data_to_be_shown");
		return $query->result_array();
	}

	public function tambah_tod($data)
	{
		$this->db->insert('tod', $data);
		return true;
	}

	public function update_tod($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('tod', $data);
		return true;
	}

	public function get_residen_per_tod($id_tod, $id_divisi)
	{
		return $this->db->query("SELECT * FROM residen r LEFT JOIN residen_tod rtod ON r.id = rtod.id_residen WHERE rtod.id_tod =$id_tod AND rtod.id_divisi = $id_divisi")->result_array();
	}
}
