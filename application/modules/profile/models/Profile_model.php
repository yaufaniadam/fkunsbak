<?php
class Profile_model extends CI_Model
{

	//--------------------------------------------------------------------
	public function get_user_detail()
	{
		$id = $this->session->userdata('user_id');
		$query = $this->db->get_where('ci_users', array('id' => $id));
		return $result = $query->row_array();
	}
	//--------------------------------------------------------------------
	public function update_user($data)
	{
		$id = $this->session->userdata('user_id');
		$this->db->where('id', $id);
		return $this->db->update('ci_users', $data);
	}
	public function update_residen($data)
	{
		$id = $this->session->userdata('user_id');
		$this->db->where('user_id', $id);
		return $this->db->update('residen', $data);
	}
	public function update_residen_by_id($data, $id)
	{
		$this->db->where('user_id', $id);
		return $this->db->update('residen', $data);
	}
	public function update_staf_by_id($data, $id)
	{
		$this->db->where('user_id', $id);
		return $this->db->update('dosen', $data);
	}

	public function get_residen()
	{
		$user_id = $this->session->userdata('user_id');
		$query = $this->db->select('nama_lengkap,id, nim,tempat_lahir, tanggal_lahir, photo, alamat, telp, gender, id_pembimbing,
			');
		$query = $this->db->get_where('residen', array('user_id' => $user_id));
		return $query->row_array();
	}

	public function get_profile()
	{
		$user_id = $this->session->userdata('user_id');
		if ($this->session->userdata('role') == 3) {
			$query = $this->db->select('nama_lengkap, nim, angkatan, tempat_lahir, tanggal_lahir, photo, alamat, telp, gender, id_pembimbing,
			');
			$query = $this->db->get_where('residen', array('user_id' => $user_id));
		} else {
			$query = $this->db->select('nama_lengkap, nip, photo, alamat, telp');
			$query = $this->db->get_where('dosen', array('user_id' => $user_id));
		}
		return $query->row_array();
	}

	public function get_residen_by_id($user_id)
	{

		$query = $this->db->select('nama_lengkap, tempat_lahir, nim, angkatan, tanggal_lahir, photo, alamat, telp, gender, id_pembimbing,
			');
		$query = $this->db->get_where('residen', array('user_id' => $user_id));
		return $query->row_array();
	}
	public function get_staf_by_id($user_id)
	{
		$query = $this->db->select('nama_lengkap, nip, photo, alamat, telp');
		$query = $this->db->get_where('dosen', array('user_id' => $user_id));
		return $query->row_array();
	}
}
