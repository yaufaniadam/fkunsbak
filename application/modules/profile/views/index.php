<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Ubah Profil Saya</h1>
			</div>
			<div class="col-sm-6 d-none d-md-block">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Ubah Profil Saya</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">

				<?php if (isset($msg) || validation_errors() !== '') : ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="fa fa-exclamation"></i> Terjadi Kesalahan</h4>
						<?= validation_errors(); ?>
						<?= isset($msg) ? $msg : ''; ?>
					</div>
				<?php endif; ?>
				<!-- fash message yang muncul ketika proses penghapusan data berhasil dilakukan -->
				<?php if ($this->session->flashdata('msg') != '') : ?>
					<div class="alert alert-success flash-msg alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4>Sukses</h4>
						<?= $this->session->flashdata('msg'); ?>
					</div>
				<?php endif; ?>

				<div class="card card-warning card-outline">
					<div class="card-body box-profile">
						<div class="row">
							<div class="col-md-2">
								<?php if ($profile['photo'] == '') { ?>
									<div style="width:100px;height:100px; background:url('<?= base_url(); ?>public/dist/img/nophoto.png') center top no-repeat; background-size:100%;" class="profile-user-img img-responsive img-circle"></div>

								<?php } else { ?>
									<div style="width:110px;height:110px;background-size:100%; background:url('<?= base_url($profile['photo']); ?>') center center no-repeat; background-size:100%;" class="profile-user-img img-responsive img-circle"></div>

								<?php } ?>

							</div>

							<div class="col-md-10">
								<h4 class="mt-4 mb-0 "><?= $profile['nama_lengkap']; ?></h4>
								<p><i class="fas fa-envelope mr-1"></i> <?= $user['email']; ?></p>
							</div>
						</div>
					</div>
				</div>

			</div>


			<div class="col-md-5">

				<div class="card card-warning card-outline">
					<div class="card-body box-profile">

						<?php echo form_open_multipart(base_url('/profile'), 'class="form-horizontal"');  ?>
						<div class="form-group">
							<label for="username" class="control-label">Username</label>
							<input type="text" value="<?= $user['username']; ?>" class="form-control" disabled>
						</div>

						<div class="form-group">
							<label for="email" class="control-label">Email</label>
							<input type="email" value="<?= $user['email']; ?>" name="email" class="form-control" id="email" placeholder="">
						</div>


						<div class="form-group">
							<label for="password" class="control-label">Password</label>
							<input type="password" name="password" class="form-control" id="password" placeholder="">
							<input type="hidden" name="password_hidden" value="<?= $user['password']; ?>">
						</div>

						<div class="form-group">
							<input type="submit" name="submit" value="Ubah Akun Saya" class="btn btn-info">
						</div>


						<?php echo form_close(); ?>

					</div>
				</div>
			</div>
			<?php if ($this->session->userdata('role') == 3) { ?>
				<div class="col-md-7">

					<div class="card card-danger card-outline">
						<div class="card-body box-profile">
							<?php echo form_open_multipart(base_url('/profile/residen'), 'class="form-horizontal"');  ?>
							<div class="form-group">
								<label for="nama_lengkap" class="control-label">Nama Lengkap</label>
								<input type="text" value="<?= $profile['nama_lengkap']; ?>" name="nama_lengkap" class="form-control" id="nama_lengkap" placeholder="">
							</div>
							<div class="form-group">
								<label for="nim" class="control-label">NIM</label>
								<input type="text" value="<?= $profile['nim']; ?>" name="nim" class="form-control" id="nim" placeholder="">
							</div>
							<div class="form-group">
								<label for="angkatan" class="control-label">Angkatan</label>
								<input type="number" value="<?= $profile['angkatan']; ?>" name="angkatan" class="form-control" id="angkatan" placeholder="">
							</div>
							<div class="form-group">
								<label for="tempat_lahir" class="control-label">Tempat Lahir</label>
								<input type="text" value="<?= $profile['tempat_lahir']; ?>" name="tempat_lahir" class="form-control" id="tempat_lahir" placeholder="">
							</div>
							<div class="form-group">
								<label for="tanggal_lahir" class="control-label">Tanggal Lahir</label>
								<input type="text" value="<?= $profile['tanggal_lahir']; ?>" name="tanggal_lahir" class="form-control" id="tanggal_lahir" placeholder="Cth: 21 Januari 1988">
							</div>
							<div class="form-group">
								<label for="tanggal_lahir" class="control-label">Jenis Kelamin</label>
								<br>
								<input type="radio" value="l" name="gender" <?= ($profile['gender'] == 'l') ? 'checked="true"' : ''; ?>> Laki-laki
								<input type="radio" value="p" name="gender" <?= ($profile['gender'] == 'p') ? 'checked="true"' : ''; ?>> Perempuan
							</div>
							<div class="form-group">
								<label for="telp" class="control-label">Telepon</label>
								<input type="text" value="<?= $profile['telp']; ?>" name="telp" class="form-control" id="telp" placeholder="">
							</div>
							<div class="form-group">
								<label for="alamat" class="control-label">Alamat</label>
								<textarea name="alamat" class="form-control" id="alamat"><?= $profile['alamat']; ?></textarea>
							</div>

							<div class="form-group">
								<label for="photo" class="control-label">Foto Profil (jpg/png) 200x200px</label>
								<input type="file" name="photo" class="form-control" id="photo">
								<input type="hidden" name="photo_hidden" value="<?= $profile['photo']; ?>">
							</div>


							<div class="form-group">
								<label for="photo" class="control-label">Staf Pembimbing Akademik</label><br>
								<div class="">
									<select id="pembimbing" class="form-control" name="pembimbing">
										<option value="">Pilih Staf Pembimbing</option>
										<?php foreach ($staf as $staf) { ?>
											<option <?php if ($profile['id_pembimbing'] == $staf['id']) {
														echo "selected";
													} ?> value="<?= $staf['id'] ?>"><?= $staf['nama_lengkap']; ?><?php $staf['id'] ?></option>
										<?php } ?>
									</select>
								</div>
								<!-- <input type="hidden" id="hidden_pembimbing" name="pembimbing"> -->
							</div>

							<div class="form-group">
								<input type="submit" name="submit" value="Ubah Profil Saya" class="btn btn-info">
							</div>
							<?php echo form_close(); ?>

						</div>
					</div>
				</div>
			<?php } else { ?>
				<div class="col-md-7">
					<div class="card card-danger card-outline">
						<div class="card-body box-profile">
							<?php echo form_open_multipart(base_url('/profile/staf'), 'class="form-horizontal"');  ?>
							<div class="form-group">
								<label for="nama_lengkap" class="control-label">Nama Lengkap</label>
								<input type="text" value="<?= $profile['nama_lengkap']; ?>" name="nama_lengkap" class="form-control" id="nama_lengkap" placeholder="">
							</div>
							<div class="form-group">
								<label for="nip" class="control-label">NIP</label>
								<input type="text" value="<?= $profile['nip']; ?>" name="nip" class="form-control" id="nip" placeholder="">
							</div>
							<div class="form-group">
								<label for="telp" class="control-label">Telepon</label>
								<input type="text" value="<?= $profile['telp']; ?>" name="telp" class="form-control" id="telp" placeholder="">
							</div>
							<div class="form-group">
								<label for="alamat" class="control-label">Alamat</label>
								<textarea name="alamat" class="form-control" id="alamat"><?= $profile['alamat']; ?></textarea>
							</div>

							<div class="form-group">
								<label for="photo" class="control-label">Foto Profil (jpg/png) 200x200px</label>
								<input type="file" name="photo" class="form-control" id="photo">
								<input type="hidden" name="photo_hidden" value="<?= $profile['photo']; ?>">
							</div>

							<div class="form-group">
								<input type="submit" name="submit" value="Ubah Profil Saya" class="btn btn-info">
							</div>
							<?php echo form_close(); ?>

						</div>
					</div>
				</div>
			<?php } ?>

		</div>
	</div>
</section>



<script>
	function getStafPembimbing() {
		var pembimbing = document.getElementById('pembimbing').value;
		document.getElementById("hidden_pembimbing").value = pembimbing;

		console.log(pembimbing)
	}
	$("#profil a.nav-link").addClass('active');
</script>