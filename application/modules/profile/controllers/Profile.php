<?php defined('BASEPATH') or exit('No direct script access allowed');
class Profile extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model', 'profile_model');
	}
	//-------------------------------------------------------------------------
	public function index()
	{
		if ($this->input->post('submit')) {

			$this->form_validation->set_rules('email', 'E-mail', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data['user'] = $this->profile_model->get_user_detail();
				$data['profile'] = $this->profile_model->get_profile();
				$data['title'] = 'Profil Saya';
				$data['view'] = 'profile/index';
				$data['staf'] = $this->db->get('dosen')->result_array();
				$this->load->view('layout/layout', $data);
			} else {
				$data = array(

					'email' => $this->input->post('email'),
					'password' => ($this->input->post('password') !== "" ? password_hash($this->input->post('password'), PASSWORD_BCRYPT) : $this->input->post('password_hidden')),
					'updated_at' => date('Y-m-d : h:m:s'),
				);

				$data = $this->security->xss_clean($data);
				$result = $this->profile_model->update_user($data);
				if ($result) {
					$this->session->set_flashdata('msg', 'Akun Anda berhasil diubah!');
					redirect(base_url('profile'), 'refresh');
				}
			}
		} else {
			$data['user'] = $this->profile_model->get_user_detail();
			$data['profile'] = $this->profile_model->get_profile();
			$data['title'] = 'Profil Saya';
			$data['view'] = 'profile/index';
			$data['staf'] = $this->db->get('dosen')->result_array();
			$this->load->view('layout/layout', $data);
		}
	}

	public function residen()
	{
		if ($this->input->post('submit')) {

			$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('telp', 'Telepon', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('gender', 'Jenis Kelamin', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('pembimbing', 'Staf Pembimbing', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('nim', 'NIM', 'trim|required', array('required' => '%s wajib diisi.'));

			if ($this->form_validation->run() == FALSE) {
				$data['user'] = $this->profile_model->get_user_detail();
				$data['profile'] = $this->profile_model->get_profile();
				$data['title'] = 'Profil Saya';
				$data['view'] = 'profile/index';
				$data['staf'] = $this->db->get('dosen')->result_array();
				$this->load->view('layout/layout', $data);
			} else {

				$upload_path = './uploads/fotoProfil';

				if (!is_dir($upload_path)) {
					mkdir($upload_path, 0777, TRUE);
				}
				//$newName = "hrd-".date('Ymd-His');
				$config = array(
					'upload_path' => $upload_path,
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => FALSE,
				);

				$this->load->library('upload', $config);
				$this->upload->do_upload('photo');
				$photo = $this->upload->data();

				$data = array(
					'nama_lengkap' => $this->input->post('nama_lengkap'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tanggal_lahir' => $this->input->post('tanggal_lahir'),
					'alamat' => $this->input->post('alamat'),
					'telp' => $this->input->post('telp'),
					'gender' => $this->input->post('gender'),
					'photo' => ($photo['file_name']) !== "" ? $upload_path . '/' . $photo['file_name'] : $this->input->post('photo_hidden'),
					'id_pembimbing' => $this->input->post('pembimbing'),
					'nim' => $this->input->post('nim'),
					'angkatan' => $this->input->post('angkatan'),
				);

				$data = $this->security->xss_clean($data);
				$result = $this->profile_model->update_residen($data);
				if ($result) {
					$this->session->set_flashdata('msg', 'Profil Anda berhasil diubah!');
					redirect(base_url('profile'), 'refresh');
				}
			}
		} else {
			$data['user'] = $this->profile_model->get_user_detail();
			$data['profile'] = $this->profile_model->get_profile();
			$data['title'] = 'Profil Saya';
			$data['view'] = 'profile/index';
			$data['staf'] = $this->db->get('dosen')->result_array();
			$this->load->view('layout/layout', $data);
		}
	}
	public function staf()
	{
		if ($this->input->post('submit')) {

			$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('telp', 'Telepon', 'trim|required', array('required' => '%s wajib diisi.'));
			$this->form_validation->set_rules('nip', 'NIP', 'trim|required', array('required' => '%s wajib diisi.'));

			if ($this->form_validation->run() == FALSE) {
				$data['user'] = $this->profile_model->get_user_detail();
				$data['profile'] = $this->profile_model->get_profile();
				$data['title'] = 'Profil Saya';
				$data['view'] = 'profile/index';
				$this->load->view('layout/layout', $data);
			} else {

				$upload_path = './uploads/fotoProfil';

				if (!is_dir($upload_path)) {
					mkdir($upload_path, 0777, TRUE);
				}
				//$newName = "hrd-".date('Ymd-His');
				$config = array(
					'upload_path' => $upload_path,
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => FALSE,
				);

				$this->load->library('upload', $config);
				$this->upload->do_upload('photo');
				$photo = $this->upload->data();

				$data = array(
					'nama_lengkap' => $this->input->post('nama_lengkap'),
					'alamat' => $this->input->post('alamat'),
					'telp' => $this->input->post('telp'),
					'photo' => ($photo['file_name']) !== "" ? $upload_path . '/' . $photo['file_name'] : $this->input->post('photo_hidden'),
					'nip' => $this->input->post('nip'),
				);

				$data = $this->security->xss_clean($data);
				$result = $this->profile_model->update_staf_by_id($data, $this->session->userdata('user_id'));
				if ($result) {
					$this->session->set_flashdata('msg', 'Profil Anda berhasil diubah!');
					redirect(base_url('profile'), 'refresh');
				}
			}
		} else {
			$data['user'] = $this->profile_model->get_user_detail();
			$data['profile'] = $this->profile_model->get_profile();
			$data['title'] = 'Profil Saya';
			$data['view'] = 'profile/index';
			$data['staf'] = $this->db->get('dosen')->result_array();
			$this->load->view('layout/layout', $data);
		}
	}
}
