<?php
class Notif_model extends CI_Model
{
	public function send_notif($id_residen, $id_ilmiah)
	{
		$nama_residen = $this->db->query("SELECT * FROM residen WHERE id = $id_residen")->row()->nama_lengkap;
		$judul_ilmiah = $this->db->query("SELECT * FROM ilmiah WHERE id = $id_ilmiah")->row()->judul_ilmiah;
		$link_ilmiah = base_url('admin/ilmiah/detail/' . $id_ilmiah);

		$this->db->insert('admin_notif', array(
			'subject' => 'nilai ilmiah',
			'id_ilmiah' => $id_ilmiah,
			'id_residen' => $id_residen,
			'status' => 0,
			'message' => "Residen $nama_residen menambahkan nilai di ilmiah <a href ='$link_ilmiah'>$judul_ilmiah</a>"
		));
		return true;
	}
}
