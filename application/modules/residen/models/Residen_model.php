<?php
class Residen_model extends CI_Model
{
	public function get_author_of_ilmiah($id_ilmiah)
	{
		$query = $this->db->query("SELECT nama_lengkap FROM residen WHERE id = (SELECT id_residen FROM ilmiah WHERE id =" . $id_ilmiah . ")");
		$result = $query->row();
		return $result->nama_lengkap;
	}
	
	public function get_residen_id_tahap()
	{
		$user_id = $_SESSION['user_id']; 
		$query = $this->db->query("SELECT * FROM tahap WHERE id = (SELECT tahap FROM residen_tahap WHERE aktif = 1 AND id_residen = (SELECT id FROM residen WHERE user_id =".$user_id."))");
		$result = $query->row();
		return $result->id;
	}
}
