<?php
class Ilmiah_model extends CI_Model
{
	public function add_ilmiah($data)
	{
		$this->db->insert('ilmiah', $data);
		return true;
	}

	public function edit_ilmiah($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('ilmiah', $data);
		return true;
	}

	public function get_all_ilmiah()
	{
		$query = $this->db->query("SELECT i.id, i.judul_ilmiah, i.date, k.kategori, rt.tahap, r.nama_lengkap FROM ilmiah i
		LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
		LEFT JOIN residen r ON r.id=i.id_residen		
		LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap		
		");
		return $query->result_array();
	}

	public function get_ilmiah_by_tahap($tahap)
	{
		$query = "SELECT i.id, i.judul_ilmiah,d.divisi, i.date, k.kategori, rt.tahap, r.nama_lengkap FROM ilmiah i
		LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
		LEFT JOIN residen r ON r.id=i.id_residen		
		LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap	
		LEFT JOIN divisi d ON d.id = i.id_divisi	
		WHERE i.id_tahap=$tahap";
		// and i.id_divisi = $divisi

		$result = $this->db->query($query)->result_array();
		return $result;
	}

	public function get_ilmiah_by_divisi()
	{
		$query = $this->db->query("SELECT i.id, i.judul_ilmiah, i.date, k.kategori, rt.tahap, r.nama_lengkap FROM ilmiah i
		LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
		LEFT JOIN residen r ON r.id=i.id_residen		
		LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap	
		WHERE (rt.tahap='2a' OR rt.tahap='2b')
		");
		return $query->result_array();
	}

	public function myIlmiah($tahap, $id, $divisi)
	{
		// $this->db->select('ilmiah.*, kategori_ilmiah.kategori');
		// $this->db->from('ilmiah');
		// $this->db->join('kategori_ilmiah', 'kategori_ilmiah.id=ilmiah.id_kategori', 'left');
		// $this->db->where(array('id_residen' => $id, 'id_tahap' => $tahap));
		$query = $this->db->query("SELECT i.*,d.divisi,k.kategori FROM `ilmiah` i 
		LEFT JOIN kategori_ilmiah k ON k.id = i.id_kategori
		LEFT JOIN divisi d ON d.id = i.id_divisi WHERE id_residen = $id AND id_tahap = $tahap");
		$result = $query->result_array();
		if ($tahap == 1) {
			$this->db->select('ilmiah.*,divisi.*, kategori_ilmiah.kategori');
			$this->db->from('ilmiah');
			$this->db->join('kategori_ilmiah', 'kategori_ilmiah.id=ilmiah.id_kategori', 'left');
			$this->db->join('divisi', 'divisi.id=ilmiah.id_divisi', 'left');
			$this->db->where(array('id_residen' => $id, 'id_tahap' => $tahap, 'file_nilai !=' => ''));
			$query = $this->db->get();
			$jumlahdoc = $query->num_rows();
		} else {
			// $this->db->select('ilmiah.*, kategori_ilmiah.kategori');
			// $this->db->from('ilmiah');
			// $this->db->join('kategori_ilmiah', 'kategori_ilmiah.id=ilmiah.id_kategori', 'left');
			// $this->db->where(array('id_residen' => $id, 'id_tahap' => $tahap, 'id_divisi' => $divisi));
			// $query = $this->db->get();
			// $result = $query->result_array();

			$this->db->select('ilmiah.*, kategori_ilmiah.kategori');
			$this->db->from('ilmiah');
			$this->db->join('kategori_ilmiah', 'kategori_ilmiah.id=ilmiah.id_kategori', 'left');
			$this->db->where(array('id_residen' => $id, 'id_tahap' => $tahap, 'id_divisi' => $divisi, 'file_nilai !=' => ''));
			$query = $this->db->get();
			$jumlahdoc = $query->num_rows();
		}

		$query3 = $this->db->query("SELECT * FROM kategori_tahap WHERE id_tahap = " . $tahap);
		$jumlahkategory = $query3->num_rows();

		if ($tahap == 2 || $tahap == 3) {
			$divisi_residen = $this->db->query("SELECT id_divisi FROM residen_divisi WHERE id = (SELECT MAX(id) FROM residen_divisi WHERE id_residen = $id)")
				->row()->id_divisi;

			$jumlah_doc_divisi_2 = $this->db->query("SELECT * FROM `ilmiah` WHERE id_residen = $id AND id_tahap = $tahap AND id_divisi = $divisi_residen  ")->num_rows();

			$progress = $jumlah_doc_divisi_2 / $jumlahkategory * 100;
			$formattedprogress = number_format($progress, 0);
		} elseif ($tahap == 1) {
			$progress = $jumlahdoc / 8 * 100;
			$formattedprogress = number_format($progress, 0);
		} else {
			$progress = $jumlahdoc / $jumlahkategory * 100;
			$formattedprogress = number_format($progress, 0);
		}

		return $query = array($result, $formattedprogress);
	}

	public function get_kategori_ilmiah_based_on_tahap($tahap, $residen_id, $id_divisi)
	{

		// $query =  $this->db->query("SELECT kt.*, ki.kategori FROM kategori_tahap kt
		// 	LEFT JOIN kategori_ilmiah ki ON ki.id=kt.id_kategori
		// 	WHERE kt.id_tahap ='$tahap'
		// 	AND ki.id NOT IN (SELECT id_kategori FROM ilmiah WHERE id_residen='$residen_id')

		// 	");
		// return $query->result_array();

		if ($tahap == 1 || $tahap == 4) {
			$query =  $this->db->query("SELECT kt.*, ki.kategori FROM kategori_tahap kt
			LEFT JOIN kategori_ilmiah ki ON ki.id=kt.id_kategori
			WHERE kt.id_tahap ='$tahap'
			AND ki.id NOT IN (SELECT id_kategori FROM ilmiah WHERE id_residen='$residen_id'  AND id_kategori != 4 )
			");
			return $query->result_array();
		} else {
			$query =  $this->db->query("SELECT kt.*, ki.kategori FROM kategori_tahap kt
			LEFT JOIN kategori_ilmiah ki ON ki.id=kt.id_kategori
			WHERE kt.id_tahap ='$tahap'
			AND ki.id NOT IN (SELECT id_kategori FROM ilmiah WHERE id_residen='$residen_id' AND id_divisi='$id_divisi' )
			");
			return $query->result_array();
		}



		/*
		$sql = $this->db->query("SELECT id_divisi FROM residen_divisi WHERE id = (SELECT MAX(id) FROM residen_divisi WHERE id_residen = $residen_id)");
		$residen_divisi = $sql->row()->id_divisi;

		$query_tahap_2 =  $this->db->query("SELECT kt.*, ki.kategori FROM kategori_tahap kt
			LEFT JOIN kategori_ilmiah ki ON ki.id=kt.id_kategori
			WHERE kt.id_tahap ='$tahap'
			AND ki.id NOT IN (SELECT id_kategori FROM ilmiah WHERE id_residen='$residen_id')  AND ki.id NOT IN (SELECT id_kategori FROM ilmiah WHERE id_divisi = $residen_divisi)
			");
		return $query->result_array();
		// return $sql->row()->id_divisi;*/
	}
}
