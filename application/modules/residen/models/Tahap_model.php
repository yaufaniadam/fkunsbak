<?php
class Tahap_model extends CI_Model
{
	public function get_tahap_by_id($id)
	{
		$query = $this->db->get_where('tahap', array('id' => $id));
		$result = $query->row();
		return $result->tahap;
	}
}
