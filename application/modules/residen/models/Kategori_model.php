<?php
class Kategori_model extends CI_Model
{
	public function get_kategori_by_tahap($tahap)
	{
		$query = $this->db->select('*, kategori')
				->from('kategori_tahap')				
				->join('kategori_ilmiah','kategori_ilmiah.id=kategori_tahap.id_kategori', 'left')
				->where(array('id_tahap'=>$tahap)
			);
		return $query->get()->result_array();
	} 
}
