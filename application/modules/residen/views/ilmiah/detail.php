<!-- Content Header (Page header) -->
<?php
foreach ($query as $ilmiah) {
}
$last = $this->uri->total_segments();
$id = $this->uri->segment($last);
?>

<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Ilmiah</h1>
			</div><!-- /.col -->
			<div class="col-sm-6 d-none d-md-block">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Ilmiah</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">

	<div class="col-12">
		<?php if (isset($msg) || validation_errors() !== '') : ?>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="fa fa-exclamation"></i> Terjadi Kesalahan</h4>
				<?= validation_errors(); ?>
				<?= isset($msg) ? $msg : ''; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php if ($this->session->flashdata('msg') != '') : ?>
		<div class="alert alert-success flash-msg alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4>Success!</h4>
			<?= $this->session->flashdata('msg'); ?>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('wrn') != '') : ?>
		<div class="alert alert-warning flash-msg alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4>Warning!</h4>
			<?= $this->session->flashdata('wrn'); ?>
		</div>
	<?php endif; ?>

	<div class="container-fluid">
		<div class="row">

			<div class="col-md-3 order-2 order-md-1">

				<!-- Profile Image -->
				<div class="card card-olive card-outline">
					<div class="card-body box-profile ilmiah-detail">
						<div class="text-center">

							<?php if ($res_pic == '') { ?>
								<div style="width:110px;height:110px; background:url('<?= base_url(); ?>public/dist/img/nophoto.png') center top no-repeat; background-size:100%;" class="profile-user-img img-responsive img-circle"></div>
							<?php } else { ?>
								<div style="width:110px;height:110px; background:url('<?= base_url($res_pic); ?>') center center no-repeat; background-size:cover;" class="profile-user-img img-responsive img-circle"></div>
							<?php } ?>

						</div>

						<h5 class="text-center py-2"><?= $author ?></h5>

						<ul class="list-group list-group-unbordered mb-3">
							<?php foreach ($query as $ilmiah) { ?>
								<li class="list-group-item">
									<b>Tahap</b> <a class="float-right"><?= ilmiah_tahap($ilmiah['id_tahap']); ?></a>
								</li>
								<li class="list-group-item">
									<b>Diunggah</b> <a class="float-right"><?= $ilmiah['date']; ?></a>
								</li>
								<li class="list-group-item">
									<b>Tanggal Sidang</b> <a class="float-right"><?= $ilmiah['tgl_maju']; ?></a>
								</li>
								<li class="list-group-item">
									<b>Status</b>
									<a class="float-right">
										<?php
										if ($ilmiah['nilai'] == '' || $ilmiah['file_nilai'] == '') {
											echo " Nilai belum diunggah";
										} elseif ($ilmiah['status'] != 1) {
											echo " Menunggu konfirmasi admin";
										} else {
											echo " Ilmiah sudah dikonfirmasi";
										}
										?>
									</a>
								</li>
								<div class="mt-3">
									<span class="mb-3 d-block">
										<?php if ($ilmiah['file_nilai']) { ?>
											<a href="<?= base_url($ilmiah['file_nilai']) ?>" style="width: 100%;" class="btn btn-warning btn-sm"><i class="fa fa-download"></i> Unduh Nilai Asli</a>
										<?php } else { ?>
											<a style="width: 100%;" class="btn btn-warning btn-sm disabled">Nilai belum diunggah</a>
										<?php } ?>
									</span>

									<span class="mb-3 d-block">
										<?php if ($ilmiah['file_presensi']) { ?>
											<a href="<?= base_url($ilmiah['file_presensi']) ?>" style="width: 100%;" class="btn btn-warning btn-sm"><i class="fa fa-download"></i> Unduh Presensi</a>
										<?php } else { ?>
											<a style="width: 100%;" class="btn btn-warning btn-sm disabled">Presensi belum diunggah</a>
										<?php } ?></span>

								</div>

							<?php } ?>
						</ul>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->



			</div>
			<!-- /.col -->
			<div class="col-md-9 order-1 order-md-2">
				<div class="card">
					<div class="card-header p-3">
						<h4><?= $ilmiah['judul_ilmiah'] ?></h4>
					</div><!-- /.card-header -->
				</div>
				<div class="card">
					<div class="card-header pl-3 pt-2 pb-2">
						Deskripsi
					</div><!-- /.card-header -->
					<div class="card-body">
						<?= $ilmiah['deskripsi'] ?>
					</div>
					<div class="card-footer">
						<a href="<?= base_url($ilmiah['file']) ?>" class="btn btn-warning btn-sm"><i class="fa fa-download"></i> Unduh</a>
					</div>
				</div>

				<?php if (getResidenId() == $residen_id) { ?>
					<?= form_open_multipart(base_url('residen/ilmiah/detail/' . $id), '') ?>
					<div class="card">
						<div class="card-header pl-3 pt-2 pb-2">
							Hasil Ilmiah
						</div>
						<div class="card-body box-profile">
							<p><strong>Nilai Ilmiah</strong>
								<input type="number" value="<?= $ilmiah['nilai'] ?>" min="0" max="100" step="0.1" pattern="^\d+(?:\.\d{1,2})?$" class="form-control" name="nilai" id="" placeholder="Contoh: 92,5">
							</p>
							<p>
								<strong>Rekap Nilai Ilmiah</strong>
								<!-- <input type="file" name="file_nilai" class="form-control" id="file_nilai" onchange="getFileName()">
								<input type="hidden" name="nama_file_nilai" class="form-control" id="nama_file_nilai"> -->
								<div class="custom-file">
									<input type="file" name="file_nilai" class="custom-file-inputs" id="file_nilai" onchange="getFileName()">

									<small id="emailHelp" class="form-text text-muted">Ekstensi yang diizinkan (jpg/jpeg/png)</small>
									<input type="hidden" name="nama_file_nilai" class="form-control" id="nama_file_nilai" value="<?= $ilmiah['file_nilai']; ?>">
								</div>
							</p>
							<p>
								<strong>Rekap Presensi</strong>
								<!-- <input type="file" name="file_presensi" class="form-control" id="file_presensi" onchange="getFilePresensiName()">
								<input type="hidden" name="nama_file_presensi" class="form-control" id="nama_file_presensi"> -->
								<div class="custom-file">
									<input type="file" name="file_presensi" class="custom-file-inputd" id="file_presensi" onchange="getFilePresensiName()">
									<small id="emailHelp" class="form-text text-muted">Ekstensi yang diizinkan (jpg/jpeg/png)</small>
									<input type="hidden" name="nama_file_presensi" class="form-control" id="nama_file_presensi" value="<?= $ilmiah['file_presensi']; ?>">
								</div>
							</p>


							<div class="row alert alert-warning">
								<div class="col-12 col-md-8">
									<p class="align-middle mt-2 mb-2 mb-md-0">
										<input type="checkbox" name="check" id="check" class="" onclick="agree_with_rules()">
										Keterangan yang saya berikan adalah benar.
									</p>
								</div>
								<div class="col-12 col-md-4 text-center text-md-right">
									<input disabled type="submit" name="submit" id="submit" value="Submit" class="btn btn-info">
								</div>

							</div>

						</div>
					</div>
					<?php echo form_close(); ?>
				<?php } ?>

			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
<script>
	function getFileName() {
		var fileName = document.getElementById("file_nilai").files.item(0).name;
		$("#nama_file_nilai").val(fileName)
		$("#nilai").html(fileName)
	}

	function getFilePresensiName() {
		var fileName = document.getElementById("file_presensi").files.item(0).name;
		$("#nama_file_presensi").val(fileName)
		$("#presensi").html(fileName)
	}

	function agree_with_rules() {
		$('#submit').prop('disabled', false)
	}

	$("#<?= $id_menu; ?>").addClass('menu-open');
	$("#<?= $id_menu; ?> .<?= $class_menu; ?> a.nav-link").addClass('active');
</script>