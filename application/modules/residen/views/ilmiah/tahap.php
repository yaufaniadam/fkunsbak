<link rel="stylesheet" href="<?= base_url() ?>/public/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<?php
$current_user_id = $this->session->user_id;
// $this->session->set_userdata('referred_from', current_url());
$last = $this->uri->total_segments();
$tahap = $this->uri->segment(4);

?>
<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>
					<?= $title; ?>
					<?php if (currentUserTahap() == $tahap) { ?>
						<?php if (currentUserTahap() == 2 || currentUserTahap() == 3) { ?>
							<?php if (current_user_divisi() != 12) { ?>
								<a href="<?= base_url('residen/ilmiah/store/' . $tahap) ?>" class="btn btn-sm btn-default">Tambah baru</a>
							<?php } ?>
						<?php } else { ?>
							<a href="<?= base_url('residen/ilmiah/store/' . $tahap) ?>" class="btn btn-sm btn-default">Tambah baru</a>
						<?php } ?>
					<?php } ?>
				</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active"><?= $title; ?></li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<!-- fash message yang muncul ketika proses penghapusan data berhasil dilakukan -->
			<?php if ($this->session->flashdata('msg') != '') : ?>
				<div class="alert alert-success flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Success!</h4>
					<?= $this->session->flashdata('msg'); ?>
				</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-sm-8">
					<div class="btn-group" role="group" aria-label="Basic example">
						<a href="<?= $tahap == 2 || $tahap == 3 ? base_url('residen/ilmiah/tahap/' . $tahap) : base_url('residen/ilmiah/tahap/' . $tahap) ?>" class="btn btn-warning">Semua Ilmiah</a>
						<?php if ($tahap == currentUserTahap() || $tahap == tahapSelesai($tahap)) { ?>
							<a href="<?= $tahap == 2 || $tahap == 3 ? base_url('residen/ilmiah/myIlmiah/' . $tahap . '/' . getResidenId()) : base_url('residen/ilmiah/myIlmiah/' . $tahap . '/' . getResidenId()) ?>" class="btn btn-default">Ilmiah Saya</a>
						<?php } ?>
					</div>
					<?php if ($tahap == 2 || $tahap == 3) { ?>
						<div class="btn-group mr-2" role="group" aria-label="Second group">
							<div class="dropdown">
								<select id="selectload" class="form-control">
									<option value="">pilih divisi</option>
									<?php foreach ($all_divisi as $divisi) { ?>
										<option value="<?= $divisi['divisi']; ?>"><?= $divisi['divisi']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="col-sm-4">

				</div>
			</div>
			<br>

			<div class="card">

				<div class="card-body">

					<table id="tb_penelitian" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="width:60%">Judul Ilmiah</th>
								<th class="d-none d-md-table-cell">Kategori</th>
								<th class="d-none d-md-table-cell">Divisi</th>
								<th class="d-none d-md-table-cell" style="width:25%">Residen</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($query as $ilmiah) {  ?>
								<tr>
									<td><a class="text-bold" href="<?= base_url('residen/ilmiah/detail/' . $ilmiah['id']) ?>"><?= $ilmiah['judul_ilmiah']; ?></a>

										<span class="d-block d-md-none">Kategori : <?= $ilmiah['kategori']; ?></span>
										<span class="d-block d-md-none">Divisi : <?= $ilmiah['divisi']; ?></span>
										<span class="d-block d-md-none font-italic"><i class="fas fa-user"></i> <?= $ilmiah['nama_lengkap']; ?></span>
									</td>
									<td class="d-none d-md-table-cell"><?= $ilmiah['kategori']; ?></td>
									<td class="d-none d-md-table-cell"><?= $ilmiah['divisi']; ?></td>
									<td class="d-none d-md-table-cell"><?= $ilmiah['nama_lengkap']; ?></td>
								</tr>
							<?php } ?>
						</tbody>
						</tfoot>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<div class="modal fade" id="confirm-delete">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Perhatian</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Tutuo">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Yakin ingin menghapus data ini?&hellip;</p>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- DataTables -->
<script src="<?= base_url() ?>/public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
</script>

<!-- page script -->
<script>
	// $(function() {
	// 	$("#tb_penelitian").DataTable();
	// });

	var table = $('#tb_penelitian').DataTable();
	$('#selectload').on('change', function() {
		table.columns(2).search(this.value).draw();
	});

	$("#<?= $id_menu; ?>").addClass('menu-open');
	$("#<?= $id_menu; ?> .<?= $class_menu; ?> a.nav-link").addClass('active');
</script>