<link rel="stylesheet" href="<?= base_url() ?>/public/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<?php
$this->session->set_userdata('referred_from', current_url());
$last = $this->uri->total_segments();
$tahap = $this->uri->segment($last - 1);
list($query, $progress) = $query;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>
					<?= $title; ?>
					<?php if (currentUserTahap() == $tahap) { ?>
						<?php if (currentUserTahap() == 2 || currentUserTahap() == 3) { ?>
							<?php if (current_user_divisi() != 12) { ?>
								<a href="<?= base_url('residen/ilmiah/store/' . $tahap) ?>" class="btn btn-sm btn-default">Tambah baru</a>
							<?php } ?>
						<?php } else { ?>
							<a href="<?= base_url('residen/ilmiah/store/' . $tahap) ?>" class="btn btn-sm btn-default">Tambah baru</a>
						<?php } ?>
					<?php } ?>
				</h1>
			</div>
			<div class="col-sm-6 d-none d-md-block">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active"><?= $title; ?></li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<!-- fash message yang muncul ketika proses penghapusan data berhasil dilakukan -->
			<?php if ($this->session->flashdata('msg') != '') : ?>
				<div class="alert alert-success flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Success!</h4>
					<?= $this->session->flashdata('msg'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('wrn') != '') : ?>
				<div class="alert alert-warning flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Warning!</h4>
					<?= $this->session->flashdata('wrn'); ?>
				</div>
			<?php endif; ?>

			<div class="row">
				<div class="col-sm-8">
					<div class="btn-group" role="group" aria-label="Basic example">
						<a href="<?= $tahap == 2 || $tahap == 3 ? base_url('residen/ilmiah/tahap/' . $tahap) : base_url('residen/ilmiah/tahap/' . $tahap) ?>" class="btn btn-default">Semua Ilmiah</a>
						<a href="<?= $tahap == 2 || $tahap == 3 ? base_url('residen/ilmiah/myIlmiah/' . $tahap . '/' . getResidenId()) : base_url('residen/ilmiah/myIlmiah/' . $tahap . '/' . getResidenId()) ?>" class="btn btn-warning">Ilmiah Saya</a>
					</div>
					<?php if ($tahap == 2 || $tahap == 3) { ?>
						<div class="btn-group mr-2" role="group" aria-label="Second group">
							<div class="dropdown">
								<select id="selectload" class="form-control">
									<option value="">pilih divisi</option>
									<?php foreach ($all_divisi as $divisi) { ?>
										<option value="<?= $divisi['divisi']; ?>"><?= $divisi['divisi']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="col-sm-4">
					<?php if ($this->session->role == 3) { ?>
						<div class="my-3 p-2 p-md-0 my-md-0 text-center text-md-left">
							<span>Progress Saya:</span>
							<div class="progress" style="width: 100%;">
								<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: <?= $progress ?>%"><?= $progress ?>%</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>

			<br>

			<div class="card">

				<div class="card-body">


					<table id="tb_penelitian" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="width:35%">Judul Ilmiah</th>
								<th style="width:20%" class="d-none d-md-table-cell">Kategori</th>
								<th style="width:20%" class="d-none d-md-table-cell">Divisi</th>
								<th class="text-center d-none d-md-table-cell">Status</th>
								<th class="text-center d-none d-md-table-cell">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($query as $ilmiah) {  ?>
								<tr>
									<td>
										<!--<?= $ilmiah['nilai'] == '' ? 'nilai masih kosong' : (($ilmiah['file_nilai'] == '') ? 'file nilai belum diupload' : "asdf") ?> -->
										<a href="<?= base_url('residen/ilmiah/detail/' . $ilmiah['id']) ?>" class="text-bold"><?= $ilmiah['judul_ilmiah']; ?></a>
										<?php if ($ilmiah['status'] != 1) { ?>
											<!-- <?php if ($ilmiah['nilai'] == "" || $ilmiah['file_nilai'] == "" || $ilmiah['status'] ==  2) { ?> -->
											<span class="d-inline-block" data-html="true" tabindex="0" data-toggle="tooltip" title="
													<?php if ($ilmiah['nilai'] == "") { ?>
													<li>Nilai masih kosong</li>
													<?php } ?>
													<?php if ($ilmiah['file_nilai'] == '') { ?>
													<li>FIle nilai asli belum diunggah</li>
													<?php } ?>
													<?php if ($ilmiah['status'] == 2) { ?>
													<li>Ilmiah perlu direvisi</li>
													<?php } ?>
												">
												<!-- <span class="badge badge-danger">!</span> -->
											</span>
											<!-- <?php } ?> -->
										<?php } ?>
										<span class="d-block d-md-none">Kategori : <?= $ilmiah['kategori']; ?></span>
										<span class="d-block d-md-none">Divisi : <?= $ilmiah['divisi']; ?></span>
										<span class="d-block d-md-none">Status :
											<?php if ($ilmiah['status'] == 1) {
												echo 'Sudah dikonfirmasi';
											} elseif ($ilmiah['status'] == 2) {
												echo 'Ilmiah perlu direvisi';
											} elseif ($ilmiah['file_nilai'] == '') {
												echo 'Nilai belum diunggah';
											} elseif ($ilmiah['status'] == 0 && $ilmiah['file_nilai'] != '') {
												echo 'Menunggu konfirmasi admin';
											} ?>
										</span>
										<span class="d-block d-md-none">
											<a href="<?= base_url('residen/ilmiah/detail/' . $ilmiah['id']) ?>" class="btn btn-default btn-xs">
												<i class="fa fa-eye"></i>
											</a>
											<a href="<?= base_url('residen/ilmiah/edit/' . $ilmiah['id']) ?>" class="btn btn-default btn-xs">
												<i class="fas fa-pencil-alt" style="color:;"></i>
											</a>
											<a href="" style="color:#fff;" title="Hapus" class="delete btn btn-xs btn-danger" data-href="<?= base_url('residen/ilmiah/destroy/' . $ilmiah['id']) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
										</span>

									</td>
									<td class="d-none d-md-table-cell"><?= $ilmiah['kategori']; ?></td>
									<td class="d-none d-md-table-cell"><?= $ilmiah['divisi']; ?></td>
									<td class="text-center d-none d-md-block">
										<!-- <?= $ilmiah['status'] == 1 ? '<i class="fas fa-check"></i>' : (($ilmiah['status'] == 2) ? '<i class="fas fa-times"></i>' : "") ?> -->
										<?php if ($ilmiah['status'] == 1) {
											echo 'Sudah dikonfirmasi';
										} elseif ($ilmiah['status'] == 2) {
											echo 'Ilmiah perlu direvisi';
										} elseif ($ilmiah['file_nilai'] == '') {
											echo 'Nilai belum diunggah';
										} elseif ($ilmiah['status'] == 0 && $ilmiah['file_nilai'] != '') {
											echo 'Menunggu konfirmasi admin';
										} ?>

									</td>

									<td class="text-center d-none d-md-table-cell">
										<a href="<?= base_url('residen/ilmiah/detail/' . $ilmiah['id']) ?>" class="btn btn-default btn-sm">
											<i class="fa fa-search" style="color:;"></i>
										</a>
										<a href="<?= base_url('residen/ilmiah/edit/' . $ilmiah['id']) ?>" class="btn btn-default btn-sm">
											<i class="fas fa-pencil-alt" style="color:;"></i>
										</a>
										<a href="" style="color:#fff;" title="Hapus" class="delete btn btn-sm btn-danger" data-href="<?= base_url('residen/ilmiah/destroy/' . $ilmiah['id']) ?>" data-toggle="modal" data-target="#confirm-delete"> <i class="fa fa-trash-alt"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
						</tfoot>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<div class="modal fade" id="confirm-delete">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Perhatian</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Tutuo">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Yakin ingin menghapus data ini?&hellip;</p>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- DataTables -->
<script src="<?= base_url() ?>/public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
</script>

<!-- page script -->
<script>
	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	})

	// $(function() {
	// 	$("#tb_penelitian").DataTable();
	// });

	var table = $('#tb_penelitian').DataTable();
	$('#selectload').on('change', function() {
		table.columns(2).search(this.value).draw();
	});

	$("#<?= $id_menu; ?>").addClass('menu-open');
	$("#<?= $id_menu; ?> .<?= $class_menu; ?> a.nav-link").addClass('active');
</script>