<?php
$current_user_id = $this->session->user_id;
$last = $this->uri->total_segments();
$tahap = $this->uri->segment($last);
// $this->session->set_userdata('referred_from', current_url());
foreach ($query as $ilmiah) {
}
foreach ($current_kategori as $current_kategori) {
}
?>

<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Ubah Data Ilmiah</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Tambah</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="row">

		<div class="col-12">

			<?php if (isset($msg) || validation_errors() !== '') : ?>
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="fa fa-exclamation"></i> Terjadi Kesalahan</h4>
					<?= validation_errors(); ?>
					<?= isset($msg) ? $msg : ''; ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('msg') != '') : ?>
				<div class="alert alert-success flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Success!</h4>
					<?= $this->session->flashdata('msg'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('wrn') != '') : ?>
				<div class="alert alert-warning flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Warning!</h4>
					<?= $this->session->flashdata('wrn'); ?>
				</div>
			<?php endif; ?>

		</div>

		<div class="col-md-6">
			<div class="card card-success card-outline">
				<div class="card-body box-profile">

					<?= form_open_multipart(base_url('residen/ilmiah/edit/' . $ilmiah['id']), '') ?>

					<div class="form-group">
						<div class="mt-3">
							<label class="control-label">Judul Ilmiah</label>
							<input value="<?= $ilmiah['judul_ilmiah'] ?>" type="text" name="judul_ilmiah" class="form-control" id="name" placeholder="">
						</div>

						<div class="mt-3">
							<label class="control-label">Deskripsi</label>
							<textarea class="form-control" name="deskripsi" id="deskripsi"><?= $ilmiah['deskripsi'] ?></textarea>
						</div>

						<div class="mt-3">
							<div class="form-group">
								<label for="foto_profil" class="control-label">File</label>
								<div>
									<input type="file" name="file" class="form-control" id="dokumen">
									<input type="hidden" name="file-hidden" value="<?= $ilmiah['file'] ?>">
								</div>
							</div>
						</div>

						<div class="mt-3">
							<div class="form-group">
								<label for="foto_profil" class="control-label">Kategori</label>
								<select class="custom-select" name="kategori">
									<?php foreach ($kategori_by_tahap as $kategori) { ?>
										<option <?= ($kategori['id'] == $ilmiah['id_kategori']) ? 'selected' : '' ?> value="<?= $kategori['id'] ?>"><?= $kategori['kategori'] ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<?php
						$id_res = $_SESSION['residen_id'];
						// $id_tahap_i = $this->db->query("SELECT * FROM `residen_tahap` WHERE id_residen = $id_res AND aktif =1")->row()->tahap;
						?>
						<?php if ($id_tahap_ilmiah == 1) { ?>

							<div class="mt-3">
								<div class="form-group">
									<label for="foto_profil" class="control-label">Divisi</label>
									<select class="custom-select <?= (form_error('divisi')) ? 'is-invalid' : ''; ?>" name="divisi" id="divisi">
										<option value="">Pilih Divisi</option>
										<?php foreach ($select_divisi as $divisi) { ?>
											<option <?= $divisi['id'] == $ilmiah['id_divisi'] ? 'selected' : ''; ?> value="<?= $divisi['id'] ?>" <?= (set_select('divisi', $divisi['id'])) ?>><?= $divisi['divisi'] ?></option>
										<?php } ?>
									</select>
									<span class="text-danger"><?php echo form_error('divisi'); ?></span>
								</div>
							</div>

							<div class="mt-3">
								<div class="form-group">
									<label for="foto_profil" class="control-label">Pembimbing Ilmiah</label>
									<select class="custom-select <?= (form_error('staff')) ? 'is-invalid' : ''; ?>" name="staff" id="staff">
										<option value="">Pilih Pembimbing Ilmiah</option>
									</select>
									<span class="text-danger"><?php echo form_error('staff'); ?></span>
								</div>
							</div>

						<?php } ?>

						<div class="form-group">
							<label for="username" class="control-label">Tanggal Maju</label>
							<div class="">
								<input type="date" name="tanggal_maju" class="form-control  <?= (form_error('tanggal_maju')) ? 'is-invalid' : ''; ?>" id="tanggal_maju" value="<?= $ilmiah['tgl_maju']; ?>">
								<span class="text-danger"><?php echo form_error('tanggal_maju'); ?></span>
								<!-- <input type="hidden" name="start_date" id="hidden_start_date"> -->
							</div>
						</div>

						<div class="form-group mt-2">
							<input type="submit" name="submit" value="Submit" class="btn btn-info">
						</div>
					</div>

					<?php echo form_close(); ?>

				</div>
			</div>
		</div>

	</div>
</section>

<script>
	<?php if ($id_tahap_ilmiah == 1) { ?>

		$(document).ready(function() {
			$('#divisi').change(function() {
				var id = $(this).val();
				$.ajax({
					url: '<?= base_url('residen/staff_divisi/staff_by_id_divisi'); ?>',
					method: 'POST',
					data: {
						id_divisi: id
					},
					dataType: 'json',
					success: function(data) {
						var html = '';
						var i;
						if (data.length == 0) {
							html += '<option>Penanggungjawab tidak ditemukan</option>'
						} else {
							for (i = 0; i < data.length; i++) {
								html += '<option value = ' + data[i].id + '>' + data[i].nama_lengkap + '</option>'
							}
						}
						$('#staff').html(html);
					}
				});
			});
		});

		$(document).ready(function() {
			var id = $('#divisi').val();
			var ilmiah_pembimbing = <?= $ilmiah['id_staf'] ?>;
			$.ajax({
				url: '<?= base_url('residen/staff_divisi/staff_by_id_divisi'); ?>',
				method: 'POST',
				data: {
					id_divisi: id
				},
				dataType: 'json',
				success: function(data) {
					var html = '';
					var i;
					if (data.length == 0) {
						html += '<option>Penanggungjawab tidak ditemukan</option>'
					} else {
						for (i = 0; i < data.length; i++) {
							if (data[i].id == ilmiah_pembimbing) {
								html += document.getElementById("staff").innerHTML = '<option selected value = ' + data[i].id + '>' + data[i].nama_lengkap + '</option>';
							} else {
								html += document.getElementById("staff").innerHTML = '<option value = ' + data[i].id + '>' + data[i].nama_lengkap + '</option>';
							}
						}
					}
					$('#staff').html(html);
				}
			});
		});
		// html += '<option value = ' + data[i].id + '>' + data[i].nama_lengkap + '</option>'
		// + document.write(data[i].id == ilmiah_pembimbing ? 'selected' : '') +
	<?php } ?>
</script>
