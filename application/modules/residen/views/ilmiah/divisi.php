<?php
$current_user_id = $this->session->user_id;
// $last = $this->uri->total_segments();
// $tahap = $this->uri->segment($last);
$this->session->set_userdata('referred_from', current_url());
?>

<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">

			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Tambah</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="row">

		<div class="col-12">

			<?php if (isset($msg) || validation_errors() !== '') : ?>
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="fa fa-exclamation"></i> Terjadi Kesalahan</h4>
					<?= validation_errors(); ?>
					<?= isset($msg) ? $msg : ''; ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('msg') != '') : ?>
				<div class="alert alert-success flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Success!</h4>
					<?= $this->session->flashdata('msg'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('wrn') != '') : ?>
				<div class="alert alert-warning flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Warning!</h4>
					<?= $this->session->flashdata('wrn'); ?>
				</div>
			<?php endif; ?>

		</div>
		<div class="col-md-6 mr-auto ml-auto">
			<h1 class="text-center">Pilih Divisi</h1>
			<div class="card card-success card-outline">
				<div class="list-group">
					<?php foreach ($query as $divisi) { ?>
						<a href="<?= base_url('residen/ilmiah/tahap/'.$tahap.'/'. $divisi['id']) ?>" class="list-group-item list-group-item-action "><?= $divisi['divisi'] ?></a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
