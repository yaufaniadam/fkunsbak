<?php
$current_user_id = $this->session->user_id;
$last = $this->uri->total_segments();
$tahap = $this->uri->segment($last);

?>

<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Ilmiah Baru</h1>
			</div>
			<div class="col-sm-6 d-none d-md-block">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Tambah</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="row">

		<!-- <div class="col-12">
			<?php if (isset($msg) || validation_errors() !== '') : ?>
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="fa fa-exclamation"></i> Terjadi Kesalahan</h4>
					<?= validation_errors(); ?>
					<?= isset($msg) ? $msg : ''; ?>
				</div>
			<?php endif; ?>
		</div> -->

		<div class="col-md-6">
			<div class="card card-success card-outline">
				<div class="card-body box-profile">

					<?= form_open_multipart(base_url('residen/ilmiah/store/' . $tahap), '') ?>

					<div class="form-group">
						<div class="mt-3">
							<label class="control-label">Judul Ilmiah</label>
							<input type="text" name="judul_ilmiah" class="form-control <?= (form_error('judul_ilmiah')) ? 'is-invalid' : ''; ?>" id="name" value="<?php if (validation_errors()) {
																																										echo set_value('judul_ilmiah');
																																									} ?>">
							<span class="text-danger"><?php echo form_error('judul_ilmiah'); ?></span>
						</div>

						<div class="mt-3">
							<label class="control-label">Deskripsi</label>
							<textarea class="form-control 
							<?= (form_error('deskripsi')) ? 'is-invalid' : '';
							?>" name="deskripsi" id="deskripsi"><?php if (validation_errors()) {
																	echo set_value('deskripsi');
																}
																?></textarea>
							<span class="text-danger"><?php echo form_error('deskripsi'); ?></span>
						</div>

						<div class="mt-3">
							<div class="form-group">
								<label for="file" class="control-label">File Ilmiah</label>
								<div>
									<div class="custom-files">
										<input type="file" name="file" class="custom-file-inputs <?= (form_error('file')) ? 'is-invalid' : ''; ?>" id="dokumen" onchange="getFileName()" value="
										<?php
										if (validation_errors()) {
											echo set_value('file');
										}
										?>
										">
										<small id="emailHelp" class="form-text text-muted">Ekstensi yang diizinkan (pdf/docx/pptx)</small>

										<input type="hidden" name="fileName" id="fileName">
									</div>

								</div>
								<span class="text-danger"><?php echo form_error('fileName'); ?></span>
							</div>
						</div>

						<div class="mt-3">
							<div class="form-group">
								<label for="foto_profil" class="control-label">Kategori</label>
								<select class="custom-select <?= (form_error('kategori')) ? 'is-invalid' : ''; ?>" name="kategori">
									<option value="">Pilih Kategori</option>
									<?php foreach ($query as $kategori) { ?>
										<option value="<?= $kategori['id_kategori'] ?>" <?= (set_select('kategori', $kategori['id_kategori'])) ?>><?= $kategori['kategori'] ?></option>
									<?php } ?>
								</select>
								<span class="text-danger"><?php echo form_error('kategori'); ?></span>
							</div>
						</div>

						<?php
						$id_res = $_SESSION['residen_id'];
						$id_tahap_i = $this->db->query("SELECT * FROM `residen_tahap` WHERE id_residen = $id_res AND aktif =1")->row()->tahap;
						?>
						<?php if ($id_tahap_i == 1) { ?>

							<div class="mt-3">
								<div class="form-group">
									<label for="foto_profil" class="control-label">Divisi</label>
									<select class="custom-select <?= (form_error('divisi')) ? 'is-invalid' : ''; ?>" name="divisi" id="divisi">
										<option value="">Pilih Divisi</option>
										<?php foreach ($select_divisi as $divisi) { ?>
											<option value="<?= $divisi['id'] ?>" <?= (set_select('divisi', $divisi['id'])) ?>><?= $divisi['divisi'] ?></option>
										<?php } ?>
									</select>
									<span class="text-danger"><?php echo form_error('divisi'); ?></span>
								</div>
							</div>

							<div class="mt-3">
								<div class="form-group">
									<label for="foto_profil" class="control-label">Pembimbing Ilmiah</label>
									<select class="custom-select <?= (form_error('staff')) ? 'is-invalid' : ''; ?>" name="staff" id="staff">
										<option value="">Pilih Pembimbing Ilmiah</option>
									</select>
									<span class="text-danger"><?php echo form_error('staff'); ?></span>
								</div>
							</div>

						<?php } ?>

						<div class="form-group">
							<label for="username" class="control-label">Tanggal Maju</label>
							<div class="">
								<input type="date" name="tanggal_maju" class="form-control  <?= (form_error('tanggal_maju')) ? 'is-invalid' : ''; ?>" id="tanggal_maju">
								<span class="text-danger"><?php echo form_error('tanggal_maju'); ?></span>
								<!-- <input type="hidden" name="start_date" id="hidden_start_date"> -->
							</div>
						</div>

						<div class="form-group mt-2">
							<input type="submit" name="submit" value="Submit" class="btn btn-info">
						</div>
					</div>

					<?php echo form_close(); ?>

				</div>
			</div>
		</div>

	</div>
</section>

<script>
	function getFileName() {
		var fileName = document.getElementById("dokumen").files.item(0).name;
		$("#fileName").val(fileName)
		$("#nama_dokumen").html(fileName)
	}

	<?php if ($id_tahap_i == 1) { ?>

		$(document).ready(function() {
			$('#divisi').change(function() {
				var id = $(this).val();
				$.ajax({
					url: '<?= base_url('residen/staff_divisi/staff_by_id_divisi'); ?>',
					method: 'POST',
					data: {
						id_divisi: id
					},
					dataType: 'json',
					success: function(data) {
						console.log(data)
						var html = '';
						var i;
						if (data.length == 0) {
							html += '<option>Penanggungjawab tidak ditemukan</option>'
						} else {
							for (i = 0; i < data.length; i++) {
								html += '<option value = ' + data[i].id + '>' + data[i].nama_lengkap + '</option>'
							}
						}
						$('#staff').html(html);
					}
				});
			});
		});

	<?php } ?>

	$("#<?= $id_menu; ?>").addClass('menu-open');
	$("#<?= $id_menu; ?> .<?= $class_menu; ?> a.nav-link").addClass('active');
</script>