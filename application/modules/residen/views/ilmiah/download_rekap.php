<link rel="stylesheet" href="<?= base_url() ?>/public/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>
					<?= $title; ?>
				</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active"><?= $title; ?></li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<!-- fash message yang muncul ketika proses penghapusan data berhasil dilakukan -->
			<?php if ($this->session->flashdata('msg') != '') : ?>
				<div class="alert alert-success flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Success!</h4>
					<?= $this->session->flashdata('msg'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('wrn') != '') : ?>
				<div class="alert alert-warning flash-msg alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4>Warning!</h4>
					<?= $this->session->flashdata('wrn'); ?>
				</div>
			<?php endif; ?>

			<br>

			<div class="card">

				<div class="card-body">


					<table id="tb_penelitian" class="table table-bordered table-striped">
						<!-- <thead>
							<tr>
								<th style="width:50%">Tahap</th>
								<th style="width:50%">Aksi</th>
							</tr>
						</thead> -->
						<tbody>
							<tr>
								<td colspan="2" class="text-center"> --Tahap 1--</td>
							</tr>
							<tr>
								<td style="width:50%">Tahap 1</td>
								<td style="width:50%"><a href="<?= base_url('residen/rekap/download/1'); ?>" class="btn btn-primary">Download</a></td>
							</tr>
							<tr>
								<td colspan="2" class="text-center"> -- Tahap 2A--</td>
							</tr>
							<?php if ($tahap2a) { ?>
								<?php foreach ($tahap2a as $divisi) { ?>
									<tr>
										<td style="width:50%"><?= $divisi['divisi']; ?></td>
										<td style="width:50%"><a href="<?= base_url('residen/rekap/download/2/' . $divisi['id']); ?>" class="btn btn-primary">Download</a></td>
									</tr>
								<?php } ?>
							<?php } else { ?>
								<td colspan="2" class="text-center">Anda belum mancapai tahap ini</td>
							<?php } ?>
							<tr>
								<td colspan="2" class="text-center"> --Tahap 2B--</td>
							</tr>
							<?php if ($tahap2b) { ?>
								<?php foreach ($tahap2b as $divisi) { ?>
									<tr>
										<td style="width:50%"><?= $divisi['divisi']; ?></td>
										<td style="width:50%"><a href="<?= base_url('residen/rekap/download/3/' . $divisi['id']); ?>" class="btn btn-primary">Download</a></td>
									</tr>
								<?php } ?>
							<?php } else { ?>
								<td colspan="2" class="text-center">Anda belum mancapai tahap ini</td>
							<?php } ?>
							<tr>
								<td colspan="2" class="text-center"> --Tahap 3--</td>
							</tr>
							<tr>
								<?php if ($tahap3) { ?>
									<td style="width:50%">Tahap 3</td>
									<td style="width:50%">
										<a href="" class="btn btn-primary">Download</a>
									</td>
								<?php } else { ?>
									<td colspan="2" class="text-center">Anda belum mancapai tahap ini</td>
								<?php } ?>
							</tr>
						</tbody>
						</tfoot>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<div class="modal fade" id="confirm-delete">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Perhatian</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Tutuo">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Yakin ingin menghapus data ini?&hellip;</p>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<a class="btn btn-danger btn-ok">Hapus</a>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- DataTables -->
<script src="<?= base_url() ?>/public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
</script>

<!-- page script -->
<script>
	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	})

	// $(function() {
	// 	$("#tb_penelitian").DataTable();
	// });

	var table = $('#tb_penelitian').DataTable();
	$('#selectload').on('change', function() {
		table.columns(2).search(this.value).draw();
	});

	$("#<?= $id_menu; ?>").addClass('menu-open');
	$("#<?= $id_menu; ?> .<?= $class_menu; ?> a.nav-link").addClass('active');
</script>
