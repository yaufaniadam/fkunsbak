<section class="content-header d-none d-md-block">
	<h1>Beranda</h1>
</section>

<section class="content">
	<div class="col-md-12">
		<div class="box box-success box-solid text-center mb-5 mt-4">
			<div class="box-header with-border">
				<h3 class="box-title">Selamat Datang, <?= getUserbyId($this->session->userdata("user_id"))['nama_lengkap']; ?></h3>
			</div>
			<div class="box-body">
				<p>
					Anda login sebagai Residen
				</p>
				<p>Tahap : <?= $tahap_residen['tahap']; ?> </p>
				<?php if ($tahap_residen['id'] == 2 || $tahap_residen['id'] == 3) { ?>
					<p>Divisi : <?= $divisi_residen['divisi']; ?> </p>
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="row">
		<!-- ./col -->
		<?php foreach ($tahap as $tahap) { ?>
			<div class="col-md-3 col-6">
				<!-- small box -->
				<div class="small-box bg-<?php if ($tahap['id'] == 1) {
												echo "blue";
											} elseif ($tahap['id'] == 2) {
												echo "red";
											} elseif ($tahap['id'] == 3) {
												echo "green";
											} else {
												echo "yellow";
											} ?>">
					<div class="inner">
						<p>Jumlah ilmiah</p>
						<h3><?= ilmiah_dashboard($tahap['id']) ?></h3>

						<p>Tahap <?= $tahap['tahap'] ?></p>
					</div>
					<div class="icon">
						<i class="far fa-file-alt"></i> </div>
				</div>
			</div>
		<?php } ?>
	</div>

</section>

<script>
	$("#beranda a.nav-link").addClass('active');
</script>
