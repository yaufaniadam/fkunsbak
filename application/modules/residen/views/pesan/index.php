<div class="card">
	<div class="card-body">
		<table id="tb_penelitian" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th style="width:100%" class="text-center">Semua Pesan</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($mails as $mail) { ?>

					<tr>
						<td>
							<div class="row <?= $mail['status'] == 0 ? 'font-weight-bold' : '' ?>">
								<div class="col-2"><?= $mail['subject'] ?></div>
								<div class="col-9"><a href="<?= base_url('residen/pesan/detail/' . $mail['id']) ?>"><?= substr($mail['content'], 0, 80) ?>...</a></div>
								<div class="col-1"><?= $mail['tanggal'] ?></div>
							</div>
						</td>
					</tr>

				<?php } ?>
			</tbody>
		</table>
	</div>
	<!-- /.card-body -->
</div>
<script src="<?= base_url() ?>/public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
</script>

<!-- page script -->
<script>
	$(function() {
		$("#tb_penelitian").DataTable();
	});
</script>
