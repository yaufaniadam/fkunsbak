<?php defined('BASEPATH') or exit('No direct script access allowed');
class Staff_divisi extends MY_Controller
{
	public function staff_by_id_divisi()
	{
		$id_divisi = $this->input->post('id_divisi');
		$data = $this->db->query("SELECT sd.*,dsn.nama_lengkap FROM staf_divisi sd LEFT JOIN dosen dsn ON sd.id_dosen = dsn.id WHERE sd.id_divisi = $id_divisi AND sd.admin_divisi = 0")->result_array();
		echo json_encode($data);
	}
}
