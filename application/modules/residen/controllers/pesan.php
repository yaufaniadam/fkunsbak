<?php defined('BASEPATH') or exit('No direct script access allowed');
class Pesan extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		// $this->load->model('Ilmiah_model', 'ilmiah_model');
		// $this->load->model('Residen_model', 'residen_model');
		// $this->load->model('Kategori_model', 'kategori_model');
		// $this->load->model('Divisi_model', 'divisi_model');
		// $this->load->model('Tahap_model', 'tahap_model');
		// $this->load->model('Notif_model', 'notif_model');
	}

	public function index()
	{
		$id_residen = $_SESSION['residen_id'];
		$data['mails'] = $this->db->get_where('notif', array('id_residen' => $id_residen))->result_array();
		$data['view'] = 'pesan/index.php';
		$this->load->view('layout/layout', $data);
	}

	public function detail($id_mail)
	{
		$query1 = $this->db->get_where('notif', array('id' => $id_mail))->row();
		$mail_stats = $query1->status;
		$id_sender = $query1->sender;

		if ($mail_stats == 0) {
			$this->db->set('status', 1);
			$this->db->where('id', $id_mail);
			$this->db->update('notif');
		}

		$data['sender'] = $this->db->get_where('ci_users', array('id' => $id_sender))->result_array();
		$data['query'] = $this->db->get_where('notif', array('id' => $id_mail))->result_array();
		$data['view'] = 'pesan/detail.php';
		$this->load->view('layout/layout', $data);
	}
}
