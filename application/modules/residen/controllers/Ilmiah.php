<?php defined('BASEPATH') or exit('No direct script access allowed');
class Ilmiah extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ilmiah_model', 'ilmiah_model');
		$this->load->model('Residen_model', 'residen_model');
		$this->load->model('Kategori_model', 'kategori_model');
		$this->load->model('Divisi_model', 'divisi_model');
		$this->load->model('Tahap_model', 'tahap_model');
		$this->load->model('Notif_model', 'notif_model');
	}

	public function index()
	{
		$result = $this->ilmiah_model->get_all_ilmiah();

		$data['query'] = $result;
		$data['id_menu'] = "ilmiah";
		$data['class_menu'] = "index";
		$data['title'] = 'Ilmiah';
		$data['view'] = 'ilmiah/index.php';
		$this->load->view('layout/layout', $data);
	}

	public function myIlmiah($tahap, $id, $divisi = 0)
	{
		$sql = $this->db->get_where('tahap', array('id' => $tahap));
		$result = $sql->row();
		$nama_tahap = $result->tahap;
		// print_r($result);
		$data['query'] = $this->ilmiah_model->myIlmiah($tahap, $id, $divisi);
		$data['all_divisi'] = $this->divisi_model->get_all_divisi();
		$data['id_menu'] = 'ilmiah';
		$data['class_menu'] = 'tahap' . $tahap;
		$data['title'] = 'Ilmiah Tahap ' . $nama_tahap;
		$data['view'] = 'ilmiah/myIlmiah.php';
		$this->load->view('layout/layout', $data);
	}

	public function tahap($tahap)
	{
		$result = $this->ilmiah_model->get_ilmiah_by_tahap($tahap);
		$result2 = $this->tahap_model->get_tahap_by_id($tahap);
		// $data['divisi'] = $divisi;
		$data['all_divisi'] = $this->divisi_model->get_all_divisi();
		$data['query'] = $result;
		$data['id_menu'] = "ilmiah";
		$data['class_menu'] = "tahap" . $tahap;
		$data['tahap'] = $tahap;
		$data['title'] = 'Ilmiah Tahap ' . $result2;
		$data['view'] = 'ilmiah/tahap.php';
		$this->load->view('layout/layout', $data);
	}

	public function divisi($tahap)
	{
		$data['query'] = $this->divisi_model->get_all_divisi();
		$data['tahap'] = $tahap;
		$data['view'] = 'ilmiah/divisi.php';
		$this->load->view('layout/layout', $data);
	}

	public function store($tahap)
	{
		// $referred_from = $this->session->userdata('referred_from');
		$residen_id = $this->session->userdata("residen_id");
		if (($tahap == 2) || ($tahap == 3)) {
			$residen_divisi = $this->divisi_model->get_current_user_divisi();
		} else {
			$residen_divisi = '';
		}
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules(
				'judul_ilmiah',
				'Judul Ilmiah',
				'trim|required',
				array('required' => '%s wajib diisi')
			);
			$this->form_validation->set_rules(
				'deskripsi',
				'Deskripsi',
				'trim|required',
				array('required' => '%s wajib diisi')
			);
			$this->form_validation->set_rules(
				'fileName',
				'Dokumen',
				'trim|required',
				array('required' => '%s wajib diisi')
			);
			$this->form_validation->set_rules(
				'kategori',
				'Kategori',
				'trim|required',
				array('required' => '%s wajib diisi')
			);

			if ($tahap == 1) {
				$this->form_validation->set_rules(
					'divisi',
					'divisi',
					'trim|required',
					array('required' => '%s wajib diisi')
				);
				$this->form_validation->set_rules(
					'staff',
					'penanggungjawab',
					'trim|required',
					array('required' => '%s wajib diisi')
				);
			}

			$this->form_validation->set_rules(
				'tanggal_maju',
				'tanggal maju',
				'trim|required',
				array('required' => '%s wajib diisi')
			);

			if ($this->form_validation->run() == FALSE) {
				$data['id_menu'] = "ilmiah";
				$data['class_menu'] = "tahap" . $tahap;
				$data['query'] =  $this->ilmiah_model->get_kategori_ilmiah_based_on_tahap($tahap, $residen_id, $residen_divisi);
				if ($tahap == 1) {
					$data['select_divisi'] = $this->db->get('divisi')->result_array();
				}
				$data['view'] = 'ilmiah/tambah.php';
				$this->load->view('layout/layout', $data);
			} else {
				$upload_path = 'uploads/dokumen';

				if (!is_dir($upload_path)) {
					mkdir($upload_path, 0777, TRUE);
				}

				$config = array(
					'upload_path' => $upload_path,
					'allowed_types' => "pdf|pptx|docx",
					'overwrite' => FALSE,
				);

				$this->load->library('upload', $config);
				$this->upload->do_upload('file');
				$file = $this->upload->data();

				$id_divisi = $this->divisi_model->get_current_user_divisi();
				$id_tahap = $this->residen_model->get_residen_id_tahap();
				$data = array(
					'judul_ilmiah' => $this->input->post('judul_ilmiah'),
					'deskripsi' => $this->input->post('deskripsi'),
					'file' => $this->input->post('file'),
					'id_residen' => $residen_id,
					'id_tahap' => $this->residen_model->get_residen_id_tahap(),
					'id_kategori' => $this->input->post('kategori'),
					'id_divisi' => ((currentUserTahap() == 2) || (currentUserTahap() == 3))  ? $id_divisi : $this->input->post('divisi'),
					'id_staf' => (currentUserTahap() == 1)  ? $this->input->post('staff') : '',
					'tgl_maju' => $this->input->post('tanggal_maju'),
					'file' => $upload_path . '/' . $file['file_name'],
				);

				$data = $this->security->xss_clean($data);
				$result = $this->ilmiah_model->add_ilmiah($data);
				if ($result) {
					$this->session->set_flashdata('msg', 'Ilmiah berhasil ditambahkan!');
					redirect(base_url("residen/ilmiah/myilmiah/$id_tahap/$residen_id"));
				}
			}
		} else {
			$data['id_menu'] = "ilmiah";
			$data['class_menu'] = "tahap" . $tahap;
			$data['query'] = $this->ilmiah_model->get_kategori_ilmiah_based_on_tahap($tahap, $residen_id, $residen_divisi);
			if ($tahap == 1) {
				$data['select_divisi'] = $this->db->get('divisi')->result_array();
			}
			$data['view'] = 'ilmiah/tambah.php';
			$this->load->view('layout/layout', $data);
		}
	}

	public function destroy($id)
	{
		$user_id = $_SESSION['user_id'];
		$referred_from = $this->session->userdata('referred_from');

		$query = $this->db->get_where('residen', array('user_id' => $user_id));
		$result = $query->row();
		$id_residen = $result->id;

		$query2 = $this->db->get_where('ilmiah', array('id' => $id));
		$result2 = $query2->row();
		$id_residen_on_ilmiah = $result2->id_residen;
		$status = $result2->status;

		if ($id_residen_on_ilmiah == $id_residen) {
			if ($status == 0) {
				$query = $this->db->get_where('ilmiah', array('id' => $id));
				$path_file = $query->row_array();
				$file_nilai = $query->row_array();
				if (realpath($path_file['file'])) {
					unlink(realpath($path_file['file']));
				}
				if (realpath($file_nilai['file_nilai'])) {
					unlink(realpath($file_nilai['file_nilai']));
				}
				$this->db->delete('ilmiah', array('id' => $id));
				$this->session->set_flashdata('msg', 'Ilmiah berhasil dihapus!');
				redirect($referred_from);
			} else {
				$this->session->set_flashdata('wrn', 'Ilmiah yang sudah dinilai tidak dapat diubah maupun dihapus');
				redirect($referred_from);
			}
		} else {
			$this->session->set_flashdata('wrn', 'Anda tidak punya hak untuk menghapus file ini');
			redirect($referred_from);
		}
	}

	public function edit($id)
	{

		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('judul_ilmiah', 'Judul', 'trim|required');
			$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$tahap_ilmiah = $this->db->query("SELECT * FROM ilmiah WHERE id = $id")->row()->id_tahap;
				$query = $this->db->get_where('ilmiah', array('id' => $id));
				$result = $query->result_array();
				$result1 = $query->row();

				$id_tahap = $result1->id_tahap;

				$kategori_by_tahap = $this->kategori_model->get_kategori_by_tahap($id_tahap);
				$query2 = $this->db->get_where('kategori_ilmiah', array('id' => $result1->id_kategori));
				$result2 = $query2->result_array();
				if ($tahap_ilmiah == 1) {
					$data['select_divisi'] = $this->db->get('divisi')->result_array();
				}
				$data['id_tahap_ilmiah'] = $id_tahap;
				$data['kategori_by_tahap'] = $kategori_by_tahap;
				$data['current_kategori'] = $result2;
				$data['query'] = $result;
				$data['view'] = 'ilmiah/edit.php';
				$this->load->view('layout/layout', $data);
			} else {
				$id_residen = $_SESSION['residen_id'];
				$max_id_divisi_residen = $this->db->query("SELECT MAX(id) AS id FROM residen_divisi WHERE id_residen = $id_residen")->row()->id;

				$tahap_ilmiah = $this->db->query("SELECT * FROM ilmiah WHERE id = $id")->row()->id_tahap;
				if ($tahap_ilmiah == 2 || $tahap_ilmiah == 3) {
					$residen_divisi = $this->db->query("SELECT id_divisi FROM residen_divisi WHERE id = $max_id_divisi_residen AND id_residen = $id_residen")->row()->id_divisi;
				}

				$data = array(
					'judul_ilmiah' => $this->input->post('judul_ilmiah'),
					'deskripsi' => $this->input->post('deskripsi'),
					'id_kategori' => $this->input->post('kategori'),
					'file' => $this->input->post('file') == "" ? $this->input->post('file-hidden') : $this->input->post('file'),
					'id_divisi' => $tahap_ilmiah == 2 || $tahap_ilmiah == 3 ? $residen_divisi : $this->input->post('divisi'),
					'id_staf' => $tahap_ilmiah == 2 || $tahap_ilmiah == 3 ? '' : $this->input->post('staff'),
					'tgl_maju' => $this->input->post('tanggal_maju'),
				);

				$query1 = $this->db->get_where('ilmiah', array('id' => $id));
				$result1 = $query1->row();
				$status = $result1->status;

				$data = $this->security->xss_clean($data);

				if ($status != 1) {
					$result = $this->ilmiah_model->edit_ilmiah($data, $id);
					$query1 = $this->db->get_where('ilmiah', array('id' => $id));
					$result1 = $query1->row();
					$tahap = $result1->id_tahap;
					$id_residen = $result1->id_residen;

					if ($result) {
						$this->session->set_flashdata('msg', 'Ilmiah berhasil diubah!');
						redirect(base_url('residen/ilmiah/myilmiah/' . $tahap . '/' . $id_residen));
					}
				} else {
					$this->session->set_flashdata('wrn', 'Ilmiah yang sudah dinilai tidak dapat diubah maupun dihapus');
					redirect(base_url('residen/ilmiah/edit/' . $id));
				}
			}
		} else {
			$tahap_ilmiah = $this->db->query("SELECT * FROM ilmiah WHERE id = $id")->row()->id_tahap;
			$query = $this->db->get_where('ilmiah', array('id' => $id));
			$result = $query->result_array();
			$result1 = $query->row();

			$id_tahap = $result1->id_tahap;

			$kategori_by_tahap = $this->kategori_model->get_kategori_by_tahap($id_tahap);
			$query2 = $this->db->get_where('kategori_ilmiah', array('id' => $result1->id_kategori));
			$result2 = $query2->result_array();
			if ($tahap_ilmiah == 1) {
				$data['select_divisi'] = $this->db->get('divisi')->result_array();
			}
			$data['id_tahap_ilmiah'] = $id_tahap;
			$data['query'] = $result;
			$data['kategori_by_tahap'] = $kategori_by_tahap;
			$data['current_kategori'] = $result2;
			$data['view'] = 'ilmiah/edit.php';
			$this->load->view('layout/layout', $data);
		}
	}

	public function detail($id)
	{
		// $referred_from = $this->session->userdata('referred_from');
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('nilai', 'Nilai', 'trim|required');
			$this->form_validation->set_rules('nama_file_nilai', 'Nilai Asli', 'trim|required');
			$this->form_validation->set_rules('nama_file_presensi', 'Presensi', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$author = $this->residen_model->get_author_of_ilmiah($id);
				$res_pic = $this->db->get_where('residen', array('nama_lengkap' => $author))->row()->photo;

				$query = $this->db->query("SELECT * FROM ilmiah WHERE id = " . $id);
				$result = $query->result_array();
				$result2 = $query->row();
				$id_tahap = $result2->id_tahap;
				$residen_id = $result2->id_residen;

				$query2 = $this->db->get_where('tahap', array('id' => $id_tahap));
				$tahap_result = $query2->row();
				$tahap = $tahap_result->tahap;

				$data['residen_id'] = $residen_id;
				$data['res_pic'] = $res_pic;
				$data['query'] = $result;
				$data['title'] = 'Ilmiah Tahap ' . $tahap;
				$data['author'] = $author;
				$data['id_menu'] = 'ilmiah';
				$data['for_breadcrumb'] = $id_tahap;
				$data['class_menu'] = 'tahap' . $id_tahap;
				$data['view'] = 'ilmiah/detail.php';
				$this->load->view('layout/layout', $data);
			} else {
				$upload_path = './uploads/nilai';

				if (!is_dir($upload_path)) {
					mkdir($upload_path, 0777, TRUE);
				}

				$config = array(
					'upload_path' => $upload_path,
					'allowed_types' => "jpeg|jpg|png",
					'encrypt_name' => TRUE,
					'overwrite' => FALSE,
				);

				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('file_nilai')) {
					$this->session->set_flashdata('wrn', 'Format File tidak didukung');
					redirect(base_url('residen/ilmiah/detail/' . $id));
				} else {
					$file = $this->upload->data();
				}

				$upload_path_presensi = './uploads/nilai';

				if (!is_dir($upload_path_presensi)) {
					mkdir($upload_path_presensi, 0777, TRUE);
				}

				$config_presensi = array(
					'upload_path' => $upload_path_presensi,
					'allowed_types' => "jpeg|jpg|png",
					'encrypt_name' => TRUE,
					'overwrite' => FALSE,
				);

				$this->load->library('upload', $config_presensi);
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('file_presensi')) {
					$this->session->set_flashdata('wrn', 'Format File tidak didukung');
					redirect(base_url('residen/ilmiah/detail/' . $id));
				} else {
					$this->upload->do_upload('file_presensi');
				}
				$file_presensi = $this->upload->data();

				// echo "upload path nilai : " . $upload_path;
				// echo "<br>upload path presensi : " . $upload_path_presensi . $file_presensi['file_name'];
				// dd($file);

				$data = array(
					'nilai' => $this->input->post('nilai'),
					'file_nilai' => $upload_path . '/' . $file['file_name'],
					'file_presensi' => $upload_path_presensi . '/' . $file_presensi['file_name']
				);

				// dd($data);

				$query1 = $this->db->get_where('ilmiah', array('id' => $id));
				$result1 = $query1->row();
				$status = $result1->status;

				$data = $this->security->xss_clean($data);

				if ($status != 1) {
					$result = $this->ilmiah_model->edit_ilmiah($data, $id);

					$query1 = $this->db->get_where('ilmiah', array('id' => $id));
					$result1 = $query1->row();
					$tahap = $result1->id_tahap;
					$id_residen = $result1->id_residen;
					if ($result) {
						$this->notif_model->send_notif($id_residen, $id);
						$this->session->set_flashdata('msg', 'Nilai berhasil diunggah!');
						redirect(base_url('residen/ilmiah/myilmiah/' . $tahap . '/' . $id_residen));
					}
				} else {
					$this->session->set_flashdata('wrn', 'Ilmiah yang sudah dinilai tidak dapat diubah maupun dihapus');
					redirect(base_url('residen/ilmiah/detail/' . $id));
				}
			}
		} else {
			$author = $this->residen_model->get_author_of_ilmiah($id);
			$res_pic = $this->db->get_where('residen', array('nama_lengkap' => $author))->row()->photo;
			$query = $this->db->query("SELECT *, DATE_FORMAT( date, '%e %b %Y') as date, DATE_FORMAT( date, '%e %b %Y') as tgl_maju FROM ilmiah WHERE id = " . $id);
			$result = $query->result_array();
			$result2 = $query->row();
			$id_tahap = $result2->id_tahap;
			$residen_id = $result2->id_residen;

			$query2 = $this->db->get_where('tahap', array('id' => $id_tahap));
			$tahap_result = $query2->row();
			$tahap = $tahap_result->tahap;

			$data['residen_id'] = $residen_id;
			$data['query'] = $result;
			$data['res_pic'] = $res_pic;
			$data['title'] = 'Ilmiah Tahap ' . $tahap;
			$data['author'] = $author;
			$data['id_menu'] = 'ilmiah';
			$data['for_breadcrumb'] = $id_tahap;
			$data['class_menu'] = 'tahap' . $id_tahap;
			$data['view'] = 'ilmiah/detail.php';
			$this->load->view('layout/layout', $data);
		}
	}
}
