<?php defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile/profile_model', 'profile_model');
	}

	public function index()
	{
		//cek kelengkapan profil, jika belum lengkap lempar ke halaman profil
		$profile = $this->profile_model->get_residen();
		// $cek_profile = array_search("", $profile) !== false;

		if ($profile['tempat_lahir'] == '' || $profile['tanggal_lahir'] == '' || $profile['alamat'] == '' || $profile['gender'] == '' || $profile['id_pembimbing'] == '') {
			redirect(base_url('profile'), 'refresh');
			exit;
		}

		$id_residen = $profile['id'];

		$tahap_residen = $this->db->query("SELECT * FROM residen_tahap rt JOIN tahap t ON rt.tahap = t.id WHERE rt.id_residen = $id_residen AND rt.aktif = 1")->row_array();

		if ($tahap_residen['id'] == 2 || $tahap_residen['id'] == 3) {
			$max_id_data = $this->db->query("SELECT MAX(id) AS id FROM residen_divisi WHERE id_residen = $id_residen")->row_array();
			$max_id = $max_id_data['id'];
			$divisi_residen = $this->db->query("SELECT * FROM residen_divisi rd JOIN divisi d ON rd.id_divisi = d.id WHERE rd.id_residen = $id_residen AND rd.id=$max_id")->row_array();
			$data['divisi_residen'] = $divisi_residen;
		}

		// if ($cek_profile == 1) {
		// 	redirect(base_url('profile'), 'refresh');
		// 	exit;
		// }
		$data['tahap'] = $this->db->get('tahap')->result_array();
		$data['tahap_residen'] = $tahap_residen;
		$data['view'] = 'dashboard/index';
		$this->load->view('layout/layout', $data);
	}
}
