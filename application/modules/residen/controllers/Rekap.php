<?php defined('BASEPATH') or exit('No direct script access allowed');
class Rekap extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$id_residen = $_SESSION['residen_id'];
		$data['tahap2a'] = $this->db->query("SELECT * FROM `residen_divisi` rd 
		LEFT JOIN divisi d ON d.id = rd.id_divisi 
		WHERE rd.id = (select MAX(id) FROM residen_divisi WHERE id_residen = $id_residen) AND rd.id_tahap = 2")->result_array();
		$data['tahap2b'] = $this->db->query("SELECT * FROM `residen_divisi` rd 
		LEFT JOIN divisi d ON d.id = rd.id_divisi 
		WHERE rd.id = (select MAX(id) FROM residen_divisi WHERE id_residen = $id_residen) AND rd.id_tahap = 3")->result_array();
		$data['tahap3'] = $this->db->query("SELECT * FROM residen_tahap WHERE id_residen = $id_residen AND tahap = 4 AND aktif = 1")->result_array();
		$data['title'] = 'Download Rekap';
		$data['view'] = 'ilmiah/download_rekap.php';
		$this->load->view('layout/layout', $data);
	}

	public function download($id_tahap, $id_divisi = 0)
	{
		$id_residen = $_SESSION['residen_id'];
		$tahap_residen = $this->db->query("SELECT * FROM residen_tahap WHERE id_residen = $id_residen AND aktif = 1")->row()->tahap;

		$data_residen = $this->db->query("SELECT * FROM residen WHERE id = $id_residen")->row();

		$penanggungjawab = $this->db->query("SELECT pj.*,dsn.nama_lengkap FROM `pj_tahap` pj LEFT JOIN dosen dsn ON pj.id_dosen = dsn.id")->result_array();

		if ($id_tahap == 1 || $id_tahap == 4) {
			$jumlah_kategori = $this->db->query("SELECT * FROM `kategori_tahap` WHERE id_tahap = $id_tahap")->num_rows();
			$total_nilai = $this->db->query("SELECT SUM(nilai) AS nilai FROM ilmiah WHERE id_tahap = $id_tahap AND id_residen = $id_residen")->row()->nilai;
			$nilai_rata_rata = $total_nilai / $jumlah_kategori;
			$daftar_ilmiah = $this->db->query("SELECT * FROM ilmiah WHERE id_residen = $id_residen AND id_tahap = $id_tahap")->result_array();
		} else {
			$jumlah_kategori = $this->db->query("SELECT * FROM `kategori_tahap` WHERE id_tahap = $id_tahap")->num_rows();
			$total_nilai = $this->db->query("SELECT SUM(nilai) AS nilai FROM ilmiah WHERE id_residen = $id_residen AND id_tahap = $id_tahap AND id_divisi = $id_divisi")->row()->nilai;
			$nilai_rata_rata = $total_nilai / $jumlah_kategori;
			$daftar_ilmiah = $this->db->query("SELECT * FROM ilmiah WHERE id_residen = $id_residen AND id_tahap = $id_tahap AND id_divisi = $id_divisi")->result_array();
		}

		$css = "<style>
		.text-center{ text-align:center;
		}
		
		</style>";
		$html = '<h1 class="text-center">REKAP ILMIAH<br>PPDS I ILMU PENYAKIT DALAM</h1>';

		$html .= '<figure class="text-center"><img src="' . base_url() . 'public/dist/img/nophoto.png" /></figure>';
		$html .= '<table style="width:60%; border:1px solid #ddd; margin : auto;">
		<tbody>
		<tr>
		<td width="170"><strong>Nama </strong></td>
		<td width="431"><strong>: ' . $data_residen->nama_lengkap . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>NIM </strong></td>
		<td width="431"><strong>: ' . $data_residen->nim . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>TTL </strong></td>
		<td width="431"><strong>: ' . $data_residen->tempat_lahir . '/' . $data_residen->tanggal_lahir . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>Alamat </strong></td>
		<td width="431"><strong>: ' . $data_residen->alamat . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>Awal Pendidikan </strong></td>
		<td width="431"><strong>: ' . $data_residen->angkatan . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>No. HP </strong></td>
		<td width="431"><strong>: ' . $data_residen->telp  . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>Tahap</strong></td>
		<td width="431"><strong>: ' . $id_tahap  . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>Nilai rata-rata</strong></td>
		<td width="431"><strong>: ' . $nilai_rata_rata  . '</strong></td>
		</tr>
		<tr>
		<td width="170"><strong>Nilai total</strong></td>
		<td width="431"><strong>: ' . $total_nilai  . '</strong></td>
		</tr>
		</tbody>
		</table>
		';

		$html .= '<table style="width:60%; border:1px solid #ddd; margin : auto;"><thead><tr><th>Judul Ilmiah</th>';
		$html .= '<th>Kategori Ilmiah</th>';
		$html .= '<th>Pembimbing</th>';
		$html .= '<th>Nilai</th>';
		$html .= '</tr></thead>';
		foreach ($daftar_ilmiah as $ilmiah) {
			$html .= '<tr><td>' . $ilmiah['judul_ilmiah'] . '</td>';
			$html .= '<td>' . $this->db->get_where('kategori_ilmiah', array('id' => $ilmiah['id_kategori']))->row()->kategori . '</td>';
			$html .= '<td>' . $this->db->get_where('dosen', array('id' => $ilmiah['id_staf']))->row()->nama_lengkap . '</td>';
			$html .= '<td>' . $ilmiah['nilai'] . '</td>';
			$html .= '</tr>';
		}

		$html .= '</table>';


		$html .= '<table style="width:60%; border:1px solid #ddd; margin : auto;">
		<tbody>
		<tr>
		<td width="200">&nbsp;</td>
		<td width="200">Mengetahui, Penanggung Jawab Tahap</td>
		<td width="200">&nbsp;</td>
		</tr>';

		$i = 0;

		foreach ($penanggungjawab as $pj) {
			$i++;
			if ($i % 2 == 1) {
				$html .= '<tr><td>' . $pj['nama_lengkap'] . '</td>';
			} else {
				$html .= '<td>' . $pj['nama_lengkap'] . '</td></tr>';
			}
		}

		$html .= '
		</tbody>
		</table>
		';

		echo $css;
		echo $html;

		// $mpdf = new \Mpdf\Mpdf();

		// $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
		// $mpdf->Output();
	}
}
