<!-- Brand Logo -->
<a href="<?= base_url() ?>admin/dashboard" class="brand-link navbar-light">
	<img src="<?= base_url() ?>public/dist/img/reads-logo.png" alt="SIM Borang UMY" class="brand-image">
	<span class="brand-text font-weight-light">&nbsp;</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
	<!-- Sidebar Menu -->
	<nav class="mt-2">
		<ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu" data-accordion="false">
			<!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
			<li class="nav-item" id="beranda">
				<a href="<?= base_url() ?>admin/dashboard" class="nav-link">
					<i class="nav-icon fas fa-home"></i>
					<p>
						Beranda
					</p>
				</a>
			</li>

			<li class="nav-item has-treeview" id="residen">
				<a href="#" class="nav-link">
					<i class="nav-icon fas fa-user-md"></i>
					<p>
						Residen
						<i class="fas fa-angle-left right"></i>
					</p>
				</a>
				<ul class="nav nav-treeview">
					<!-- <li class="nav-item index">
						<a href="<?= base_url('admin/ilmiah/'); ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Semua Ilmiah</p>
						</a>
					</li> -->
					<li class="nav-item tahap1">
						<a href="<?= base_url('admin/residen/tahap/1/0') ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Residen Tahap 1</p>
						</a>
					</li>
					<li class="nav-item tahap2">
						<a href="<?= base_url('admin/residen/tahap/2/1') ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Residen Tahap 2a</p>
						</a>
					</li>
					<li class="nav-item tahap3">
						<a href="<?= base_url('admin/residen/tahap/3/1') ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Residen Tahap 2b</p>
						</a>
					</li>
					<li class="nav-item tahap4">
						<a href="<?= base_url('admin/residen/tahap/4/0') ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Residen Tahap 3</p>
						</a>
					</li>
					<li class="nav-item lobby">
						<a href="<?= base_url('admin/residen/lobby') ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>
								Lobby Divisi
								<?php if (count_residen_in_lobby()) { ?>
									<span class="badge badge-pill badge-warning"><?= count_residen_in_lobby(); ?></span>
								<?php } ?>
							</p>
						</a>
					</li>

				</ul>

			</li>
			<li class="nav-item has-treeview" id="ilmiah">
				<a href="#" class="nav-link">
					<i class="nav-icon fas fa-book"></i>
					<p>
						Ilmiah
						<i class="fas fa-angle-left right"></i>
					</p>
				</a>
				<ul class="nav nav-treeview">

					<li class="nav-item itahap1">
						<a href="<?= base_url('admin/ilmiah/tahap/1') ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Ilmiah Tahap 1</p>
						</a>
					</li>
					<li class="nav-item itahap2">
						<a href="<?= base_url('admin/ilmiah/tahap/2') ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Ilmiah Tahap 2a</p>
						</a>
					</li>
					<li class="nav-item itahap3">
						<a href="<?= base_url('admin/ilmiah/tahap/3') ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Ilmiah Tahap 2b</p>
						</a>
					</li>
					<li class="nav-item itahap4">
						<a href="<?= base_url('admin/ilmiah/tahap/4') ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Ilmiah Tahap 3</p>
						</a>
					</li>

				</ul>

			</li>
			<li class="nav-item has-treeview" id="dpj">
				<a href="#" class="nav-link">
					<i class="nav-icon fas fa-calendar-alt"></i>
					<p>
						Staf
						<i class="fas fa-angle-left right"></i>
					</p>
				</a>
				<ul class="nav nav-treeview">
					<li class="nav-item daftar_dpj">
						<a href="<?= base_url('admin/dosenpj'); ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Staf Tahap</p>
						</a>
					</li>

					<li class="nav-item daftar_sd">
						<a href="<?= base_url('admin/stafdivisi'); ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Staf Divisi</p>
						</a>
					</li>
					<li class="nav-item staf_akademik">
						<a href="<?= base_url('admin/stafdivisi/akademik'); ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Staf Pembimbing Akademik</p>
						</a>
					</li>
				</ul>
			</li>

			<li class="nav-header">ADMINISTRATOR</li>
			<li class="nav-item has-treeview" id="pengguna">
				<a href="#" class="nav-link">
					<i class="nav-icon fas fa-users"></i>
					<p>
						Pengguna
						<i class="fas fa-angle-left right"></i>
					</p>
				</a>
				<ul class="nav nav-treeview">
					<li class="nav-item semua_pengguna">
						<a href="<?= base_url('admin/users'); ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Semua Pengguna</p>
						</a>
					</li>
					<li class="nav-item tambah_pengguna">
						<a href="<?= base_url('admin/users/add'); ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Tambah Pengguna</p>
						</a>
					</li>
					<li class="nav-item upload_residen">
						<a href="<?= base_url('admin/users/upload'); ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Upload Pengguna</p>
						</a>
					</li>

				</ul>
			</li>

			<li class="nav-item has-treeview" id="tod">
				<a href="#" class="nav-link">
					<i class="nav-icon fas fa-calendar-alt"></i>
					<p>
						Tour of Duty
						<i class="fas fa-angle-left right"></i>
					</p>
				</a>
				<ul class="nav nav-treeview">
					<li class="nav-item daftar_tod">
						<a href="<?= base_url('admin/tod'); ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Daftar TOD</p>
						</a>
					</li>
					<li class="nav-item tambah_tod">
						<a href="<?= base_url('admin/tod/tambah_tod'); ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Tambah TOD</p>
						</a>
					</li>

				</ul>
			</li>



			<li class="nav-item has-treeview" id="pesan">
				<a href="<?= base_url('admin/notif') ?>" class="nav-link">
					<i class="nav-icon fas fa-envelope"></i>
					<p>
						Pesan
						<?php if (count_admin_notif() > 0) { ?>
							<span class="badge badge-pill badge-warning"><?= count_admin_notif() ?></span>
						<?php } ?>
					</p>
				</a>
			</li>

		</ul>

	</nav>
	<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
