<!-- Brand Logo -->
<a href="<?= base_url() ?>admin/dashboard" class="brand-link navbar-light">
	<img src="<?= base_url() ?>public/dist/img/reads-logo.png" alt="SIM Borang UMY" class="brand-image">
	<span class="brand-text font-weight-light">&nbsp;</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
	<!-- Sidebar Menu -->
	<nav class="mt-2">
		<ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu" data-accordion="false">
			<!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
			<li class="nav-item" id="beranda">
				<a href="<?= base_url() ?>dosen/dashboard" class="nav-link">
					<i class="nav-icon fas fa-home"></i>
					<p>
						Beranda Dosen
					</p>
				</a>
			</li>

			<li class="nav-item" id="ilmiah">
				<a href="<?= base_url("admin/ilmiah") ?>" class="nav-link">
					<i class="nav-icon fas fa-book"></i>
					<p>
						Ilmiah
					</p>
				</a>
			</li>

			<li class="nav-item" id="ilmiah">
				<a href="<?= base_url("admin/residen") ?>" class="nav-link">
					<i class="nav-icon fas fa-book"></i>
					<p>
						Residen
					</p>
				</a>
			</li>
			<!-- <li class="nav-item has-treeview" id="ilmiah">
				<a href="#" class="nav-link">
					<i class="nav-icon fas fa-book"></i>
					<p>
						Ilmiah 
						<i class="fas fa-angle-left right"></i>
					</p>
				</a>	
				<ul class="nav nav-treeview">
					<?php foreach (tahapHelper() as $tahap) { ?>
					<li class="nav-item tahap<?= $tahap['id'] ?>"> 
						<a href="<?= base_url('dosen/ilmiah/tahap/' . $tahap['id']); ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Ilmiah Tahap <?= $tahap['tahap'] ?></p>
						</a>
					</li>
					<?php } ?>
				</ul>

			</li> -->

		</ul>

	</nav>
	<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
