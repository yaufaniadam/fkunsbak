<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-light">
	<!-- Left navbar links -->
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
		</li>

	</ul>



	<!-- Right navbar links -->
	<ul class="navbar-nav ml-auto">
		<?php if ($_SESSION['role'] == 3) { ?>
			<li class="nav-item dropdown">
				<a class="btn dropdown-toggle mr-4" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="far fa-envelope"></i>
					<?php if (count_mail() > 0) { ?>
						<span class="badge badge-danger"><?= count_mail() ?></span></h6>
					<?php } ?>
				</a>

				<div class="dropdown-menu" style="border-radius: 0;width: 280px; padding: 0;">
					<ul class="list-group list-group-flush">
						<?php if (email()) { ?>
							<?php foreach (email() as $email) { ?>
								<a href="<?= base_url('residen/pesan/detail/' . $email['id']) ?>" class="list-group-item list-group-item-action flex-column align-items-start">
									<div class="d-flex w-100 justify-content-between">
										<h7 class="mb-1"><b> <?= $email['subject'] ?></b></h5>
									</div>
									<small class="mb-1"><?= substr($email['content'], 0, 20) ?>...</small>
								</a>
							<?php } ?>
						<?php } else { ?>
							<div class="align-middle text-center w-100 p-3">
								<span class="text-secondary text-center">tidak ada pesan masuk</span>
							</div>
						<?php } ?>
					</ul>
					<div class="text-center bg-secondary">
						<a href="<?= base_url('residen/pesan') ?>"><b>Lihat Semua Pesan</b></a>
					</div>
				</div>
			</li>
		<?php } ?>

		<li class="nav-item d-none">
			<?php if ($_SESSION['role'] != 1) { ?>
				<a class="nav-link" href="<?= base_url('profile'); ?>">
					<?php

					$photo = getUserbyId($this->session->userdata("user_id"))['photo'];
					$nama = getUserbyId($this->session->userdata("user_id"))['nama_lengkap'];
					if ($photo == '') { ?>

						<img width="30" height="" class="img-fluid img-circle" src="<?= base_url(); ?>public/dist/img/nophoto.png" alt="User profile picture">

					<?php } else { ?>

						<img width="30" height="" class="img-fluid img-circle" src="<?= base_url($photo); ?>">

					<?php } ?> &nbsp;

					&nbsp; <?= ($nama == '' ? $this->session->userdata("username") : $nama) ?>
				</a>
			<?php } ?>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="<?= site_url('auth/logout'); ?>">
				<i class="fas fa-sign-out-alt"></i> Logout
			</a>
		</li>
	</ul>
</nav>

<?php if ($_SESSION['role'] == 3) { ?>

	<style>
		.list-group {
			max-height: 200px;
			overflow-y: auto;
		}

		::-webkit-scrollbar {
			width: 5px;
		}

		/* Track */
		::-webkit-scrollbar-track {
			background: lightgray;
		}

		/* Handle */
		::-webkit-scrollbar-thumb {
			background: #888;
		}

		/* Handle on hover */
		::-webkit-scrollbar-thumb:hover {
			background: #555;
		}
	</style>
	<!-- /.navbar -->
<?php } ?>