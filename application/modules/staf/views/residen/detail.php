<!-- Content Header (Page header) -->
<?php
$segments = $this->uri->segment(5, 1);
$tahap_indc = $segments;
if ($segments == 2) {
	$tahap_indc = '2a';
}
if ($segments == 3) {
	$tahap_indc = '2b';
}
if ($segments == 4) {
	$tahap_indc = 3;
}
foreach ($residen as $residen) {
}

$id_residen = $this->uri->segment(4);
$tahap_residen = $this->uri->segment(5);
$divisi_residen = $this->uri->segment(6);
$progress = residenProgress($id_residen, $tahap_residen, $divisi_residen);
?>
 
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Residen</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->


<section class="content">

	<div class="col-12">
		<?php if (isset($msg) || validation_errors() !== '') : ?>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="fa fa-exclamation"></i> Terjadi Kesalahan</h4>
				<?= validation_errors(); ?>
				<?= isset($msg) ? $msg : ''; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php if ($this->session->flashdata('msg') != '') : ?>
		<div class="alert alert-success flash-msg alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4>Success!</h4>
			<?= $this->session->flashdata('msg'); ?>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('wrn') != '') : ?>
		<div class="alert alert-warning flash-msg alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4>Warning!</h4>
			<?= $this->session->flashdata('wrn'); ?>
		</div>
	<?php endif; ?>

	<div class="container-fluid">
		<div class="row">

			<div class="col-md-3">

				<!-- Profile Image -->
				<div class="card card-olive card-outline">
					<div class="card-body box-profile">
						<div class="text-center">
							<img style="width: 200px;" src="<?= $residen['photo'] != "" ? $residen['photo'] : base_url('./uploads/fotoProfil/dummy.png') ?>" class="img-circle">
						</div>
						<h3 class="profile-username text-center"><?= $residen['nama_lengkap'] ?></h3>

						<ul class="list-group list-group-unbordered mb-3">
							<li class="list-group-item">
								<b>Tahap</b> <a class="float-right"><?= $residen_tahap ?></a>
							</li>
							<?php if ($residen_tahap == 2 or $residen_tahap == 3) { ?>
								<li class="list-group-item">
									<b>Divisi</b> <a class="float-right"><?= residenDivisi($divisi_residen) ?></a>
								</li>
							<?php } ?>
							<li class="list-group-item">
								<b>Progress : </b>
								<a>
									<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: <?= $progress; ?>%;height :20px; ">
										<?= $progress; ?>%
									</div>
								</a>
							</li>
						</ul>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->

			</div>
			<!-- /.col -->
			<div class="col-md-9">
				<div class="card">
					<div class="card-header pl-3 pt-2 pb-2">
						<div class="row">
							<div class="col-sm">
								<h3>Daftar Ilmiah</h3>
							</div>

						</div>
					</div><!-- /.card-header -->

					<div class="card-body">
						<br>
						<table id="tb_penelitian" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th style="width: 50%;" class="text-center">Judul Ilmiah</th>
									<th class="text-center">Kategori</th>
									<th class="text-center">Approved</th>
									<th class="text-center">Detail</th>
								</tr>
							</thead>
							<tbody>
								<?php if ($ilmiah) {
									foreach ($ilmiah as $ilmiah) { ?>
										<tr>
											<td><?= $ilmiah['judul_ilmiah'] ?></td>
											<td><?= $ilmiah['id_kategori'] ?></td>
											<td class="text-center">
												<?= $ilmiah['status'] == 1 ? '<i class="fas fa-check"></i>' : (($ilmiah['status'] == 2) ? '<i class="fas fa-times"></i>' : "") ?>
											</td>
											<td class="text-center">
												<a href="<?= base_url("dosen/ilmiah/detail/" . $ilmiah['id']) ?>" class="btn btn-default btn-sm">
													<i class="fa fa-search" style="color:;"></i>
												</a>
											</td>
										</tr>
								<?php }
								} else {
									echo "<tr><td class='text-center' colspan = 3 >No Data</td></tr>";
								}
								?>

							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</section>

<!-- tb js -->
<script src="<?= base_url() ?>/public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script>
	$('#confirm-konfirm').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});

	$(function() {
		$("#tb_penelitian").DataTable();
	});

	$("#<?= $id_menu; ?>").addClass('menu-open');
	$("#<?= $id_menu; ?> .<?= $class_menu; ?> a.nav-link").addClass('active');
</script>

<style>
	.scrollable-menu {
		height: auto;
		max-height: 200px;
		overflow-x: hidden;
	}
</style>
