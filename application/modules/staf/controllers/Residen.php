<?php defined('BASEPATH') or exit('No direct script access allowed');
class Residen extends Staf_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ilmiah_model', 'ilmiah_model');
		$this->load->model('residen_model', 'residen_model');
		$this->load->model('divisi_model', 'divisi_model');
		$this->load->model('tahap_model', 'tahap_model');
		// $this->load->model('kategori_model', 'kategori_model');
		// $this->load->model('divisi_model', 'divisi_model');
	}

	public function index()
	{
		$query = $this->db->query('select nama_lengkap, nim from residen');
		$result = $query->result_array();

		$data['query'] = $result;
		$data['id_menu'] = "residen";
		$data['class_menu'] = "index";
		$data['title'] = 'Residen';
		$data['type'] = 'allResiden';
		$data['deskripsi'] = 'Tampilkan semua residen baik yang masih aktif maupun yng sudah lulus';
		$data['view'] = 'residen/index';
		$this->load->view('layout/layout', $data);
	}

	public function tahap($id_tahap = 1, $id_divisi = 0)
	{
		// $query = $this->db->query("SELECT r.nama_lengkap, r.nim, r.id,
		// (select rt.tahap from residen_tahap rt where rt.id_residen = r.id order by rt.id desc limit 1) as tahap 
		// from residen r		
		// ");
		// $result = $this->residen_model->get_residen_by_tahap();
		// $id_residen = $result['id'];
		// echo $id_residen;

		$data['query'] = $this->residen_model->get_residen_by_tahap_and_divisi($id_tahap, $id_divisi);
		$data['tahap_id'] = $id_tahap;
		$data['id_menu'] = "ilmiah";
		$data['class_menu'] = "tahap" . $id_tahap;
		$data['title'] = 'Residen by Tahap';
		$data['type'] = 'residenByTahap';
		// $data['deskripsi'] = 'Tampilkan semua residen aktif dan Tahap yang ditempuh saat ini';
		$data['view'] = 'residen/index';
		$this->load->view('layout/layout', $data);
	}

	public function detail($id_residen, $id_tahap = 1, $id_divisi = 0)
	{
		$data['residen'] = $this->residen_model->get_residen_by_id($id_residen);
		$data['residen_tahap'] = $this->tahap_model->get_residen_tahap($id_residen);
		$data['tahap'] = $this->tahap_model->get_all_tahap();
		$data['ilmiah'] = $this->ilmiah_model->get_residen_ilmiah($id_residen, $id_tahap, $id_divisi);
		$data['residen_divisi'] = $this->divisi_model->get_residen_divisi($id_residen);
		$data['divisi'] = $this->divisi_model->get_all_divisi();
		$data['view'] = 'residen/detail';
		$this->load->view('layout/layout', $data);
	}

	public function tahap_by_residen($id_residen)
	{
		$query = $this->db->query("SELECT rt.*, r.nama_lengkap FROM residen_tahap rt
		LEFT JOIN residen r ON r.id=rt.id_residen 
		WHERE rt.id_residen=$id_residen
		");
		$result = $query->result_array();

		$data['query'] = $result;
		$data['id_menu'] = "residen";
		$data['class_menu'] = "tahap-residen";
		$data['title'] = 'Tahap spesifik Residen';
		$data['type'] = 'tahapSpesifikResiden';
		$data['deskripsi'] = 'Tampilkan semua tahap spesifik residen';
		$data['view'] = 'residen/index';
		$this->load->view('layout/layout', $data);
	}

	public function divisi()
	{
		$query = $this->db->query("select r.nama_lengkap, r.nim, rt.tahap, rto.id_tod, rto.id_divisi, d.divisi, CONCAT(DATE_FORMAT(tod.start_date, '%M %Y') , ' - ', DATE_FORMAT(tod.end_date, '%M %Y')) as periode
		from residen r
		LEFT JOIN residen_tahap rt ON rt.id_residen = r.id
		LEFT JOIN residen_tod rto ON rto.id_residen = r.id
		JOIN divisi d ON rto.id_divisi = d.id
		JOIN tod tod ON rto.id_tod = tod.id
		WHERE (rt.tahap='2a' OR rt.tahap='2b')	
		");
		$result = $query->result_array();

		$data['query'] = $result;
		$data['id_menu'] = "residen";
		$data['class_menu'] = "residen-divisi";
		$data['title'] = 'Residen by Divisi';
		$data['type'] = 'residenByDivisi';
		$data['deskripsi'] = 'Tampilkan semua residen yang sudah menempuh tahap 2a dan 2b serta divisinya yg ditempuh saat ini';
		$data['view'] = 'residen/index';
		$this->load->view('layout/layout', $data);
	}

	public function divisi_by_residen($id_residen)
	{
		$query = $this->db->query("SELECT rd.*,r.nama_lengkap FROM residen_divisi rd
		LEFT JOIN residen r ON r.id=rd.id_residen
		WHERE rd.id_residen=$id_residen
		");
		$result = $query->result_array();

		$data['query'] = $result;
		$data['id_menu'] = "residen";
		$data['class_menu'] = "divisi-residen";
		$data['title'] = 'Divisi spesifik Residen';
		$data['type'] = 'divisiSpesifikResiden';
		$data['deskripsi'] = 'Tampilkan semua divisi spesifik residen';
		$data['view'] = 'residen/index';
		$this->load->view('layout/layout', $data);
	}
}
