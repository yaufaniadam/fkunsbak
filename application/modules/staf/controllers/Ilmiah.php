<?php defined('BASEPATH') or exit('No direct script access allowed');
class Ilmiah extends MY_Controller
{

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model('ilmiah_model', 'ilmiah_model');
		$this->load->model('residen_model', 'residen_model');
		$this->load->model('kategori_model', 'kategori_model');
		$this->load->model('divisi_model', 'divisi_model');
	}

	public function tahap($id_tahap)
	{
		$result = $this->ilmiah_model->get_ilmiah_by_tahap($id_tahap);

		// if ($result == null) {
		// 	$data['error'] = 'Data tidak ditemukan';
		// 	$data['view'] = 'layout/error.php';
		// 	$this->load->view('layout/layout', $data);
		// }

		// if ($result) {
			$query2 = $this->db->get_where('tahap', array('id' => $id_tahap));
			$tahap_result = $query2->row();
			$tahap = $tahap_result->tahap;

			$data['query'] = $result;
			$data['id_menu'] = "ilmiah";
			$data['class_menu'] = "tahap" . $id_tahap;
			$data['tahap'] = $tahap; 
			$data['title'] = 'Ilmiah Tahap ' . $tahap;
			$data['view'] = 'ilmiah/tahap.php';
			$this->load->view('layout/layout', $data);
		// }

		
	}

	public function detail($id)  
	{ 
		$author = $this->residen_model->get_author_of_ilmiah($id);

		$query = $this->db->query("SELECT * FROM ilmiah WHERE id = " . $id);
		$result = $query->result_array();
		$result2 = $query->row();
		$id_tahap = $result2->id_tahap;

		$query2 = $this->db->get_where('tahap', array('id' => $id_tahap));
		$tahap_result = $query2->row();
		$tahap = $tahap_result->tahap;

		$data['query'] = $result;
		$data['title'] = 'Ilmiah Tahap ' . $tahap;
		$data['author'] = $author;
		$data['id_menu'] = 'ilmiah';
		$data['for_breadcrumb'] = $id_tahap;
		$data['class_menu'] = 'tahap' . $id_tahap;
		$data['view'] = 'ilmiah/detail.php';
		$this->load->view('layout/layout', $data);
	}

	public function konfirm($id,$stats)
	{
		$query = $this->db->get_where('ilmiah', array('id' => $id));
		$result = $query->row();
		$status = $result->status;
		$nilai = $result->nilai;

		// function (){};
		
		if ($status != 1 && $nilai != null && $stats == 1) {
			$this->db->where('id', $id);
			$this->db->update('ilmiah', array('status' => $stats));
			$this->session->set_flashdata('msg', 'Ilmiah berhasil dikonfirmasi!');
			redirect(base_url('dosen/ilmiah/detail/' . $id));
		} elseif ($status != 1 && $status != 2 && $stats == 2) {
			$this->db->where('id', $id);
			$this->db->update('ilmiah', array('status' => $stats));
			$this->session->set_flashdata('msg', 'Permintaan revisi berhasil dikirim!');
			redirect(base_url('dosen/ilmiah/detail/' . $id));
		} elseif ($status == 1) {
		} elseif ($nilai == null) {
			$this->session->set_flashdata('wrn', 'Dokumen ilmiah belum lengkap!');
			redirect(base_url('dosen/ilmiah/detail/' . $id));
			$this->session->set_flashdata('wrn', 'Ilmiah sudah dikonfirmasi!');
			redirect(base_url('dosen/ilmiah/detail/' . $id));
		}
	}
}
