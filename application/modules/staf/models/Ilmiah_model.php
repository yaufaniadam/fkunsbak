<?php
class Ilmiah_model extends CI_Model
{
	public function edit_ilmiah($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('ilmiah', $data);
		return true;
	}

	public function get_all_ilmiah()
	{
		$query = $this->db->query("SELECT i.id, i.judul_ilmiah, i.date, k.kategori, rt.tahap, r.nama_lengkap FROM ilmiah i
		LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
		LEFT JOIN residen r ON r.id=i.id_residen		
		LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap		
		");
		return $query->result_array();
	}

	public function get_ilmiah_by_tahap($tahap)
	{
		$divisi_dosen = $_SESSION['id_divisi'];

		$query = $this->db->query("SELECT i.status, i.id, i.judul_ilmiah, i.date, k.kategori, rt.tahap, r.nama_lengkap FROM ilmiah i
		LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
		LEFT JOIN residen r ON r.id=i.id_residen		
		LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap	
		WHERE i.id_tahap=$tahap AND i.id_divisi = $divisi_dosen ");
		return $query->result_array();
	}

	public function get_ilmiah_by_divisi()
	{
		$query = $this->db->query("SELECT i.id, i.judul_ilmiah, i.date, k.kategori, rt.tahap, r.nama_lengkap FROM ilmiah i
		LEFT JOIN kategori_ilmiah k ON k.id=i.id_kategori		
		LEFT JOIN residen r ON r.id=i.id_residen		
		LEFT JOIN residen_tahap rt ON rt.id=i.id_tahap	
		WHERE (rt.tahap='2a' OR rt.tahap='2b')
		");
		return $query->result_array();
	}

	public function get_residen_ilmiah($id_residen,$id_tahap,$id_divisi)
	{
		$query = $this->db->get_where('ilmiah',array('id_residen'=>$id_residen,'id_tahap'=>$id_tahap,'id_divisi'=>$id_divisi));
		return $query->result_array();
	}

	
}
