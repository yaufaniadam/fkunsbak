<?php
class Kategori_model extends CI_Model
{
	public function get_kategori_by_tahap($tahap)
	{
		$query = $this->db->get_where('kategori_ilmiah',array('tahap'=>$tahap));
		return $query->result_array();
	}
}
