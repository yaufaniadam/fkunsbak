<?php
class Residen_model extends CI_Model
{
	public function get_author_of_ilmiah($id_ilmiah)
	{
		$query = $this->db->query("SELECT nama_lengkap FROM residen WHERE id = (SELECT id_residen FROM ilmiah WHERE id =" . $id_ilmiah . ")");
		$result = $query->row();
		return $result->nama_lengkap;
	}
	
	public function get_residen_id_tahap()
	{
		$user_id = $_SESSION['user_id'];
		$query = $this->db->query("SELECT id FROM tahap WHERE tahap = (SELECT tahap FROM residen_tahap WHERE aktif = 1 AND id_residen = (SELECT id FROM residen WHERE user_id =".$user_id."))");
		$result = $query->row();
		return $result->id;
	}

	public function get_residen_by_tahap_and_divisi($id_tahap,$id_divisi)
	{
		// $query = $this->db->query("SELECT r.nama_lengkap,r.nim,r.id,rt.tahap FROM residen r,residen_tahap rt WHERE r.id = rt.id_residen AND rt.tahap = $id_tahap AND rt.aktif = 1	
		// ");

		$query = $this->db->query("SELECT * FROM residen r LEFT JOIN residen_tahap rt ON r.id = rt.id_residen LEFT JOIN residen_divisi rd ON r.id = rd.id_residen WHERE rt.aktif = 1 AND rt.tahap = $id_tahap AND rd.id_divisi = $id_divisi AND rd.id = (SELECT MAX(id) FROM residen_divisi)
		");
 
		return  $query->result_array();
	}
 
	public function get_residen_by_id($id_residen)
	{
		$query = $this->db->get_where('residen',array('id'=>$id_residen));
		return $query->result_array(); 
	}
}
