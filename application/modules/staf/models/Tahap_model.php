<?php
class Tahap_model extends CI_Model
{
	public function get_residen_tahap($id_residen)
	{
		$query = $this->db->get_where('residen_tahap',array('id_residen'=>$id_residen,'aktif'=>1));
		$id_tahap =  $query->row()->tahap;

		$query2 = $this->db->get_where('tahap',array('id'=>$id_tahap));
		return $query2->row()->tahap;
	}

	public function get_all_tahap()
	{
		$query = $this->db->get('tahap');
		return $query->result_array();
	}
}
  