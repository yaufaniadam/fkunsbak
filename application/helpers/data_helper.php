<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


// -----------------------------------------------------------------------------
function getUserbyId($id)
{
	$CI = &get_instance();
	if ($CI->session->userdata("role") == 2) {
		return $CI->db->select('u.*, d.nama_lengkap,d.photo')->from('ci_users u')->where('u.id', $id)
			->join('dosen d', 'u.id = d.user_id', 'left')
			->get()->row_array();
	} else if ($CI->session->userdata("role") == 3) {
		return $CI->db->select('u.*, r.nama_lengkap,r.photo')->from('ci_users u')->where('u.id', $id)
			->join('residen r', 'u.id = r.user_id', 'left')
			->get()->row_array();
	} else {
		return $CI->db->select('u.*, r.nama_lengkap,r.photo')->from('ci_users u')->where('u.id', $id)
			->join('residen r', 'u.id = r.user_id', 'left')
			->get()->row_array();
	}
}

function ilmiah_tahap($id_tahap)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT tahap FROM tahap WHERE id = $id_tahap")->row()->tahap;
	return $query;
}

function count_residen_in_lobby()
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT * FROM residen_divisi WHERE id_divisi = 12")->num_rows();
	return $query;
}

function getAllResiden()
{
	$CI = &get_instance();
	$query = $CI->db->get('residen');
	return $query->result_array();
}

function getAllDivisi()
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT * FROM divisi WHERE id != 12");
	return $query->result_array();
}

function currentUserTahap()
{
	$CI = &get_instance();
	$currentUserId = $_SESSION['user_id'];
	$query = $CI->db->query("SELECT * FROM tahap WHERE id = (SELECT tahap FROM residen_tahap WHERE aktif = 1 AND id_residen = (SELECT id FROM residen WHERE user_id =" . $currentUserId . "))
	");
	$tahap =  $query->row();
	return $tahap->id;
}

function getCurrentUser()
{
	$CI = &get_instance();
	$currentUserId = $_SESSION['user_id'];
	$query = $CI->db->get_where('residen', array('user_id' => $currentUserId));
	$currentUser = $query->row();
	return $currentUser->nama_lengkap;
}

function tahapHelper()
{
	$CI = &get_instance();
	$query = $CI->db->get('tahap');
	$result = $query->result_array();
	return $result;
}

function residenProgress($id_residen, $id_tahap, $id_divisi = 0)
{
	$CI = &get_instance();

	if ($id_tahap == 1) {
		$query = $CI->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap));
	} else {
		$query = $CI->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'id_divisi' => $id_divisi, 'file_nilai !=' => ''));
	}
	$jumlahdoc = $query->num_rows();

	$query2 = $CI->db->get_where('kategori_tahap', array('id_tahap' => $id_tahap));
	$jumlahkategory = $query2->num_rows();


	if ($id_tahap == 1) {
		$progress = number_format($jumlahdoc / 8 * 100);
	} else {
		$progress = $jumlahdoc / $jumlahkategory * 100;
	}
	$formattedprogress = number_format($progress, 0);

	return $formattedprogress;
	// return $id_residen;
}

function approvedProgress($id_residen, $id_tahap, $id_divisi = 0)
{
	$CI = &get_instance();


	if ($id_tahap == 1) {
		$query = $CI->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap));
	} else {
		$query = $CI->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'id_divisi' => $id_divisi));
	}
	$jumlahdoc = $query->num_rows();

	$query3 = $CI->db->get_where('kategori_tahap', array('id_tahap' => $id_tahap));
	$jumlahkategory = $query3->num_rows();

	if ($jumlahdoc) {
		if ($id_tahap == 1) {
			$query2 = $CI->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'status' => 1));
			$docapproved = $query2->num_rows();
			$progress = $docapproved / 8 * 100;
		} else {
			$query2 = $CI->db->get_where('ilmiah', array('id_residen' => $id_residen, 'id_tahap' => $id_tahap, 'id_divisi' => $id_divisi, 'status' => 1));
			$docapproved = $query2->num_rows();
			$progress = $docapproved / $jumlahkategory * 100;
		}
		$formattedprogress = number_format($progress, 0);
		return $formattedprogress;
	} else {
		return 0;
	}
}

function tahapSelesai($id_tahap)
{
	$CI = &get_instance();
	$currentUserId = $_SESSION['user_id'];
	$query = $CI->db->get_where('residen', array('user_id' => $currentUserId));
	$residenId = $query->row()->id;
	$query2 = $CI->db->query("SELECT tahap FROM `residen_tahap` WHERE id_residen = $residenId AND tahap = $id_tahap");

	if ($query2->num_rows() < 1) {
		return null;
	} else {
		return $CI->db->query("SELECT tahap FROM `residen_tahap` WHERE id_residen = $residenId AND tahap = $id_tahap")->row()->tahap;
	}
}

function residenDivisi($id_divisi)
{
	$CI = &get_instance();
	$query = $CI->db->get_where('divisi', array('id' => $id_divisi));
	return $query->row()->divisi;
}

// function kategoriHelper($tahap){
// 	$CI = &get_instance();
// 	$query = $CI->db->get_where('kategori_ilmiah',array('tahap'=>$tahap));
// 	$result = $query->result_array();
// 	return $result;
// }

function getResidenId()
{
	$CI = &get_instance();
	$user_id = $_SESSION['user_id'];

	$query = $CI->db->get_where('residen', array('user_id' => $user_id));
	$result = $query->row();
	return $result->id;
}

function email()
{
	$CI = &get_instance();
	$id_residen = $_SESSION["residen_id"];

	$query = $CI->db->get_where("notif", array("id_residen" => $id_residen, 'status' => 0))->result_array();

	return $query;
}

function count_mail()
{
	$CI = &get_instance();
	$id_residen = $_SESSION["residen_id"];

	$query = $CI->db->get_where("notif", array("id_residen" => $id_residen, 'status' => 0))->num_rows();

	return $query;
}

function count_admin_notif()
{
	$CI = &get_instance();

	$query = $CI->db->get_where('admin_notif', array('status' => 0))->num_rows();

	return $query;
}


function indonesian_date($timestamp = '', $date_format = 'j F Y', $suffix = 'WIB')
{
	if (trim($timestamp) == '') {
		$timestamp = time();
	} elseif (!ctype_digit($timestamp)) {
		$timestamp = strtotime($timestamp);
	}
	# remove S (st,nd,rd,th) there are no such things in indonesia :p
	$date_format = preg_replace("/S/", "", $date_format);
	$pattern = array(
		'/Mon[^day]/', '/Tue[^sday]/', '/Wed[^nesday]/', '/Thu[^rsday]/',
		'/Fri[^day]/', '/Sat[^urday]/', '/Sun[^day]/', '/Monday/', '/Tuesday/',
		'/Wednesday/', '/Thursday/', '/Friday/', '/Saturday/', '/Sunday/',
		'/Jan[^uary]/', '/Feb[^ruary]/', '/Mar[^ch]/', '/Apr[^il]/', '/May/',
		'/Jun[^e]/', '/Jul[^y]/', '/Aug[^ust]/', '/Sep[^tember]/', '/Oct[^ober]/',
		'/Nov[^ember]/', '/Dec[^ember]/', '/January/', '/February/', '/March/',
		'/April/', '/June/', '/July/', '/August/', '/September/', '/October/',
		'/November/', '/December/',
	);
	$replace = array(
		'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min',
		'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu',
		'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des',
		'Januari', 'Februari', 'Maret', 'April', 'Juni', 'Juli', 'Agustus', 'Sepember',
		'Oktober', 'November', 'Desember',
	);
	$date = date($date_format, $timestamp);
	$date = preg_replace($pattern, $replace, $date);
	$date = "{$date} {$suffix}";
	return $date;
}

function cek_duplikat_user($username, $email)
{
	$CI = &get_instance();

	$query = $CI->db->select('id')->from("ci_users")->where("username", $username)->or_where("email", $email)->get()->result();

	if ($query) {
		return "1";
	} else {
		return '0';
	}
}

function get_residen_per_period($id_tod, $id_divisi)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT id_residen,nama_lengkap FROM residen_tod rt LEFT JOIN residen r ON rt.id_residen = r.id WHERE id_tod = $id_tod AND id_divisi = $id_divisi");
	$results = $query->result_array();

	return $results;
}

function get_tod_id()
{
	$CI = &get_instance();
	$query = $CI->db->query('select id from tod');
	$results = $query->result_array();

	return $results;
}

function ilmiah_dashboard($id_tahap)
{
	$CI = &get_instance();
	if ($_SESSION['role'] == 1) {
		return $CI->db->query("SELECT * FROM ilmiah WHERE id_tahap = $id_tahap AND status = 1 ")->num_rows();
	} elseif ($_SESSION['role'] == 3) {
		$id_residen = $_SESSION['residen_id'];
		return $CI->db->query("SELECT * FROM ilmiah WHERE id_tahap = $id_tahap AND id_residen = $id_residen ")->num_rows();
	}
}

function residen_dibimbing($id_dosen)
{
	$CI = &get_instance();
	$query = $CI->db->query("SELECT * FROM residen WHERE id_pembimbing = $id_dosen")->result_array();
	return $query;
}

function current_user_divisi()
{
	$CI = &get_instance();
	$id_user = $_SESSION['residen_id'];
	$query = $CI->db->get_where('residen_divisi', array('id_residen' => $id_user))->row()->id_divisi;
	return $query;
}
