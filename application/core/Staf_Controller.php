<?php defined('BASEPATH') or exit('No direct script access allowed');
class Staf_Controller extends Admin_Controller
{
	function __construct()
	{
		parent::__construct();

		if (!($this->session->userdata('role') == 2)) {
			$data['view'] = 'layout/restricted';
			$this->load->view('layout/layout', $data);
		}
	}
}
